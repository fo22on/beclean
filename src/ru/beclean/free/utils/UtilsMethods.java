package ru.beclean.free.utils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import ru.beclean.free.R;
import ru.beclean.free.ui.servertypes.Reserv;
import ru.beclean.free.ui.servertypes.Services;
import ru.beclean.free.ui.servertypes.ServiciesTypes;
import ru.beclean.free.ui.servertypes.ServiciesTypes.ServiceType;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.Location;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

public class UtilsMethods
{
	private static final String PREFS_NAME = "trash";
	private static final String PREFS_TOKEN_NAME = "jhrerjkger8g9345nlvbs8dv";
	private static final String PREFS_USER_CAR_CATEGORY_NAME = "klgdksdlasjknfdfdvkdfjkd";
	private static final String PREFS_USER_NAME = "sdlisvdig";
	private static final String PREFS_USER_PHONE = "kclsdvsigruisdufu";
	private static final String PREFS_RESERV = "gdjhtytgbsrjkr";
	private static final int MAP_MIN_ZOOM = 9;
	private static final double MOSCOW_LATITUDE = 55.753559;
	private static final double MOSCOW_LONGETUDE = 37.609218;

	public static String getPhoneFromFormattedString(CharSequence in)
	{
		StringBuilder sb = new StringBuilder();
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(in);
		while (m.find())
		{
			sb.append(m.group(0));
		}
		return sb.toString();
	}

	public static void saveToken(Context context, String token)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString(PREFS_TOKEN_NAME, token);
		editor.commit();
	}

	public static String getToken(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

		return prefs.getString(PREFS_TOKEN_NAME, "");
	}

	public static void saveUserCarCategory(Context context, int id)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putInt(PREFS_USER_CAR_CATEGORY_NAME, id);
		editor.commit();
	}

	public static int getUserCarCategory(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		int out = prefs.getInt(PREFS_USER_CAR_CATEGORY_NAME, -1);

		return out;
	}
	
	public static String[] getUser(Context context)
	{
		String[] out = new String[2];
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		out[0] = prefs.getString(PREFS_USER_PHONE, null);
		out[1] = prefs.getString(PREFS_USER_NAME, null);
		return out;
	}
	
	public static void setUser(Context context, String phone, String name)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString(PREFS_USER_PHONE, phone);
		editor.putString(PREFS_USER_NAME, name);
		editor.commit();
	}

	/**
	 * 
	 * @param map
	 *            ����� � ������� ����� ����� ����������
	 * @param startPoint
	 *            ���������� ������ �������� ���� ������ (0,0)
	 * @param endPoint
	 *            ���������� ������� ������� ���� ������ (width, height)
	 * @return new RectF(latitudeStart,longetudeStart,latitudeEnd, longetudeEnd)
	 */
	public static RectF getVisibleRectangle(GoogleMap map, Point startPoint, Point endPoint)
	{
		RectF bounds = new RectF();
		Projection p = map.getProjection();
		bounds.left = (float) p.fromScreenLocation(startPoint).latitude;
		bounds.top = (float) p.fromScreenLocation(startPoint).longitude;
		bounds.left = (float) p.fromScreenLocation(endPoint).latitude;
		bounds.top = (float) p.fromScreenLocation(endPoint).longitude;
		return bounds;
	}

	public static String getDateFromMils(long milliSeconds)
	{
		// 2014-05-25 16:00
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());

		// Create a calendar object that will convert the date and time value in
		// milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	public static BitmapDescriptor getIcon(int id)
	{
		return BitmapDescriptorFactory.fromResource(id);
	}

	public static BitmapDescriptor getIcon(Resources res, int id, int count)
	{
		Bitmap pin = BitmapFactory.decodeResource(res, id);
		Bitmap drawPin = Bitmap.createBitmap(pin.getWidth(), pin.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(drawPin);
		Paint textPaint = new Paint();

		textPaint.setColor(res.getColor(R.color.text_white_color));
		textPaint.setAntiAlias(true);
		textPaint.setTextAlign(Align.CENTER);
		textPaint.setStyle(Paint.Style.FILL);
		textPaint.setTextSize(res.getDimensionPixelSize(R.dimen.text_marker_size));
		textPaint.setTypeface(Typeface.DEFAULT_BOLD);

		canvas.drawBitmap(pin, 0, 0, null);
		canvas.drawText("" + count, drawPin.getWidth() / 2, drawPin.getHeight() / 2 + textPaint.getTextSize() / 2.5F,
				textPaint);

		pin.recycle();

		return BitmapDescriptorFactory.fromBitmap(drawPin);
	}

	public static CameraUpdate getDefaultMapPosition()
	{
		return CameraUpdateFactory.newLatLngZoom(new LatLng(MOSCOW_LATITUDE, MOSCOW_LONGETUDE), getMapMinZoom());
	}

	public static int getMapMinZoom()
	{
		return MAP_MIN_ZOOM;
	}

	public static Location getDefaultPosition()
	{
		Location current = new Location(StaticStrings.MOSCOw);
		current.setLatitude(MOSCOW_LATITUDE);
		current.setLongitude(MOSCOW_LONGETUDE);
		return current;
	}

	public static boolean initSerices(ArrayList<Services> washSer, ServiciesTypes types)
	{
		boolean out = false;
		for (Services service : washSer)
		{
			boolean isOtherService = false;
			for (ServiceType type : types.others)
			{
				if (service.equals(Integer.valueOf(type.id)))
				{
					out = true;
					isOtherService = true;
					service.isOtherServices = isOtherService;
					service.name = type.name;
					break;
				}
			}
			if (!isOtherService)
			{
				for (ServiceType type : types.items)
				{
					if (service.equals(Integer.valueOf(type.id)))
					{
						isOtherService = false;
						service.isOtherServices = isOtherService;
						service.name = type.name;
						break;
					}
				}
			}
			if (service.name == null)
			{
				washSer.remove(service);
			}
		}
		return out;
	}

	public static String[] SplitUsingTokenizer(String subject, String delimiters)
	{
		StringTokenizer strTkn = new StringTokenizer(subject, delimiters);
		ArrayList<String> arrLis = new ArrayList<String>(subject.length());

		while (strTkn.hasMoreTokens())
		{
			arrLis.add(strTkn.nextToken());
		}

		return arrLis.toArray(new String[0]);
	}

	public static RectF getRectForMapList(Context context, Location current)
	{
		RectF rect = new RectF();
		long duration = (FilterUtils.getFilterTimeEnd(context) - FilterUtils.getFilterTimeStart(context))/1000/60;
		double raduisGradus = duration / 2.5F * 1F / 70F;

		rect.left = (float) (current.getLatitude() + raduisGradus);
		rect.top = (float) (current.getLongitude()- raduisGradus);
		rect.right = (float) (current.getLatitude() - raduisGradus);
		rect.bottom = (float) (current.getLongitude() + raduisGradus);

		return rect;
	}
	
	public static String getDistance(Location location, Location currentPosition)
	{
		DecimalFormat df = new DecimalFormat("0.##");
		if(location!=null&&currentPosition!=null)
		{
			double distance = location.distanceTo(currentPosition);
			String postfix;
			if(distance/1000>1)
			{
				postfix = " ��";
				distance = distance/1000;
			}
			else
			{
				postfix = " �";
			}
			
			return df.format(distance) + postfix;
		}
		return "";
	}
	
	public static boolean isNumber(CharSequence number)
	{
		Pattern p = Pattern.compile("^[�-��-�]{1}[0-9]{3}[�-��-�]{2}[0-9]{2,3}" +
				"|[�-��-�]{1}[0-9]{3}[�-��-�]{2}" +
				"|[�-��-�]{1}[0-9]{4}[0-9]{2,3}" +
				"|[0-9]{3}[A-Za-z0-9]{1,8}" +
				"|[0-9]{4}[�-��-�]{2}[0-9]{2,3}" +
				"|[�-��-�]{2}[0-9�-��-�]{3,4}[0-9]{2,3}$");  
        Matcher m = p.matcher(number);  
        return m.matches(); 
	}
		
	public static boolean isCurrentDay (Calendar calendar)
	{
		Calendar current = new GregorianCalendar();
		return (current.get(Calendar.YEAR)==calendar.get(Calendar.YEAR)
				&&current.get(Calendar.MONTH)==calendar.get(Calendar.MONTH)
				&&current.get(Calendar.DAY_OF_MONTH)==calendar.get(Calendar.DAY_OF_MONTH));
	}
	
	public static long getDateFromString(String dateString)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
		
		try
		{
			return format.parse(dateString).getTime();
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return 0;
	}
	
	public static String getStringFromDate(long mils)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
		return format.format(new Date(mils));
	}
	
	public static void saveReserv(Context context, Reserv reserv)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString(PREFS_RESERV, reserv.toString());
		editor.commit();
	}
	
	public static Reserv getReserv(Context context)
	{
		Reserv out = null;
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		String json = prefs.getString(PREFS_RESERV, null);
		if(json!=null)
		{
			try
			{
				out = new Reserv(new JSONObject(json));
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return out;
	}
	
	public static void removeReserv (Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.remove(PREFS_RESERV);
		editor.commit();
	}
}
