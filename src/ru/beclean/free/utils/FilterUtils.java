package ru.beclean.free.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.format.DateUtils;

public class FilterUtils
{
	private static final String PREFS_NAME = "kdsdskdfl.rer";
	private static final String PREFS_FILTER_DATE = "hdselsef";
	private static final String PREFS_FILTER_TIME_START = "uwiushse";
	private static final String PREFS_FILTER_TIME_END = "ldsorwade";
	private static final String PREFS_FILTER_SERVICES = "ytbhjlkjy";
	private static final String PREFS_FILTER_SERVICES_OTHER = "udsydfbsv";
	private static final String PREFS_FILTER_PRICE = "ubhslebsv";
	private static final long timeIn3H = 3*60*60*1000;
	
	public static void setFilterDate(Context context, long mils)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor= prefs.edit();
		editor.putLong(PREFS_FILTER_DATE, mils);
		editor.commit();
	}
	
	public static long getFilterDate(Context context)
	{
		return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getLong(PREFS_FILTER_DATE, System.currentTimeMillis());
	}
	
	public static void setFilterTImes(Context context, long start, long end)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor= prefs.edit();
		editor.putLong(PREFS_FILTER_TIME_START, start);
		editor.putLong(PREFS_FILTER_TIME_END, end);
		editor.commit();
	}
	
	public static long getFilterTimeStart(Context context)
	{
		return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getLong(PREFS_FILTER_TIME_START, System.currentTimeMillis());
	}
	
	/**
	 * @param context - App Context
	 * @param what - 0 ����� ������: 1 ����� ���������.
	 * @return ����������������� ������ � �����
	 * */
	public static String getFilterFormattedTime(Context context, int what)
	{
		long date = getFilterDate(context) + ((what==0)?getFilterTimeStart(context):getFilterTimeEnd(context));
		return UtilsMethods.getDateFromMils(date);
	}
	
	public static long getFilterTimeEnd(Context context)
	{
		return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getLong(PREFS_FILTER_TIME_END, System.currentTimeMillis() + timeIn3H);
	}
	
	public static void setServices(Context context, int ... ids)
	{
		if(ids==null)
			return;
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ids.length; i++)
		{
			sb.append(ids[i]);
			if(i+1<ids.length)
			{
				sb.append(StaticStrings.SEPARATOR);
			}
		}
		
		Editor editor = prefs.edit();
		editor.putString(PREFS_FILTER_SERVICES, sb.toString());
		editor.commit();
	}
	
	public static int[] getServices(Context context)
	{
		int[] out;
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		String[] items = prefs.getString(PREFS_FILTER_SERVICES, "").split(StaticStrings.SEPARATOR);
		if(items.length>0&&items[0].length()>0)
		{
			out = new int[items.length];
			for (int i = 0; i < items.length; i++)
			{
				out[i] = Integer.parseInt(items[i]);
			}
		}
		else
		{
			out = new int[0];
		}
		return out;
	}
	
	public static void setOtherServices(Context context, int ... ids)
	{
		if(ids==null||ids.length<1)
			return;
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ids.length; i++)
		{
			sb.append(ids[i]);
			if(i+1<ids.length)
			{
				sb.append(StaticStrings.SEPARATOR);
			}
		}
		
		Editor editor = prefs.edit();
		editor.putString(PREFS_FILTER_SERVICES_OTHER, sb.toString());
		editor.commit();
	}
	
	public static int[] getOtherServices(Context context)
	{
		int[] out;
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		String[] items = prefs.getString(PREFS_FILTER_SERVICES_OTHER, "").split(StaticStrings.SEPARATOR);
		if(items.length>0&&items[0].length()>0)
		{
			out = new int[items.length];
			for (int i = 0; i < items.length; i++)
			{
				out[i] = Integer.parseInt(items[i]);
			}
		}
		else
		{
			out = new int[0];
		}
		return out;
	}
	
	public static CharSequence getFormatedDate (long mils)
	{
		SimpleDateFormat format = new SimpleDateFormat(", d MMM", Locale.getDefault());
		return DateUtils.getRelativeTimeSpanString(mils, System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS, DateUtils.FORMAT_ABBREV_WEEKDAY) + format.format(new Date(mils));
	}
	
	public static CharSequence getFormatedTime(long milsStart, long delay)
	{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault());
		
		return format.format(new Date(milsStart))+ " - " + format.format(new Date(milsStart + delay));
	}
	
	public static void setPrice(Context context, int price)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor= prefs.edit();
		editor.putInt(PREFS_FILTER_PRICE, price);
		editor.commit();
	}
	
	public static int getPrice(Context context)
	{
		return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getInt(PREFS_FILTER_PRICE, -1);
	}
	
	public static JSONArray getPriceList(Context context)
	{
		JSONArray array = new JSONArray();
		array.put(0);
		array.put(getPrice(context));
		return array;
	}
	
	public static void clearFilters(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor= prefs.edit();
		
		editor.clear();
		editor.commit();
	}
	{
		
	}
}
