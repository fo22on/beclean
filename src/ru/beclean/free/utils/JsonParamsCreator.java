package ru.beclean.free.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParamsCreator
{
	private JSONObject mParams;
	private JSONArray mArray;
	public JsonParamsCreator ()
	{
		mParams = new JSONObject();
	}
	
	public JsonParamsCreator addParam(String fieldName, Object... params)
	{
		Object field;
		
		if(params==null||params.length<1)
		{
			field = new JSONArray();
		}
		else if(params.length==1)
		{
			field = params[0];
		}
		else
		{
			field = new JSONArray();
			for (int i = 0; i < params.length; i++)
			{
				((JSONArray)field).put(params[i]);
			}
		}
		
		try
		{
			mParams.putOpt(fieldName, field);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return this;
	}
	
	public JsonParamsCreator setParams(String... params)
	{
		mArray = null;
		mArray = new JSONArray();
		for (int i = 0; i < params.length; i++)
		{
			mArray.put(params[i]);
		}
		return this;
	}
	
	public JSONObject getParams()
	{
		return mParams;
	}
	
	public JSONArray getArrayParams()
	{
		return mArray;
	}
}
