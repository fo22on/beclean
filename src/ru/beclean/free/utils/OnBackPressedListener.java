package ru.beclean.free.utils;

public interface OnBackPressedListener
{
	boolean onBackPressed();
}
