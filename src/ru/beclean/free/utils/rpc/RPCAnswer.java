package ru.beclean.free.utils.rpc;

import org.json.JSONArray;
import org.json.JSONObject;

public class RPCAnswer
{
	private RPCError error;
	private Object result;
	private boolean isArray = false;
	
	
	public RPCAnswer (JSONObject object)
	{
		JSONObject error = object.optJSONObject("error");
		
		/*{"type":"ReservationError","message":"reservation is only 30 minutes from the current time"}*/
		
		if(error!=null)
		{
			this.error = new RPCError(error);
		}
		else
		{
			result = object.optJSONObject("result");
			if(result==null)
			{
				result = object.optJSONArray("result");
				if(result!=null)
				{
					isArray = true;
				}
			}
		}
	}
	
	public boolean isError()
	{
		return error!=null;
	}
	
	public JSONObject getResult()
	{
		return isError()?error.source:(isArray?null:(JSONObject)result);
	}
	
	public JSONArray getResultArray()
	{
		return isError()?null:(isArray?(JSONArray)result:null);
	}
	
	public String getErrorType()
	{
		return error.errorType;
	}
	public String getError()
	{
		return error.errorMSG;
	}
}
