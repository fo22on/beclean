package ru.beclean.free.utils.rpc;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.Transformer;

public class RPCTransformer implements Transformer
{
	private OnAuthErrorListener mOnAuthErrorListener;
	@SuppressWarnings("unchecked")
	@Override
	public <T> T transform(String url, Class<T> type, String encoding, byte[] data, AjaxStatus status)
	{
		JSONObject object = null;
		RPCAnswer answer = null;
		try
		{
			 object = new JSONObject(new String(data));
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		if(object!=null)
		{
			answer = new RPCAnswer(object);
			if(answer.isError())
			{
				if(answer.getErrorType().equalsIgnoreCase("AuthError")&&mOnAuthErrorListener!=null)
				{
					mOnAuthErrorListener.onAuthError(answer);
				}
			}
		}
			
		return (T) answer;
	}
	
	public void setOnAuthErrorListener (OnAuthErrorListener onAuthErrorListener)
	{
		mOnAuthErrorListener = onAuthErrorListener;
	}
}
