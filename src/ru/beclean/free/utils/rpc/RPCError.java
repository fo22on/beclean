package ru.beclean.free.utils.rpc;

import org.json.JSONObject;

public class RPCError
{
	public String errorType;
	public String errorMSG;
	public JSONObject source;
	
	public RPCError(JSONObject o)
	{
		errorType = o.optString("type");
		errorMSG = o.optString("message");
		source = o;
	}
}
