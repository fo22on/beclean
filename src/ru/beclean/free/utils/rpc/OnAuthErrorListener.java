package ru.beclean.free.utils.rpc;

public interface OnAuthErrorListener
{
	void onAuthError(RPCAnswer a);
}
