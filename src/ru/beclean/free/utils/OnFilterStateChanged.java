package ru.beclean.free.utils;

public interface OnFilterStateChanged
{
	void onNewState(boolean isOpen);
}
