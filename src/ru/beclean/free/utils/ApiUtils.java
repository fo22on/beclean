package ru.beclean.free.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.androidquery.callback.AjaxCallback;

public class ApiUtils
{
	
	public static final String API_BASE_URL = "dev-beclean.me";
	public static final String API_URL = "http://mapi." + API_BASE_URL;
	public static final String API_URL_SKIN = "http://mapi." + API_BASE_URL;
	public static final String API_AUTHORIZATION_TITLE = "Authorization";
	public static final String API_METHOD_TITLE = "method";
	public static final String API_PARAMS_TITLE = "params";
	public static final String API_SESSION_TITLE = "Session ";
	public static final String API_URL_SERVICE_IMG = API_URL + "static/skins/android-phone/default/cw-detail/additional-services/";

	public static final String API_METHOD_CARWASH_ADD_TO_FAVORITE = "client.append_carwash";
	public static final String API_METHOD_CARWASH_REMOVE_FROM_FAVORITE = "client.remove_carwash";
	public static final String API_METHOD_CARWASHES_GET = "carwashes.get_details";
	public static final String API_METHOD_CARWASHES_SERVICES = "carwashes.get_services_by_cw_id";
	public static final String API_METHOD_CARWASHES_RATING = "carwashes.set_rating";
	public static final String API_METHOD_CARWASHES_ROTATOR = "carwashes.rotator";
	public static final String API_METHOD_CARWASHES_UPDATE_ROTATOR = "carwashes.update_rotations";
	
	public static final String API_METHOD_CLIENT_AUTH = "client.auth";
	public static final String API_METHOD_CLIENT_CAR_APPEND = "client.append_car";
	public static final String API_METHOD_CLIENT_CAR_REMOVE = "client.remove_car";
	public static final String API_METHOD_CLIENT_CAR_UPDATE = "client.update_car";
	public static final String API_METHOD_CLIENT_CARS_GET = "client.get_cars";
	public static final String API_METHOD_CLIENT_CREATE_PROFILE = "client.create_profile";
	public static final String API_METHOD_CLIENT_LOG_OUT = "client.logout";
	public static final String API_METHOD_CLIENT_NEW = "client.create";
	
	public static final String API_METHOD_REFERENCES_CAR_CLASSES = "references.car_classes";
	public static final String API_METHOD_REFERENCES_FILES_URL = "references.get_file_server_url";
	public static final String API_METHOD_REFERENCES_PHONE_SUPPORT = "references.get_call_center_phone";
	public static final String API_METHOD_REFERENCES_SERVICES = "references.services";
	public static final String API_METHOD_SEARCH_ADDRESS = "search.address";
	
	public static final String API_METHOD_RESERVATIONS_IN_PLACE = "reservations.in_place";
	public static final String API_METHOD_RESERVATIONS_REMOVE = "reservations.remove";
	public static final String API_METHOD_RESERVATIONS_SET = "reservations.set";

	public static JSONObject getParams(String method, Object... params)
	{
		JSONObject input = new JSONObject();
		try
		{
			input.putOpt(API_METHOD_TITLE, method);
			Object array = null;
			if (params == null || params.length == 0)
			{
				array = new JSONArray();
			}
			else if (params.length % 2 > 0)
			{
				if (JSONObject.class.isInstance(params[0])||JSONArray.class.isInstance(params[0]))
				{
					array = params[0];
				}
				else
				{
					array = new JSONArray();
					((JSONArray) array).put(params[0]);
				}
			}
			else
			{
				array = new JSONObject();
				for (int i = 0; i < params.length; i += 2)
				{
					((JSONObject) array).putOpt(params[i].toString(), params[i + 1]);
				}
			}
			input.putOpt(API_PARAMS_TITLE, array);
		} catch (JSONException e)
		{
			input = null;
		}
		return input;
	}

	public static void initAuth(Context context, AjaxCallback<?> callback)
	{
		String token = UtilsMethods.getToken(context);
		if (token != null && !token.equals(""))
		{
			callback.header(API_AUTHORIZATION_TITLE, API_SESSION_TITLE + token);
		}
	}
	
	public static String getServicePictureUrl(int id)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(API_URL_SERVICE_IMG);
		sb.append(id);
		sb.append(".png");
		return sb.toString();
	}
}
