package ru.beclean.free.db;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import ru.beclean.free.ui.map.model.CarWash;
import ru.beclean.free.ui.mywash.MyCarWash;
import ru.beclean.free.ui.servertypes.Reserv;
import ru.beclean.free.ui.servertypes.UserCar;
import ru.beclean.free.ui.servertypes.WashFull;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBUtils
{
	public static boolean isWashInFavorites(Context context, int id)
	{
		boolean out = false;
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		String sql = "SELECT count(*) FROM " + DBHelper.TABLE_NAME + " WHERE " + DBHelper.WASH_ID + " = " + id;
		Cursor cursor = db.rawQuery(sql, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast())
		{
			if (cursor.getInt(0) > 0)
			{
				out = true;
				break;
			}
			cursor.moveToNext();
		}

		cursor.close();
		db.close();

		return out;
	}

	public static void addWashToFavorites(Context context, CarWash cw)
	{
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();
		String sql = "INSERT INTO " + DBHelper.TABLE_NAME + " ( " + DBHelper.WASH_ID + ", " + DBHelper.WASH_NAME + ", "
				+ DBHelper.WASH_ADDRESS + ", " + DBHelper.COORDINATE_LATITUDE + ", " + DBHelper.COORDINATE_LONGETUDE
				+ ") VALUES (" + cw.id + ", '" + cw.name + "', '" + cw.address + ", '" + cw.lat + ", " + cw.lng + ")";

		db.execSQL(sql);

		db.close();
	}

	public static void addWashToFavorites(Context context, WashFull cw)
	{
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();
		String sql = "INSERT INTO " + DBHelper.TABLE_NAME + " ( " + DBHelper.WASH_ID + ", " + DBHelper.WASH_NAME + ", "
				+ DBHelper.WASH_ADDRESS + ", " + DBHelper.COORDINATE_LATITUDE + ", " + DBHelper.COORDINATE_LONGETUDE
				+ " ) VALUES ( " + cw.id + ", '" + cw.name + "',' " + cw.address + "', " + cw.lat + ", " + cw.lng + ")";

		db.execSQL(sql);

		db.close();
	}

	public static void removeWashFromFavorites(Context context, int cwId)
	{
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();
		String sql = "DELETE FROM " + DBHelper.TABLE_NAME + " WHERE " + DBHelper.WASH_ID + " = " + cwId;

		db.execSQL(sql);

		db.close();
	}

	public static ArrayList<MyCarWash> getFavoritesWashes(Context context)
	{
		ArrayList<MyCarWash> washes = new ArrayList<MyCarWash>();
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		String sql = "SELECT * FROM " + DBHelper.TABLE_NAME;
		Cursor cursor = db.rawQuery(sql, null);

		cursor.moveToFirst();

		while (!cursor.isAfterLast())
		{
			washes.add(new MyCarWash(cursor));
			cursor.moveToNext();
		}

		cursor.close();
		db.close();
		return washes;
	}

	public static String[] getFavoritesWashesIds(Context context)
	{
		ArrayList<String> washes = new ArrayList<String>();
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		String sql = "SELECT " + DBHelper.WASH_ID + " FROM " + DBHelper.TABLE_NAME;
		Cursor cursor = db.rawQuery(sql, null);

		cursor.moveToFirst();

		while (!cursor.isAfterLast())
		{
			washes.add("" + cursor.getInt(0));
			cursor.moveToNext();
		}

		cursor.close();
		db.close();
		return washes.toArray(new String[washes.size()]);
	}

	public static ArrayList<UserCar> getMyCars(Context context)
	{
		ArrayList<UserCar> cars = new ArrayList<UserCar>();
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		String sql = "SELECT * FROM " + DBHelper.TABLE_CARS_NAME;

		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast())
		{
			cars.add(new UserCar(cursor));
			cursor.moveToNext();
		}

		cursor.close();
		db.close();
		return cars;
	}

	public static UserCar getMyCar(Context context, int id)
	{
		UserCar car = null;
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		String sql = "SELECT * FROM " + DBHelper.TABLE_CARS_NAME + " WHERE " + DBHelper.CAR_ID + " = " + id;

		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();

		if (!cursor.isAfterLast())
		{
			car = new UserCar(cursor);
		}

		cursor.close();
		db.close();
		return car;
	}

	public static void saveMyCars(Context context, ArrayList<UserCar> cars)
	{
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		db.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_CARS_NAME);
		db.execSQL(DBHelper.CREATE_MY_CARS_TABLE);

		StringBuilder sb = new StringBuilder();
		for (UserCar uc : cars)
		{
			sb.append("INSERT INTO ");
			sb.append(DBHelper.TABLE_CARS_NAME);
			sb.append(" ( ");
			sb.append(DBHelper.CAR_ID);
			sb.append(", ");
			sb.append(DBHelper.CAR_CLASS_ID);
			sb.append(", ");
			sb.append(DBHelper.CAR_REMOVE_BUNDLE);
			sb.append(", ");
			sb.append(DBHelper.CAR_NAME);
			sb.append(", ");
			sb.append(DBHelper.CAR_NUMBER);
			sb.append(" ) VALUES ( ");
			sb.append(uc.id);
			sb.append(", ");
			sb.append(uc.car_class_id);
			sb.append(", ");
			sb.append(uc.is_removed ? "1" : "0");
			sb.append(", '");
			sb.append(uc.name);
			sb.append("', '");
			sb.append(uc.car_number);
			sb.append("')");

			db.execSQL(sb.toString());
			sb.setLength(0);
		}

		db.close();
	}

	public static void saveCar(Context context, UserCar uc)
	{
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		db.execSQL("DELETE FROM " + DBHelper.TABLE_CARS_NAME + " WHERE " + DBHelper.CAR_ID + " = " + uc.id);

		StringBuilder sb = new StringBuilder();

		sb.append("INSERT INTO ");
		sb.append(DBHelper.TABLE_CARS_NAME);
		sb.append(" ( ");
		sb.append(DBHelper.CAR_ID);
		sb.append(", ");
		sb.append(DBHelper.CAR_CLASS_ID);
		sb.append(", ");
		sb.append(DBHelper.CAR_REMOVE_BUNDLE);
		sb.append(", ");
		sb.append(DBHelper.CAR_NAME);
		sb.append(", ");
		sb.append(DBHelper.CAR_NUMBER);
		sb.append(" ) VALUES ( ");
		sb.append(uc.id);
		sb.append(", ");
		sb.append(uc.car_class_id);
		sb.append(", ");
		sb.append(uc.is_removed ? "1" : "0");
		sb.append(", '");
		sb.append(uc.name);
		sb.append("', '");
		sb.append(uc.car_number);
		sb.append("')");

		db.execSQL(sb.toString());

		db.close();
	}

	public static void deleteCar(Context context, int carId)
	{
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		db.execSQL("DELETE FROM " + DBHelper.TABLE_CARS_NAME + " WHERE " + DBHelper.CAR_ID + " = " + carId);

		db.close();
	}
	
	public static void deletAllCars(Context context)
	{
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();
		
		db.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_CARS_NAME);
		db.execSQL(DBHelper.CREATE_MY_CARS_TABLE);
		
		db.close();
	}

	public static void deleteReserv(Context context, int id)
	{
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		db.execSQL("DELETE FROM " + DBHelper.TABLE_RESERV_NAME + " WHERE " + DBHelper.RESERV_ID + " = " + id);

		db.close();
	}

	public static void addReserv(Context context, Reserv r)
	{
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		db.execSQL("INCERT INTO " + DBHelper.TABLE_RESERV_NAME + " (" + DBHelper.RESERV_ID + ", "
				+ DBHelper.RESERV_DATA + ") VALUES (" + r.id + ", " + r.getSource() + ")");

		db.close();
	}

	public static Reserv getReserv(Context context)
	{
		Reserv reserv = null;
		DBHelper helper = new DBHelper(context);
		SQLiteDatabase db = helper.getReadableDatabase();

		String sql = "SELECT " + DBHelper.RESERV_DATA + " FROM " + DBHelper.TABLE_RESERV_NAME;
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();
		if (!cursor.isAfterLast())
		{
			try
			{
				reserv = new Reserv(new JSONObject(cursor.getString(0)));
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

		cursor.close();
		db.close();

		return reserv;
	}
}
