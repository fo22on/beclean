package ru.beclean.free.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper
{
	private final static String DB_NAME = "gsvcdf";
	private final static int DB_VERSION = 1;
	
	public final static String TABLE_NAME = "gomhet";
	public final static String TABLE_CARS_NAME = "vsjket";
	public final static String TABLE_RESERV_NAME = "otinuf";
	
	public final static String FIELD_ID = "_id";
	public final static String WASH_ID = "oba";
	public final static String WASH_NAME = "srus";
	public final static String WASH_ADDRESS = "iapi";
	public final static String COORDINATE_LATITUDE = "rag";
	public final static String COORDINATE_LONGETUDE = "ami";
	
	public final static String CAR_ID = "gfd";
	public final static String CAR_CLASS_ID = "gpsfd";
	public final static String CAR_REMOVE_BUNDLE = "xsssd";
	public final static String CAR_NAME = "corybl";
	public final static String CAR_NUMBER = "xtenf";
	
	public final static String RESERV_ID = "treng";
	public final static String RESERV_DATA = "xjenwg";
	
	public final static String CREATE_FAVORITE_WASHES_TABLE = "CREATE TABLE " + TABLE_NAME + " (" 
			+ FIELD_ID + " INTEGER PRIMARY KEY, " 
			+ WASH_ID + " INTEGER, "
			+ WASH_NAME + " TEXT, "
			+ WASH_ADDRESS + " TEXT, "
			+ COORDINATE_LATITUDE + " FLOAT, "
			+ COORDINATE_LONGETUDE + " FLOAT)";
	
	public final static String CREATE_MY_CARS_TABLE = "CREATE TABLE " + TABLE_CARS_NAME + " ("
			+ FIELD_ID + " INTEGER PRIMARY KEY, "
			+ CAR_ID + " INTEGER, "
			+ CAR_CLASS_ID + " INTEGER, "
			+ CAR_REMOVE_BUNDLE + " INTEGER, "
			+ CAR_NAME + " TEXT, "
			+ CAR_NUMBER + " TEXT)";
	public final static String CREATE_RESERV_TABLE =  "CREATE TABLE " + TABLE_RESERV_NAME + " ("
			+ FIELD_ID + " INTEGER PRIMARY KEY, "
			+ RESERV_ID + " INTEGER, "
			+ RESERV_DATA + " TEXT)";

	public DBHelper(Context context)
	{
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(CREATE_FAVORITE_WASHES_TABLE);
		db.execSQL(CREATE_MY_CARS_TABLE);
		db.execSQL(CREATE_RESERV_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARS_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESERV_NAME);
		onCreate(db);
	}
}