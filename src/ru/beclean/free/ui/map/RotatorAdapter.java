package ru.beclean.free.ui.map;

import java.util.ArrayList;

import com.androidquery.AQuery;

import ru.beclean.free.R;
import ru.beclean.free.ui.map.model.CarWash;
import ru.beclean.free.utils.UtilsMethods;
import android.content.Context;
import android.location.Location;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class RotatorAdapter extends PagerAdapter implements OnClickListener
{
	private ArrayList<CarWash> mItems;
	private LayoutInflater inflater;
	public static final int ITEMS_BEFORE = 2;
	private OnWashItemClickListener mOnWashItemClickListener = null;
	private Location currentLocation;

	public RotatorAdapter(Context context, ArrayList<CarWash> items, Location current)
	{
		inflater = LayoutInflater.from(context);
		mItems = items;
		currentLocation = current;
	}
	
	public void setLocation(Location current)
	{
		currentLocation = current;
		notifyDataSetChanged();
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position)
	{
		position = getRealPositin(position);
		CarWash item = mItems.get(position);
		View v = inflater.inflate(R.layout.rotator_item_layout, container, false);
		v.setTag(position);
		v.setOnClickListener(this);

		AQuery aq = new AQuery(v);
		aq.id(R.id.titleTextView).text(item.name);
		aq.id(R.id.descriptionTextView).text(item.address);
		aq.id(R.id.ratingBar).rating(item.rating);
		aq.id(R.id.distanceTextView).text(UtilsMethods.getDistance(item.location, currentLocation));
		container.addView(v, 0);
		return v;
	}

	@Override
	public int getCount()
	{
		return mItems.size() + ITEMS_BEFORE * 2;
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1)
	{
		return arg0.equals(arg1);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object)
	{
		container.removeView((View) object);
	}

	@Override
	public int getItemPosition(Object object)
	{
		return POSITION_NONE;
	}

	private int getRealPositin(int position)
	{
		int out;
		int length = mItems.size();
		if (position < ITEMS_BEFORE)
		{
			out = length - ITEMS_BEFORE + position;
		}
		else if (position < length + ITEMS_BEFORE)
		{
			out = position - ITEMS_BEFORE;
		}
		else
		{
			out = position - length - ITEMS_BEFORE;
		}

		if (out < 0)
		{
			out = 0;
		}
		else if (out >= length)
		{
			out = length - 1;
		}

		return out;
	}

	public void setOnWashItemClickListener(OnWashItemClickListener mOnWashItemClickListener)
	{
		this.mOnWashItemClickListener = mOnWashItemClickListener;
	}

	@Override
	public void onClick(View v)
	{
		Integer pos = (Integer) v.getTag();
		if(mOnWashItemClickListener!=null)
		{
			mOnWashItemClickListener.onWashClick(pos.intValue());
		}
	}
}
