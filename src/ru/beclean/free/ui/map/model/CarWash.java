package ru.beclean.free.ui.map.model;

import java.util.ArrayList;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.beclean.free.db.DBHelper;
import android.database.Cursor;
import android.location.Location;

public class CarWash
{
	public int id; // "id"
	public int cityId; // "city_id"
	public String[] photos; // "photos"
	public Object[] servics; // "services"
	public String address; // "address"
	public String name; // "name"
	public double sumPrice; // "sum_prices"
	public double lng; // "lng"
	public double lat; // "lat"
	public ArrayList<Reservation> reservations; // "reservations"
	public Location location;
	public float rating;

	/*
	 * { "id": 44, "city_id": 1, "photos": [ ], "services": [], "address":
	 * "����� �������� �����, 57�1", "name": "���������", "sum_prices": 300,
	 * "lng": 37.616863, "lat": 55.700958, "reservations":[["2014-08-19 06:50:00","2014-08-19 07:23:00"]]},
	 */
	//TODO �������� ������� � �������
	public CarWash(JSONObject object)
	{
		id = object.optInt("id");
		cityId = object.optInt("city_id");
		JSONArray photosArray = object.optJSONArray("photos");
		if (photosArray != null && photosArray.length() > 0)
		{
			photos = new String[photosArray.length()];
			for (int i = 0; i < photos.length; i++)
			{
				photos[i] = photosArray.optString(i);
			}
		}
		address = object.optString("address");
		name = object.optString("name");
		sumPrice = object.optDouble("sum_prices");
		lng = object.optDouble("lng");
		lat = object.optDouble("lat");
		location = new Location(name);
		location.setLatitude(lat);
		location.setLongitude(lng);
		rating = (float) object.optDouble("rating", 0);
		reservations = new ArrayList<Reservation>();
		JSONArray reservationArray = object.optJSONArray("reservations");
		for (int i = 0; i < reservationArray.length(); i++)
		{
			reservations.add(new Reservation(reservationArray.optJSONArray(i)));
		}
	}

	public CarWash(Cursor cursor)
	{
		int fielsWashId = cursor.getColumnIndex(DBHelper.WASH_ID);
		int fielsWashName = cursor.getColumnIndex(DBHelper.WASH_NAME);
		int fielsWashAddress = cursor.getColumnIndex(DBHelper.WASH_ADDRESS);
		int fielsWashLat = cursor.getColumnIndex(DBHelper.COORDINATE_LATITUDE);
		int fielsWashLng = cursor.getColumnIndex(DBHelper.COORDINATE_LONGETUDE);
		
		id = cursor.getInt(fielsWashId);
		name = cursor.getString(fielsWashName);
		address = cursor.getString(fielsWashAddress);
		
		lng = cursor.getDouble(fielsWashLng);
		lat = cursor.getDouble(fielsWashLat);
		location = new Location(name);
		location.setLatitude(lat);
		location.setLongitude(lng);
	}

	public static class CarWashDistanceComparator implements Comparator<CarWash>
	{
		private Location currentLocation;

		public CarWashDistanceComparator(Location current)
		{
			currentLocation = current;
		}

		@Override
		public int compare(CarWash lhs, CarWash rhs)
		{
			Float l = currentLocation.distanceTo(lhs.location);
			Float r = currentLocation.distanceTo(rhs.location);
			return l.compareTo(r);
		}

	}

	public static class CarWashPriceComparator implements Comparator<CarWash>
	{
		@Override
		public int compare(CarWash lhs, CarWash rhs)
		{
			return Double.compare(lhs.sumPrice, rhs.sumPrice);
		}
	}

	public static class CarWashTimeComparator implements Comparator<CarWash>
	{
		@Override
		public int compare(CarWash lhs, CarWash rhs)
		{
			return Double.compare(lhs.sumPrice, rhs.sumPrice);
		}
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o==null)
		{
			return false;
		}
		if(CarWash.class.isInstance(o))
		{
			return this.id==((CarWash)o).id;
		}
		else if(Integer.class.isInstance(o))
		{
			return this.id==((Integer)o).intValue();
		}
		return false;
	}
}
