package ru.beclean.free.ui.map.model;

import org.json.JSONArray;

import ru.beclean.free.utils.UtilsMethods;

public class Reservation
{
	public long start;
	public long end;
	
	public Reservation(JSONArray o)
	{
		start = UtilsMethods.getDateFromString(o.optString(0));
		end = UtilsMethods.getDateFromString(o.optString(1));
	}
}
