package ru.beclean.free.ui.map;

import java.util.ArrayList;

import ru.beclean.free.R;
import ru.beclean.free.ui.servertypes.SearchItem;
import ru.beclean.free.utils.UtilsMethods;
import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SearchResultAdapter extends BaseAdapter
{
	private ArrayList<SearchItem> mItems;
	private LayoutInflater inflater;
	private Location currentPosition;
	
	public SearchResultAdapter(Context context, ArrayList<SearchItem> items, Location current)
	{
		inflater = LayoutInflater.from(context);
		mItems = items;
		currentPosition = current;
	}
	
	@Override
	public int getCount()
	{
		return mItems.size();
	}

	@Override
	public SearchItem getItem(int position)
	{
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		SearchItem item = getItem(position);
		ViewHolder holder;
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.item_search_result, parent, false);
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.titleTextView);
			holder.description = (TextView) convertView.findViewById(R.id.descriptionTextView);
			holder.distance = (TextView) convertView.findViewById(R.id.distanceTextView);
			
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.title.setText(item.name);
		holder.description.setText(item.description);
		holder.distance.setText(UtilsMethods.getDistance(item.location, currentPosition));
		
		return convertView;
	}

	private static class ViewHolder 
	{
		TextView title;
		TextView description;
		TextView distance;
	}
}
