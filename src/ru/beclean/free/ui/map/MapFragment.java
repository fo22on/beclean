package ru.beclean.free.ui.map;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.db.DBUtils;
import ru.beclean.free.ui.MainActivity;
import ru.beclean.free.ui.map.model.CarWash;
import ru.beclean.free.ui.parent.ParentActivity;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.servertypes.SearchItem;
import ru.beclean.free.ui.servertypes.UserCar;
import ru.beclean.free.ui.wash.WashDescriptionFragment;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.FilterUtils;
import ru.beclean.free.utils.JsonParamsCreator;
import ru.beclean.free.utils.OnBackPressedListener;
import ru.beclean.free.utils.OnFilterStateChanged;
import ru.beclean.free.utils.UtilsMethods;
import ru.beclean.free.utils.rpc.RPCAnswer;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.androidmapsextensions.ClusterOptions;
import com.androidmapsextensions.ClusterOptionsProvider;
import com.androidmapsextensions.ClusteringSettings;
import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.GoogleMap.OnCameraChangeListener;
import com.androidmapsextensions.GoogleMap.OnMarkerClickListener;
import com.androidmapsextensions.GoogleMap.OnMyLocationChangeListener;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.SupportMapFragment;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.wallet.l;

public class MapFragment extends ParentFragment implements OnClickListener, OnMyLocationChangeListener,
		OnCameraChangeListener, OnMarkerClickListener, OnWashItemClickListener
{
	private AQuery aq;
	private GoogleMap map;

	private final static float myLocationCurrentZoom = 13;

	private ViewPager mViewPager;
	private RotatorAdapter mAdapter;
	private LinearLayout searchPanelLinearLayout;

	private ArrayList<CarWash> mItems;
	private ArrayList<Marker> markers = new ArrayList<Marker>();
	private int lastSelectedMarker = -1;

	private SupportMapFragment fragment;

	private boolean isSearchShown = false;
	private EditText searchEditText;
	private long lastButtonClickMills = 0;
	
	private SensorManager mSensorManager;
	private Sensor mCompass;

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		FragmentManager fm = getChildFragmentManager();
		fragment = (SupportMapFragment) fm.findFragmentById(R.id.mapContainer);
		if (fragment == null)
		{
			fragment = SupportMapFragment.newInstance();
			fm.beginTransaction().replace(R.id.mapContainer, fragment).commit();
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		 mSensorManager = (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);
		 mCompass = mSensorManager.getDefaultSensor(Sensor.TYPE_AX);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_map, container, false);
		aq = new AQuery(rootView);

		aq.id(R.id.zoomInImageButton).clicked(this);
		aq.id(R.id.zoomOutImageButton).clicked(this);
		aq.id(R.id.myLocationImageButton).clicked(this);
		aq.id(R.id.filterImageButton).clicked(this);
		aq.id(R.id.menuButton).clicked(this);
		aq.id(R.id.searchCancelButton).clicked(this);

		aq.id(R.id.searchResultListView).itemClicked(searchResultListener);
		aq.id(R.id.filterImageButton).clicked(this);
		searchPanelLinearLayout = (LinearLayout) aq.id(R.id.searchPanelLinearLayout).clicked(this).getView();

		searchEditText = aq.id(R.id.searchTextView).clicked(this).getEditText();

		searchEditText.setOnFocusChangeListener(searchFocusListener);
		searchEditText.setOnEditorActionListener(searchEditorListener);
		searchEditText.addTextChangedListener(searchTextWatcher);

		mViewPager = (ViewPager) aq.id(R.id.viewPager).getView();
		mViewPager.setVisibility(View.GONE);
		mViewPager.setOnPageChangeListener(new OnPageChangeListener()
		{
			private int currentPosition = -1;

			@Override
			public void onPageSelected(int position)
			{
				currentPosition = position;
				Log.w("onPageSelected", "" + position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2)
			{
			}

			@Override
			public void onPageScrollStateChanged(int state)
			{
				if (state == ViewPager.SCROLL_STATE_IDLE)
				{
					int position = currentPosition;
					if (currentPosition == 1)
					{
						position = mAdapter.getCount() - RotatorAdapter.ITEMS_BEFORE - 1;
					}
					else if (currentPosition == mAdapter.getCount() - RotatorAdapter.ITEMS_BEFORE)
					{
						position = RotatorAdapter.ITEMS_BEFORE;
					}
					try
					{
						mViewPager.setCurrentItem(position, false);
					} catch (Exception e)
					{}

					if (lastSelectedMarker != -1)
					{
						markers.get(lastSelectedMarker).setIcon(UtilsMethods.getIcon(R.drawable.blue_pin));
					}
					lastSelectedMarker = position - RotatorAdapter.ITEMS_BEFORE;
					markers.get(lastSelectedMarker).setIcon(UtilsMethods.getIcon(R.drawable.yellow_pin));

				}
			}
		});
		mViewPager.setPageMargin(-getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin));

		return rootView;
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (map == null)
		{
			map = fragment.getExtendedMap();
			map.setMyLocationEnabled(true);
			map.animateCamera(UtilsMethods.getDefaultMapPosition());

			map.getUiSettings().setZoomControlsEnabled(false);
			map.getUiSettings().setMyLocationButtonEnabled(false);
			map.setClustering(new ClusteringSettings().clusterOptionsProvider(new ClusterOptionsProvider()
			{
				@Override
				public ClusterOptions getClusterOptions(List<Marker> markers)
				{
					BitmapDescriptor blueIcon = UtilsMethods.getIcon(getResources(), R.drawable.blue_pin_more,
							markers.size());
					return new ClusterOptions().icon(blueIcon);
				}
			}));

			map.setOnMyLocationChangeListener(this);
			map.setOnCameraChangeListener(this);
			map.setOnMarkerClickListener(this);
		}
		((ParentActivity) getActivity()).addBackPressListener(onBackPressedListener);
		((MainActivity) getActivity()).setOnFilterStateChanged(mOnFilterStateChanged);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		((ParentActivity) getActivity()).removeBackPressListener(onBackPressedListener);
		((MainActivity) getActivity()).removeOnFilterStateChanged(mOnFilterStateChanged);
	}

	private int lastSearchSize;
	private int lastSearchPos;

	private OnEditorActionListener searchEditorListener = new OnEditorActionListener()
	{
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
		{
			boolean out = false;
			if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE
					|| (actionId == EditorInfo.IME_ACTION_UNSPECIFIED && event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
			{
				downloadSearchData(v.getText().toString());
				out = true;
			}
			return out;
		}
	};

	private OnFocusChangeListener searchFocusListener = new OnFocusChangeListener()
	{
		@Override
		public void onFocusChange(View v, boolean hasFocus)
		{
			if (hasFocus && !isSearchShown)
			{
				toggleSearch();
			}
		}
	};

	private TextWatcher searchTextWatcher = new TextWatcher()
	{
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count)
		{
			if (count > 2)
			{
				downloadSearchData(s.toString());
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after)
		{
		}

		@Override
		public void afterTextChanged(Editable s)
		{
		}
	};

	@Override
	public void onClick(View v)
	{
		if(lastButtonClickMills+getResources().getInteger(R.integer.animation_duration)>System.currentTimeMillis()) return;
		float currentZoom = map.getCameraPosition().zoom;
		switch (v.getId())
		{
			case R.id.zoomInImageButton:
			{
				if ((currentZoom + 1) <= map.getMaxZoomLevel())
				{
					currentZoom++;
					animateZoom(currentZoom, null);
				}
				break;
			}
			case R.id.zoomOutImageButton:
			{
				if ((currentZoom - 1) >= UtilsMethods.getMapMinZoom())
				{
					currentZoom--;
					animateZoom(currentZoom, null);
				}
				break;
			}
			case R.id.myLocationImageButton:
			{
				Location myLocation = map.getMyLocation();

				if (myLocation != null)
				{
					LatLng myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
					if (currentZoom < myLocationCurrentZoom)
					{
						currentZoom = myLocationCurrentZoom;
					}
					map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, currentZoom));
				}
				break;
			}
			case R.id.filterImageButton:
			{
				((MainActivity) getActivity()).toggleFilter();
				break;
			}
			case R.id.menuButton:
			{
				toggleMenu();
				break;
			}

			case R.id.searchTextView:
			case R.id.searchPanelLinearLayout:
			{
				toggleSearch();
				break;
			}
			
			case R.id.searchCancelButton:
			{
				searchEditText.setText("");
				toggleSearch();
				break;
			}
			default:
				break;
		}
		
		lastButtonClickMills = System.currentTimeMillis();
	}

	private void toggleSearch()
	{
		AnimatorSet set = new AnimatorSet();
		int duration = getResources().getInteger(R.integer.animation_duration);
		Interpolator interpolator = AnimationUtils.loadInterpolator(getActivity(),
				android.R.anim.accelerate_decelerate_interpolator);
		int searchTextViewSize;
		if (isSearchShown)
		{
			searchTextViewSize = lastSearchSize;
		}
		else
		{
			searchTextViewSize = getResources().getDisplayMetrics().widthPixels;
			lastSearchSize = searchPanelLinearLayout.getWidth();
			lastSearchPos = searchPanelLinearLayout.getLeft();
		}

		float startAlpha = isSearchShown ? 1f : 0f;
		float endAlpha = isSearchShown ? 0f : 1f;
		ValueAnimator animator = ValueAnimator.ofFloat(searchPanelLinearLayout.getWidth(), searchTextViewSize);
		animator.setDuration(duration);
		animator.setInterpolator(new AccelerateDecelerateInterpolator());
		animator.addUpdateListener(new AnimatorUpdateListener()
		{
			@Override
			public void onAnimationUpdate(ValueAnimator animation)
			{
				Float value = (Float) animation.getAnimatedValue();
				LayoutParams params = searchPanelLinearLayout.getLayoutParams();
				params.width = value.intValue();
				searchPanelLinearLayout.setLayoutParams(params);
			}
		});

		final int toX = isSearchShown ? lastSearchPos : searchPanelLinearLayout.getLeft() + ((isSearchShown) ? -1 : +1)
				* getResources().getDimensionPixelSize(R.dimen.search_text_margin);
		final ObjectAnimator xAnimator = ObjectAnimator.ofFloat(searchPanelLinearLayout, "x",
				searchPanelLinearLayout.getLeft(), toX);
		xAnimator.setDuration(duration / 4);
		xAnimator.setInterpolator(interpolator);
		xAnimator.addListener(new AnimatorListener()
		{
			@Override
			public void onAnimationStart(Animator animation)
			{
			}

			@Override
			public void onAnimationRepeat(Animator animation)
			{
			}

			@Override
			public void onAnimationEnd(Animator animation)
			{
				if (isSearchShown)
				{
					aq.id(R.id.searchCancelButton).visibility(View.VISIBLE);
					searchEditText.requestFocus();
					// imm.showSoftInput(editText,
					// InputMethodManager.SHOW_FORCED);
					searchEditText.postDelayed(new Runnable()
					{
						@Override
						public void run()
						{
							InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
									Context.INPUT_METHOD_SERVICE);
							imm.showSoftInput(searchEditText, 0);
						}
					}, 200);
				}
				else
				{
					aq.id(R.id.searchCancelButton).visibility(View.GONE).getView().requestFocus();
					searchEditText.clearFocus();
					searchEditText.postDelayed(new Runnable()
					{
						@Override
						public void run()
						{
							InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
									Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
							searchEditText.clearFocus();
						}
					}, 200);
				}
			}

			@Override
			public void onAnimationCancel(Animator animation)
			{
			}
		});

		ObjectAnimator alpha = ObjectAnimator.ofFloat(aq.id(R.id.searchLinearLayout).getView(), "alpha", startAlpha,
				endAlpha);
		alpha.setDuration(duration);
		alpha.setInterpolator(interpolator);

		ObjectAnimator filterAlpha = ObjectAnimator.ofFloat(aq.id(R.id.filterImageButton).getView(), "alpha", endAlpha,
				startAlpha);
		filterAlpha.setDuration(duration);
		filterAlpha.setInterpolator(interpolator);

		ObjectAnimator cancelAlpha = ObjectAnimator.ofFloat(aq.id(R.id.searchCancelButton).getView(), "alpha",
				startAlpha, endAlpha);
		cancelAlpha.setDuration(duration);
		cancelAlpha.setInterpolator(interpolator);

		if (isSearchShown)
		{
			AnimatorSet start = new AnimatorSet();
			start.playTogether(animator, alpha, filterAlpha, cancelAlpha);
			start.addListener(new AnimatorListener()
			{

				@Override
				public void onAnimationStart(Animator animation)
				{
	
				}

				@Override
				public void onAnimationRepeat(Animator animation)
				{
				}

				@Override
				public void onAnimationEnd(Animator animation)
				{
					aq.id(R.id.searchCancelButton).visibility(View.GONE);
					xAnimator.setFloatValues(searchPanelLinearLayout.getLeft(), toX);
					xAnimator.start();
					aq.id(R.id.searchLinearLayout).visibility(View.GONE);
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{
				}
			});
			set.play(start);
		}
		else
		{
			set.play(animator).with(alpha).with(filterAlpha).with(cancelAlpha).after(xAnimator);
			set.addListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationStart(Animator animation)
				{
					aq.id(R.id.searchLinearLayout).visibility(View.VISIBLE);
				}
			});
		}
		set.start();
		isSearchShown = !isSearchShown;
	}

	private void animateZoom(float zoom, LatLng point)
	{
		if (point == null)
		{
			map.animateCamera(CameraUpdateFactory.zoomTo(zoom));
		}
		else
		{
			map.animateCamera(CameraUpdateFactory.newLatLngZoom(point, zoom));
		}
	}

	private Location currentLocation = null;

	@Override
	public void onMyLocationChange(Location location)
	{
		((BeApp) getActivity().getApplication()).setCurrentLocation(location);
		currentLocation = location;
		if(mAdapter!=null)
		{
			mAdapter.setLocation(location);
		}
	}

	private float lastZoom = -1;

	@Override
	public void onCameraChange(CameraPosition cameraPos)
	{
		if (lastZoom != cameraPos.zoom || cameraPos.zoom - UtilsMethods.getMapMinZoom() > 2)
		{
			downloadRotator(cameraPos);
		}
		lastZoom = cameraPos.zoom;

	}

	private void downloadRotator(CameraPosition cameraPos)
	{
		if (!isAdded())
		{
			return;
		}
		JsonParamsCreator creator = new JsonParamsCreator();
		LatLng leftTop = map.getProjection().fromScreenLocation(new Point(0, 0));
		LatLng rightBottom = map.getProjection().fromScreenLocation(
				new Point(getResources().getDisplayMetrics().widthPixels,
						getResources().getDisplayMetrics().heightPixels));
		JSONArray leftTopPoint = new JSONArray();
		JSONArray rightBottomPoint = new JSONArray();
		try
		{
			leftTopPoint.put(leftTop.longitude);
			leftTopPoint.put(leftTop.latitude);
			rightBottomPoint.put(rightBottom.longitude);
			rightBottomPoint.put(rightBottom.latitude);

		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		UserCar car = DBUtils.getMyCar(getActivity(), UtilsMethods.getUserCarCategory(getActivity()));
		creator.addParam("geo_square", leftTopPoint, rightBottomPoint).addParam("car_class_id", car.car_class_id);
		if (FilterUtils.getFilterTimeEnd(getActivity()) != 0)
		{
			creator.addParam("free_time", FilterUtils.getFilterFormattedTime(getActivity(), 0),
					FilterUtils.getFilterFormattedTime(getActivity(), 1));
		}
		int[] services = FilterUtils.getServices(getActivity()); 
		if(services!=null&&services.length>0)
		{
			JSONArray array = new JSONArray();
			for (int service : services)
			{
				array.put(service);
			}
			creator.addParam("services_list", array);
			
			creator.addParam("prices_list", FilterUtils.getPriceList(getActivity()));
		}
		
		JSONObject params = creator.getParams();

		JSONObject rotatorParams = ApiUtils.getParams(ApiUtils.API_METHOD_CARWASHES_ROTATOR, params);
		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer object, AjaxStatus status)
			{
				if (getActivity() == null || isDetached())
				{
					return;
				}
				if (object == null || object.isError())
				{
					return;
				}
				ArrayList<CarWash> washes = new ArrayList<CarWash>();
				JSONArray array = object.getResultArray();

				if (array != null && array.length() > 0)
				{
					for (Marker marker : markers)
					{
						marker.remove();
					}
					markers.clear();
					for (int i = 0; i < array.length(); i++)
					{
						CarWash carWash = new CarWash(array.optJSONObject(i));
						washes.add(carWash);
						markers.add(map.addMarker(new MarkerOptions().position(new LatLng(carWash.lat, carWash.lng))
								.title(carWash.name).icon(UtilsMethods.getIcon(R.drawable.blue_pin)).data(i)));
					}

					mAdapter = new RotatorAdapter(getActivity(), washes, ((BeApp) getActivity().getApplication()).getCurrentPosition());
					mAdapter.setOnWashItemClickListener(MapFragment.this);
					lastSelectedMarker = -1;
					mViewPager.setAdapter(mAdapter);
					mItems = null;
					mItems = washes;
					if (!markerClick)
					{
						mViewPager.setCurrentItem(RotatorAdapter.ITEMS_BEFORE);
						if (mViewPager.getVisibility() != View.VISIBLE)
						{
							mViewPager.setVisibility(View.VISIBLE);
						}
					}
					else
					{
						markerClick = false;
					}
				}
			}
		};
		ApiUtils.initAuth(getActivity(), cb);
		aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, rotatorParams,
				RPCAnswer.class, cb);
	}

	private boolean markerClick = false;

	@Override
	public boolean onMarkerClick(Marker marker)
	{
		// map.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(),
		// map.getCameraPosition().zoom));
		if (marker.getData() != null)
		{
			int position = (Integer) marker.getData();
			if (mAdapter != null && mViewPager.getAdapter() == null)
			{
				mViewPager.setAdapter(mAdapter);
			}
			if (mViewPager.getVisibility() != View.VISIBLE)
			{
				mViewPager.setVisibility(View.VISIBLE);
			}
			mViewPager.setCurrentItem(position + RotatorAdapter.ITEMS_BEFORE, true);
			markerClick = true;
		}
		else
		{
			animateZoom(map.getCameraPosition().zoom + 1F, marker.getPosition());
		}

		return true;
	}

	@Override
	public void onWashClick(int position)
	{
		FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
		WashDescriptionFragment wf = new WashDescriptionFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(WashDescriptionFragment.WASH_ID, mItems.get(position).id);
		wf.setArguments(bundle);
		wf.setReserves(mItems.get(position).reservations);

		ft.addToBackStack(MapFragment.class.getName());
		ft.replace(((MainActivity) getActivity()).getContainerID(), wf);
		ft.commit();
	}

	private void downloadSearchData(String s)
	{
		String[] data = UtilsMethods.SplitUsingTokenizer(s, " ");
		JsonParamsCreator creator = new JsonParamsCreator();
		creator.addParam("search", (Object[]) data);
		JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_SEARCH_ADDRESS, creator.getParams());

		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer object, AjaxStatus status)
			{
				super.callback(url, object, status);
				if (!object.isError())
				{
					ArrayList<SearchItem> items = new ArrayList<SearchItem>();
					JSONArray array = object.getResultArray();
					if (array != null)
					{
						for (int i = 0; i < array.length(); i++)
						{
							items.add(new SearchItem(array.optJSONObject(i)));
						}
					}
					SearchResultAdapter adapter = new SearchResultAdapter(getActivity(), items, currentLocation);
					aq.id(R.id.searchResultListView).adapter(adapter);
				}
			}
		};

		ApiUtils.initAuth(getActivity(), cb);
		aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, params,
				RPCAnswer.class, cb);
	}

	private OnBackPressedListener onBackPressedListener = new OnBackPressedListener()
	{
		@Override
		public boolean onBackPressed()
		{
			boolean out = false;
			if (isSearchShown)
			{
				out = true;
				toggleSearch();
			}
			return out;
		}
	};

	private OnItemClickListener searchResultListener = new OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			SearchItem item = (SearchItem) parent.getAdapter().getItem(position);
			LatLngBounds bounds = new LatLngBounds(item.lowerCorner, item.upperCorner);
			map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));
			toggleSearch();
		}
	};
	
	private OnFilterStateChanged mOnFilterStateChanged = new OnFilterStateChanged()
	{
		@Override
		public void onNewState(boolean isOpen)
		{
			if(!isOpen&&map!=null)
			{
				downloadRotator(map.getCameraPosition());
			}
		}
	};
}
