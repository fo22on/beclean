package ru.beclean.free.ui.map;

import org.json.JSONObject;

public class WorkTime
{
	public String startTime;
	public String endTime;
	public boolean isWeekEnd;
	
	public WorkTime (JSONObject object)
	{
		isWeekEnd = object.optBoolean("is_weekend");
		int h_from, m_from, h_to, m_to;
		h_from = object.optInt("h_from");
		m_from = object.optInt("m_from");
		h_to = object.optInt("h_to");
		m_to = object.optInt("m_to");
		
		setStartTme(h_from, m_from);
		setEndTime(h_to, m_to);
	}

	private void setEndTime(int h_to, int m_to)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(h_to);
		sb.append(":");
		if(m_to<10)
		{
			sb.append("0");
			sb.append(m_to);
		}
		else
		{
			sb.append(m_to);
		}
		endTime = sb.toString();
	}

	private void setStartTme(int h_from, int m_from)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(h_from);
		sb.append(":");
		if(m_from<10)
		{
			sb.append("0");
			sb.append(m_from);
		}
		else
		{
			sb.append(m_from);
		}
		startTime = sb.toString();
	}
	public String toString ()
	{
		return startTime + " - " + endTime;
	}
}
