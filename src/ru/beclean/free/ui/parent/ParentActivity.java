package ru.beclean.free.ui.parent;

import java.util.ArrayList;

import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.utils.OnBackPressedListener;
import android.support.v4.app.FragmentActivity;

public class ParentActivity extends BaseActivity
{
	private ArrayList<OnBackPressedListener> mBackPressedListeners = new ArrayList<OnBackPressedListener>();
	
	@Override
	public void onBackPressed()
	{
		for (OnBackPressedListener listener : mBackPressedListeners)
		{
			if(listener.onBackPressed())
			{
				return;
			}
		}
		if(getSupportFragmentManager().getBackStackEntryCount()>0)
		{
			getSupportFragmentManager().popBackStack();
		}
		else
		{
			super.onBackPressed();
		}
	}
	
	public void addBackPressListener(OnBackPressedListener listener)
	{
		mBackPressedListeners.add(listener);
	}
	
	public void removeBackPressListener(OnBackPressedListener listener)
	{
		mBackPressedListeners.remove(listener);
	}

	@Override
	public int getContainerID()
	{
		return R.id.container;
	}
}
