package ru.beclean.free.ui.parent;

import ru.beclean.free.ui.MainActivity;
import android.support.v4.app.Fragment;

public class ParentFragment extends Fragment
{
	protected void toggleMenu()
	{
		try
		{
			((MainActivity)getActivity()).toggleMenu(this);	
		} catch (ClassCastException e)
		{
			e.printStackTrace();
		}
	}
	
	public void backPress()
	{
		getActivity().onBackPressed();
	}
}
