package ru.beclean.free.ui.parent;

import android.support.v4.app.FragmentActivity;

public abstract class BaseActivity extends FragmentActivity
{
	public abstract int getContainerID();
}
