package ru.beclean.free.ui.splash;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.servertypes.CarsTypes;
import ru.beclean.free.ui.servertypes.ServiciesTypes;
import ru.beclean.free.utils.ApiUtils;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class SplashFragment extends ParentFragment
{
	public enum SaveType
	{
		CAR_CATEGORIES, SERVICES, FILE_URL;
	}

	private OnSplashCompleteListener mOnSplashCompleteListener;
	private AQuery aq;
	private static final int REQUESTS_COUNT = 3;
	private int requestsCompleted = 0;

	public SplashFragment()
	{

	}

	public static SplashFragment newInstance(OnSplashCompleteListener onSplashCompleteListener)
	{
		SplashFragment f = new SplashFragment();
		f.mOnSplashCompleteListener = onSplashCompleteListener;
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_splash, container, false);

		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
		if (status != ConnectionResult.SUCCESS)
		{
			Toast.makeText(getActivity(), R.string.error_google_play_services_not_installed, Toast.LENGTH_LONG).show();
			getActivity().finish();
			return rootView;
		}

		aq = new AQuery(rootView);

		downloadCarCategories();
		downloadServices();
		downloadFileUrl();

		return rootView;
	}

	private void downloadFileUrl()
	{
		JSONObject urlParams = ApiUtils.getParams(ApiUtils.API_METHOD_REFERENCES_FILES_URL);
		aq.post(ApiUtils.API_URL, urlParams, JSONObject.class, new AjaxCallback<JSONObject>()
		{
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status)
			{
				if (object != null)
				{
					String result = object.optString("result");
					if (result != null)
					{
						saveData(SaveType.FILE_URL, result);
					}
				}
				else
				{
					Toast.makeText(getActivity(), R.string.error_internet_not_work, Toast.LENGTH_SHORT).show();
					getActivity().finish();
				}
				requestComplete();
			}
		});
	}

	private void requestComplete()
	{
		requestsCompleted++;
		if (requestsCompleted >= REQUESTS_COUNT)
		{
			Thread t = new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						Thread.sleep(1000l);
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
					if (getActivity() != null)
					{
						getActivity().runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								if (mOnSplashCompleteListener != null)
								{
									mOnSplashCompleteListener.onSplashComplete(SplashFragment.this);
								}
							}
						});
					}
				}
			});
			t.start();
		}
	}

	private void downloadCarCategories()
	{
		JSONObject carsParams = ApiUtils.getParams(ApiUtils.API_METHOD_REFERENCES_CAR_CLASSES);
		aq.post(ApiUtils.API_URL, carsParams, JSONObject.class, new AjaxCallback<JSONObject>()
		{
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status)
			{
				// {"id":null,"error":null,"result":[{"id":1,"name":"��"},{"id":2,"name":"��"},{"id":3,"name":"��"},{"id":4,"name":"��"}]}
				super.callback(url, object, status);
				if (object != null)
				{
					JSONArray result = object.optJSONArray("result");
					if (result != null)
					{
						CarsTypes types = new CarsTypes(result);
						saveData(SaveType.CAR_CATEGORIES, types);
					}
				}
				else
				{
					Toast.makeText(getActivity(), R.string.error_internet_not_work, Toast.LENGTH_SHORT).show();
					getActivity().finish();
				}
				requestComplete();
			}
		});
	}

	private void downloadServices()
	{
		JSONObject servicesParams = ApiUtils.getParams(ApiUtils.API_METHOD_REFERENCES_SERVICES,
				(Object[]) new String[0]);
		aq.post(ApiUtils.API_URL, servicesParams, JSONObject.class, new AjaxCallback<JSONObject>()
		{
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status)
			{
				super.callback(url, object, status);
				if (object != null)
				{
					JSONArray result = object.optJSONArray("result");
					ServiciesTypes types = new ServiciesTypes(result);
					saveData(SaveType.SERVICES, types);
				}

				requestComplete();
			}
		});
	}

	private void saveData(SaveType type, Object data)
	{
		BeApp beApp = ((BeApp) getActivity().getApplication());
		switch (type)
		{
			case CAR_CATEGORIES:
				if (CarsTypes.class.isInstance(data))
					beApp.setCarTypes((CarsTypes) data);
				break;
			case SERVICES:
				if (ServiciesTypes.class.isInstance(data))
					beApp.setServiciesTypes((ServiciesTypes) data);
				break;
			case FILE_URL:
				beApp.setFileUrl((String)data);
				break;
		}
	}
}
