package ru.beclean.free.ui.splash;

import android.support.v4.app.Fragment;

public interface OnSplashCompleteListener
{
	void onSplashComplete(Fragment f);
}
