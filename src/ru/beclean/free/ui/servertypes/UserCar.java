package ru.beclean.free.ui.servertypes;

import org.json.JSONObject;

import ru.beclean.free.db.DBHelper;

import android.database.Cursor;

public class UserCar
{
	public int id;
	public int car_class_id;
	public boolean is_removed;
	public String car_number;
	public String name;
	
	public UserCar(JSONObject o)
	{
		id = o.optInt("id");
		car_class_id = o.optInt("car_class_id");
		is_removed = o.optBoolean("is_removed");
		car_number = o.optString("car_number");
		name = o.optString("name");
		if(name.equals("null"))
		{
			name = "";
		}
	}
	
	public UserCar(Cursor c)
	{
		int carIdColumn = c.getColumnIndex(DBHelper.CAR_ID);
		int carClassIdColumn = c.getColumnIndex(DBHelper.CAR_CLASS_ID);
		int carNameColumn = c.getColumnIndex(DBHelper.CAR_NAME);
		int carNumberColumn = c.getColumnIndex(DBHelper.CAR_NUMBER);
		int carIsDeletedColumn = c.getColumnIndex(DBHelper.CAR_REMOVE_BUNDLE);
		
		id= c.getInt(carIdColumn);
		car_class_id = c.getInt(carClassIdColumn);
		is_removed = c.getInt(carIsDeletedColumn)==1;
		car_number = c.getString(carNumberColumn);
		name = c.getString(carNameColumn);
		
		if(name.equals("null"))
		{
			name = "";
		}
	}
}
