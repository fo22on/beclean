package ru.beclean.free.ui.servertypes;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.beclean.free.utils.UtilsMethods;

public class Reserv
{
	public int id;
	public long start;
	public long finish;
	public long reserv_time;
	public int client_id;
	public int[] services;
	public int car_id;
	public String price;
	public String phone;
	public int cw_id;
	public int runtime; //mins
	
	private CharSequence source;
	
	/*
	 * {"id":5,
	 * "cw_id":324,
	 * "car_id":1,
	 * "price":1681,
	 * "phone":null,
	 * "reserv_time":"2014-09-06 23:17",
	 * "start":"2014-09-07 04:11",
	 * "finish":"2014-09-07 05:18",
	 * "services":[1,12],
	 * "runtime":67,"client_id":2}
	 * */
	public Reserv(JSONObject o)
	{
		id = o.optInt("id");
		cw_id = o.optInt("cw_id");
		car_id = o.optInt("car_id");
		price = o.optString("price");
		phone = o.optString("phoone");
		reserv_time = UtilsMethods.getDateFromString(o.optString("reserv_time"));
		start = UtilsMethods.getDateFromString(o.optString("start"));
		finish = UtilsMethods.getDateFromString(o.optString("finish"));
		JSONArray servicesArray = o.optJSONArray("services");
		if(servicesArray!=null)
		{
			services = new int[servicesArray.length()];
			for (int i = 0; i < services.length; i++)
			{
				services[i] = servicesArray.optInt(i);
			}
		}
		runtime = o.optInt("runtime");
		source = o.toString();
	}
	
	
	public CharSequence getSource()
	{
		return source;
	}
	
	@Override
	public String toString()
	{
		return source.toString();
	}
}
