package ru.beclean.free.ui.servertypes;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.beclean.free.ui.map.WorkTime;

public class WashFull
{
	public double lat;
	public double lng;
	public int id;
	public ArrayList<String> photos;
	public int idCity;
	public int idCountry;
	public float rating;
	public String address;
	public ArrayList<WorkTime> workTimes;
	public String name;
	public int may_take_together;
	public ArrayList<Services> services;
	
	public WashFull (JSONObject object)
	{
		id = object.optInt("id");
		JSONArray geo = object.optJSONArray("geo_point");
		if (geo != null)
		{
			lat = geo.optDouble(0);
			lng = geo.optDouble(1);
		}
		photos = new ArrayList<String>();
		workTimes = new ArrayList<WorkTime>();
		services = new ArrayList<Services>();
		JSONArray imgs = object.optJSONArray("photos");
		if (imgs != null)
		{
			String paramName = "file_name";
			for (int i = 0; i < imgs.length(); i++)
			{
				photos.add(imgs.optJSONObject(i).optString(paramName));
			}
		}
		JSONObject meta = object.optJSONObject("meta");
		if(meta!=null)
		{
			idCity = meta.optInt("city_id");
			idCountry = meta.optInt("country_id");
			rating = meta.optInt("rating");
			address = meta.optString("address");
			JSONArray work_time = meta.optJSONArray("work_time");
			if (work_time != null)
			{
				for (int i = 0; i < work_time.length(); i++)
				{
					workTimes.add(new WorkTime(work_time.optJSONObject(i)));
				}
			}
			name = meta.optString("name");
			may_take_together = meta.optInt("may_take_together");
		}
		JSONArray services = object.optJSONArray("services");
		if(services!=null)
		{
			for (int i = 0; i < services.length(); i++)
			{
				this.services.add(new Services(services.optJSONObject(i)));
			}
		}
	}
	
	///photos/cw/��� �����/������� ��������
	
/*	{
	    "geo_point": [
	        55.798, 
	        37.6996
	    ], 
	    "photos": [
        	{
            	"id": 1, 
            	"file_name": "2014-07-23.jpg", 
            	"cw_id": 37, 
            	"is_general": true
        	}
    	],  
	    "meta": {
	        "df": 1, 
	        "rating": 0, 
	        "address": "������", 
	        "work_time": [
	            {
	                "id": 225, 
	                "cw_id": 113, 
	                "m_to": 59, 
	                "is_weekend": false, 
	                "h_to": 23, 
	                "h_from": 7, 
	                "m_from": 0
	            }, 
	            {
	                "id": 226, 
	                "cw_id": 113, 
	                "m_to": 59, 
	                "is_weekend": true, 
	                "h_to": 23, 
	                "h_from": 7, 
	                "m_from": 0
	            }
	        ], 
	        "country_id": 1, 
	        "name": "���������", 
	        "may_take_together": 4
	    }, 
	    "services": [
	        {
	            "id": 225, 
	            "service_id": 1, 
	            "runtime": 25, 
	            "cw_id": 113, 
	            "car_class_id": 2, 
	            "price": 547
	        }, 
	        {
	            "id": 226, 
	            "service_id": 2, 
	            "runtime": 27, 
	            "cw_id": 113, 
	            "car_class_id": 2, 
	            "price": 481
	        }
	    ]
	}
	*/
}
