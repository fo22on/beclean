package ru.beclean.free.ui.servertypes;

import org.json.JSONObject;

public class CarType
{
	public int id;
	public String name;
	
	public CarType (JSONObject object)
	{
		if(object==null)
		{
			throw new NullPointerException("JSONObject must bee not null");
		}
		id = object.optInt("id");
		name= object.optString("name");
	}
}
