package ru.beclean.free.ui.servertypes;

import org.json.JSONObject;

import ru.beclean.free.application.BeApp;
import ru.beclean.free.ui.servertypes.ServiciesTypes.ServiceType;

public class Services
{
	public int runtime;
	public String name;
	public int idService;
	public int id;
	public int price;
	public int classAvto;
	public boolean isOtherServices = false;
	
	public Services(JSONObject object)
	{
		id= object.optInt("id");
		idService = object.optInt("service_id");
		classAvto = object.optInt("car_class_id");
		runtime = object.optInt("runtime");
		price = object.optInt("price");
		name = object.optString("name");
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o==null)
			return false;
		if(Integer.class.isInstance(o))
		{
			Integer idService = Integer.valueOf(this.idService);
			return idService.equals(Integer.valueOf((Integer)o));
		}
		return false;
	}
	
	public static void initServices (WashFull cw, ServiciesTypes types)
	{
		for (Services service:cw.services)
		{
			boolean isOtherService = false;
			for (ServiceType type : types.others)
			{
				if(service.idService==type.id)
				{
					isOtherService = true;
					break;
				}
			}
			service.isOtherServices = isOtherService;
		}
	}
//	 {
//         "id": 225, 
//         "service_id": 1, 
//         "runtime": 25, 
//         "cw_id": 113, 
//         "car_class_id": 2, 
//         "price": 547
//     }
}
