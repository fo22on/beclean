package ru.beclean.free.ui.servertypes;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

import android.location.Location;

public class SearchItem
{
	public String text;
	public String name;
	public String description;
	public Location location;
	public LatLng upperCorner;
	public LatLng lowerCorner;
	
	public SearchItem (JSONObject object)
	{
		text = object.optString("text");
		name = object.optString("name");
		description = object.optString("description");
		if (object.has("geo_point"))
		{
			JSONArray geoPoint = object.optJSONArray("geo_point");
			location = new Location(text);
			location.setLatitude(geoPoint.optDouble(1));
			location.setLongitude(geoPoint.optDouble(0));
		}
		if(object.has("envelope"))
		{
			JSONArray up = object.optJSONObject("envelope").optJSONArray("upper_corner");
			JSONArray low = object.optJSONObject("envelope").optJSONArray("lower_corner");
			upperCorner = new LatLng(up.optDouble(1), up.optDouble(0));
			lowerCorner = new LatLng(low.optDouble(1), low.optDouble(0));
			
		}
	}
	/*"text": "������, ���������� ���� (������), ������������� �����, ��������, ������� �", 
            "geo_point": [
                "124.716201", 
                "56.648365"
            ], 
            "description": "��������, ������������� �����, ���������� ���� (������), ������", 
            "name": "������� �", 
            "envelope": {
                "upper_corner": [
                    "124.721393", 
                    "56.652146"
                ], 
                "lower_corner": [
                    "124.711009", 
                    "56.644584"
                ]
            }*/
}
