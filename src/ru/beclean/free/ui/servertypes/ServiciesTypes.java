package ru.beclean.free.ui.servertypes;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class ServiciesTypes
{
	// {"id":null,
	// "error":null,
	// "result":[
	// {"id":1,"type":1,"runtime":30,"name":"����������� �����"},
	// {"id":2,"type":1,"runtime":10,"name":"����� ������"},
	// {"id":3,"type":1,"runtime":20,"name":"����� ������"}]}
	public ArrayList<ServiceType> items;
	public ArrayList<ServiceType> others;

	public ServiciesTypes(JSONArray array)
	{
		items = new ArrayList<ServiceType>();
		others = new ArrayList<ServiceType>();
		if (array != null)
		{
			int length = array.length();
			for (int i = 0; i < length; i++)
			{
				ServiceType item = null;
				try
				{
					item = new ServiceType(array.optJSONObject(i));
				} catch (Exception e)
				{}

				if (item != null) 
				{
					switch (item.type)
					{
						case 1:
							items.add(item);
							break;
						case 2:
							others.add(item);
						default:
							break;
					}
					
				}
			}
		}
	}

	public static class ServiceType
	{
		public int id;
		public int type;
		public int runtime;
		public String name;
		public int min_price;
		public int max_price;

		public ServiceType(JSONObject object)
		{
			id = object.optInt("id");
			type = object.optInt("type_id");
			runtime = object.optInt("runtime");
			name = object.optString("name");
			min_price = object.optInt("min_price");
			max_price = object.optInt("max_price");
		}
	}
}
