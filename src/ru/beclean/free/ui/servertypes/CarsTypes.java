package ru.beclean.free.ui.servertypes;

import java.util.ArrayList;

import org.json.JSONArray;

public class CarsTypes
{
	public ArrayList<CarType> types;
	public CarsTypes (JSONArray array)
	{
		int length = array.length();
		types = new ArrayList<CarType>();
		for (int i = 0; i < length; i++)
		{
			types.add(new CarType(array.optJSONObject(i)));
		}
	}
}
