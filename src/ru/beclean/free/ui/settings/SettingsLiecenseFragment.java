package ru.beclean.free.ui.settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import ru.beclean.free.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SettingsLiecenseFragment extends Fragment
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_settings_license, container, false);
		TextView tv = (TextView) rootView.findViewById(R.id.licenseHtmlTextView);
		try
		{
			StringBuilder buf=new StringBuilder();
		    InputStream json=getActivity().getAssets().open("license.htm");
		    BufferedReader in=
		        new BufferedReader(new InputStreamReader(json, "UTF-8"));
		    String str;

		    while ((str=in.readLine()) != null) {
		      buf.append(str);
		    }

		    in.close();
		    tv.setText(Html.fromHtml(buf.toString()));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return rootView;
	}
}
