package ru.beclean.free.ui.settings;

import org.json.JSONObject;

import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.rpc.RPCAnswer;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class SettingsActivity extends FragmentActivity implements OnClickListener
{
	private AQuery aq;
	private String phoneSupport = "";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		aq = new AQuery(this);
		initActionBar();

		aq.id(R.id.licenceTextView).clicked(this);
		aq.id(R.id.logOutButton).clicked(this);
		aq.id(R.id.supportButton).clicked(this);
		aq.id(R.id.likeImageView).clicked(this);

		phoneSupport = ((BeApp) getApplication()).getPhoneSupport();
		if (TextUtils.isEmpty(phoneSupport))
		{
			downloadPhoneSupport();
		}
	}

	private void initActionBar()
	{
		aq.id(R.id.actionBarBackImageButton).image(R.drawable.icon_menu_selector).clicked(this);
		aq.id(R.id.actionBarTitleTextView).text(R.string.menu_item_settings);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.actionBarBackImageButton:
			{
				onBackPressed();
				break;
			}
			case R.id.licenceTextView:
			{
				FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				ft.replace(R.id.container, new SettingsLiecenseFragment(), SettingsLiecenseFragment.class.getName());
				ft.addToBackStack(SettingsLiecenseFragment.class.getName());
				ft.commit();
				break;
			}
			case R.id.logOutButton:
			{
				Builder builder = new Builder(this);
				builder.setTitle(R.string.settings_warning_logout_title);
				builder.setMessage(getString(R.string.settings_warning_logout_description));
				builder.setPositiveButton(getString(R.string.settings_warning_logout_button_title),
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								dialog.dismiss();
								AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
								{
									@Override
									public void callback(String url, RPCAnswer object, AjaxStatus status)
									{
										super.callback(url, object, status);
									}
								};
								ApiUtils.initAuth(getApplicationContext(), cb);
								JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_CLIENT_LOG_OUT);
								aq.transformer(((BeApp) getApplication()).getTransformer()).post(ApiUtils.API_URL,
										params, RPCAnswer.class, cb);
							}
						});
				builder.setNegativeButton(getString(R.string.settings_warning_logout_cancel_title),
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								dialog.dismiss();
							}
						});
				builder.create().show();
				break;
			}
			case R.id.supportButton:
			{
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:" + phoneSupport));
				startActivity(intent);
				break;
			}
			case R.id.likeImageView:
			{
				break;
			}
		}
	}

	private void downloadPhoneSupport()
	{
		JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_REFERENCES_PHONE_SUPPORT);
		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			public void callback(String url, RPCAnswer answer, AjaxStatus status)
			{
				if (answer != null && !answer.isError())
				{
					if (answer.getResult() != null)
					{
						JSONObject res = answer.getResult();

						((BeApp) getApplication()).setPhoneSupport((String) res.toString());
						phoneSupport = ((BeApp) getApplication()).getPhoneSupport();
					}
				}
			};
		};
		ApiUtils.initAuth(this, cb);
		aq.transformer(((BeApp) getApplication()).getTransformer()).post(ApiUtils.API_URL, params, RPCAnswer.class, cb);
	}
}
