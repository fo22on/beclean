package ru.beclean.free.ui.filter.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.util.Log;

public class RangeBarItem extends ItemInSection
{
	public String title;
	private long startTime;
	private long endTime;
	private SimpleDateFormat sdf;
	private final long millsInDay = 24*60*60*1000;
	
	public RangeBarItem()
	{
		TimeZone timeZone = Calendar.getInstance().getTimeZone();
		timeZone.setRawOffset(60*60*1000);
		sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
		
		sdf.setTimeZone(timeZone);
	}

	@Override
	public String getTitle()
	{
		return title;
	}

	@Override
	public boolean isSelected()
	{
		return false;
	}
	
	public void setTime(long start, long end)
	{
		startTime = start;
		endTime = end;
		
		title = sdf.format(new Date(start)) + " - " + sdf.format(new Date(end)); 
	}
	
	public void setIndexes(int start, int end)
	{
		setTime(start*5000*60, end*5000*60);
		Log.d("setIndexes", "start:" + start + "; end" + end);
	}
	
	public long getStartTime()
	{
		return startTime;
	}
	
	public long getEndTime()
	{
		return endTime;
	}
	
	public int getStartIndex()
	{
		return getIndex(startTime);
	}
	
	public int getEndIndex()
	{
		return getIndex(endTime);
	}
	
	private int getIndex(long mils)
	{
		mils = mils%millsInDay;
		
		int index = (int)((mils/60000)/5); 
		return index;
	}
}
