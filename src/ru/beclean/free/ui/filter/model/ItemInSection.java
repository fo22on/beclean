package ru.beclean.free.ui.filter.model;

public abstract class ItemInSection
{
	public abstract String getTitle();
	public abstract boolean isSelected();
}
