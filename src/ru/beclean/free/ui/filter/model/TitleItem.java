package ru.beclean.free.ui.filter.model;


public class TitleItem extends ItemInSection
{
	private String title;
	
	public TitleItem (String title)
	{
		this.title = title;  
	}
	
	@Override
	public String getTitle()
	{
		return title==null?"":title;
	}

	@Override
	public boolean isSelected()
	{
		return false;
	}
}
