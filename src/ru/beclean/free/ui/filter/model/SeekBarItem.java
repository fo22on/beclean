package ru.beclean.free.ui.filter.model;

public class SeekBarItem extends ItemInSection
{
	private String title = null;
	private int minPrice = 0;
	private int maxPrice = 0;
	private int currentPrice;
	private String postfix;
	
	public SeekBarItem (int currentPrice, int maxPrice, int minPrice)
	{
		this.currentPrice = currentPrice;
		this.maxPrice = maxPrice;
		this.minPrice = minPrice;
		inintTitle();
	}
	
	public int getMaxPrice()
	{
		return maxPrice;
	}
	
	public int getPrice()
	{
		return currentPrice;
	}
	
	public void setPostfix(String postfix)
	{
		this.postfix = postfix;
		inintTitle();
	}
	
	private void inintTitle()
	{
		title = currentPrice + postfix;
	}

	public void setCurrentPrice(int price)
	{
		currentPrice = price;
		inintTitle();
	}
	@Override
	public String getTitle()
	{
		return title;
	}

	@Override
	public boolean isSelected()
	{
		return false;
	}

	public int getMinPrice()
	{
		return minPrice;
	}
}
