package ru.beclean.free.ui.filter.model;


public class CheckBoxItem extends ItemInSection
{
	public int id;
	private String title;
	private boolean isSelected;
	
	public CheckBoxItem (String title, boolean isSelected)
	{
		this.title = title;
		this.isSelected = isSelected;
	}
	
	@Override
	public String getTitle()
	{
		return title;
	}

	@Override
	public boolean isSelected()
	{
		return isSelected;
	}
	
	public void setSelection(boolean selection)
	{
		isSelected = selection;
	}
}
