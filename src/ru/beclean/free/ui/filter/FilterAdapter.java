package ru.beclean.free.ui.filter;

import java.util.ArrayList;

import com.edmodo.rangebar.RangeBar;
import com.edmodo.rangebar.RangeBar.OnRangeBarChangeListener;

import ru.beclean.free.R;
import ru.beclean.free.ui.filter.model.CheckBoxItem;
import ru.beclean.free.ui.filter.model.ItemInSection;
import ru.beclean.free.ui.filter.model.RangeBarItem;
import ru.beclean.free.ui.filter.model.SeekBarItem;
import ru.beclean.free.ui.filter.model.TitleItem;
import ru.beclean.free.utils.FilterUtils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class FilterAdapter extends BaseAdapter
{
	private final static int ITEM_VIEW_TYPE = 0;
	private final static int ITEM_RANGE_TYPE = 1;
	private final static int ITEM_PRICE_TYPE = 2;
	private final static int VIEW_TYPE_COUNT = ITEM_PRICE_TYPE + 1;
	private LayoutInflater inflater;
	private ArrayList<Section> mItems;
	private Context mContext;

	public FilterAdapter(Context context, ArrayList<Section> items)
	{
		mContext = context;
		inflater = LayoutInflater.from(context);
		mItems = items;
	}

	@Override
	public int getCount()
	{
		return mItems.size();
	}
	
	@Override
	public int getItemViewType(int position)
	{
		if(position==1) return ITEM_RANGE_TYPE;
		if(position==5) return ITEM_PRICE_TYPE;
		else return ITEM_VIEW_TYPE;
	}
	
	@Override
	public int getViewTypeCount()
	{
		return VIEW_TYPE_COUNT;
	}

	@Override
	public Section getItem(int position)
	{
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		Section item = getItem(position);
		switch (getItemViewType(position))
		{
			case ITEM_VIEW_TYPE:
			{
				ViewHolder holder;
				if(convertView==null)
				{
					convertView = inflater.inflate(R.layout.item_filter_main, parent, false);
					holder = new ViewHolder();
					holder.title = (TextView) convertView.findViewById(R.id.titleTextView);
					holder.data = (LinearLayout) convertView.findViewById(R.id.dataLinearLayout);
					convertView.setTag(holder);
				}
				else
				{
					holder = (ViewHolder) convertView.getTag();
				}
				
				holder.title.setText(item.title);
				holder.data.removeAllViews();
				
				for (ItemInSection itemSection : item.items)
				{
					View v = inflater.inflate(R.layout.item_filter_item, holder.data, false);
					if(TitleItem.class.isInstance(itemSection))
					{
						v.findViewById(R.id.selectImageView).setVisibility(View.GONE);
						TextView title = (TextView) v.findViewById(R.id.titleTextView);
						title.setText(itemSection.getTitle());
					}
					else if(CheckBoxItem.class.isInstance(itemSection))
					{
						boolean isSelected = ((CheckBoxItem)itemSection).isSelected();
						v.findViewById(R.id.selectImageView).setVisibility(isSelected?View.VISIBLE:View.INVISIBLE);
						TextView title = (TextView) v.findViewById(R.id.titleTextView);
						title.setText(itemSection.getTitle());
					}
					
					LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
					v.setLayoutParams(params);
					holder.data.addView(v);
				}
				break;
			}
			case ITEM_RANGE_TYPE:
			{
				ViewRangeBarHolder holder;
				if(convertView==null)
				{
					convertView = inflater.inflate(R.layout.item_fragment_filter_time_delay, parent, false);
					holder = new ViewRangeBarHolder();
					holder.title = (TextView) convertView.findViewById(R.id.timeTextView);
					holder.bar = (RangeBar) convertView.findViewById(R.id.timeRangeBar);
					
					holder.bar.setTag(R.id.tagBarText, holder.title);
					convertView.setTag(holder);
				}
				else
				{
					holder = (ViewRangeBarHolder) convertView.getTag();
				}
				
				RangeBarItem rbi = (RangeBarItem) item.items.get(0);
				holder.bar.setTag(R.id.tagBarSource, rbi);
				holder.bar.setThumbIndices(rbi.getStartIndex(), rbi.getEndIndex());
				
				holder.bar.setOnRangeBarChangeListener(new OnRangeBarChangeListener()
				{
					@Override
					public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex)
					{
						if(leftThumbIndex==0&&rightThumbIndex==0) return;
						TextView title = (TextView) rangeBar.getTag(R.id.tagBarText);
						RangeBarItem rbi = (RangeBarItem) rangeBar.getTag(R.id.tagBarSource);
						rbi.setIndexes(leftThumbIndex, rightThumbIndex);
						title.setText(rbi.getTitle());
						FilterUtils.setFilterTImes(mContext, rbi.getStartTime(), rbi.getEndTime());
					}
				});
				
				holder.title.setText(rbi.getTitle());
				break;
			}
			case ITEM_PRICE_TYPE:
			{
				ViewSeekBarHolder holder;
				if(convertView==null)
				{
					convertView = inflater.inflate(R.layout.item_fragment_filter_price_max, parent, false);
					holder = new ViewSeekBarHolder();
					holder.title = (TextView) convertView.findViewById(R.id.timeTextView);
					holder.bar = (SeekBar) convertView.findViewById(R.id.priceSeekBar);
					
					holder.bar.setTag(R.id.tagBarText, holder.title);
					convertView.setTag(holder);
				}
				else
				{
					holder = (ViewSeekBarHolder) convertView.getTag();
				}
				SeekBarItem sbi = (SeekBarItem) item.items.get(0);
				holder.bar.setTag(R.id.tagBarSource, sbi);
				
				holder.bar.setMax(sbi.getMaxPrice()-sbi.getMinPrice());
				holder.bar.setProgress(sbi.getPrice()-sbi.getMinPrice());
				
				holder.bar.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
				{
					
					@Override
					public void onStopTrackingTouch(SeekBar seekBar)
					{}
					
					@Override
					public void onStartTrackingTouch(SeekBar seekBar)
					{}
					
					@Override
					public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
					{
						TextView title = (TextView) seekBar.getTag(R.id.tagBarText);
						SeekBarItem sbi = (SeekBarItem) seekBar.getTag(R.id.tagBarSource);
						sbi.setCurrentPrice(progress+sbi.getMinPrice());
						title.setText(sbi.getTitle());
						FilterUtils.setPrice(mContext, sbi.getPrice());
					}
				});
				
				holder.title.setText(sbi.getTitle());
				break;
			}
		}
		
		
//		convertView = inflater.inflate(R.layout.item_fragment_filter_time_delay, parent, false);
//		convertView.setTag(holder);
		
		return convertView;
	}

	private static class ViewHolder 
	{
		TextView title;
		LinearLayout data;
	}
	
	private static class ViewRangeBarHolder
	{
		TextView title;
		RangeBar bar;
	}
	
	private static class ViewSeekBarHolder
	{
		TextView title;
		SeekBar bar;
	}
}
