package ru.beclean.free.ui.filter;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import ru.beclean.free.R;
import ru.beclean.free.ui.mycars.MyCarsFragment;
import ru.beclean.free.ui.parent.ParentActivity;

public class FilterSelectCarActivity extends ParentActivity{
	public final static String FROM_FILTER = "from_filter";
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_main);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		MyCarsFragment f = new MyCarsFragment();
		Bundle bundle = new Bundle();
		bundle.putBoolean(FROM_FILTER, true);
		f.setArguments(bundle);
		ft.replace(R.id.containerView, f);
		ft.commit();
	}
	
	@Override
	public int getContainerID()
	{
		return R.id.containerView;
	}
}
