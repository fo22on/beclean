package ru.beclean.free.ui.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.androidquery.AQuery;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.AdapterView.OnItemClickListener;
import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.db.DBUtils;
import ru.beclean.free.ui.filter.model.CheckBoxItem;
import ru.beclean.free.ui.filter.model.ItemInSection;
import ru.beclean.free.ui.filter.model.RangeBarItem;
import ru.beclean.free.ui.filter.model.SeekBarItem;
import ru.beclean.free.ui.filter.model.TitleItem;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.servertypes.CarType;
import ru.beclean.free.ui.servertypes.CarsTypes;
import ru.beclean.free.ui.servertypes.ServiciesTypes;
import ru.beclean.free.ui.servertypes.UserCar;
import ru.beclean.free.ui.servertypes.ServiciesTypes.ServiceType;
import ru.beclean.free.utils.FilterUtils;
import ru.beclean.free.utils.UtilsMethods;

public class FilterFragment extends ParentFragment implements OnItemClickListener, OnClickListener
{
	private AQuery aq;
	private boolean isEdit = false;
	private int sectionId = -1;
	private ArrayList<Section> filters;
	private FilterAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_filter, container, false);
		aq = new AQuery(rootView);

		aq.id(R.id.cancelButton).clicked(this);
		aq.id(R.id.dataListView).itemClicked(this);

		return rootView;
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (!isEdit)
		{
			initFilters();
			mAdapter = new FilterAdapter(getActivity(), filters);
			aq.id(R.id.dataListView).adapter(mAdapter);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
	{
		switch (sectionId)
		{
			case -1:
				mainItemClick(position);
				break;
			case 2:
			case 3:
			{
				FilterCheckBoxAdapter adapter = (FilterCheckBoxAdapter) parent.getAdapter();
				if (position == 0)
				{
					int[] selectedIds = adapter.getSelectedIds();
					if (sectionId == 2)
					{
						FilterUtils.setServices(getActivity(), selectedIds);
					}
					else if (sectionId == 3)
					{
						FilterUtils.setOtherServices(getActivity(), selectedIds);
					}
					isEdit = false;
					sectionId = -1;
					notifyAdapter();
				}
				else
				{
					adapter.setSelection(position);
				}
				break;
			}
			case 4:
			{
				FilterCheckBoxAdapter adapter = (FilterCheckBoxAdapter) parent.getAdapter();
				if (position == 0)
				{
					int[] selectedIds = adapter.getSelectedIds();
					
					UtilsMethods.saveUserCarCategory(getActivity(), selectedIds[0]);
					isEdit = false;
					sectionId = -1;
					notifyAdapter();
				}
				else
				{
					adapter.setChecked(position);
				}
				break;
			}
			default:
				break;
		}
	}

	private void initFilters()
	{
		filters = new ArrayList<Section>();
		String[] titles = getResources().getStringArray(R.array.filters_titles);
		for (int i = 0; i < titles.length; i++)
		{
			Section s = getSection(titles[i], i, true);
			if (s != null)
				filters.add(s);
		}
	}

	private Section getSection(String title, int position, boolean selectedItemsOnly)
	{
		Section s = new Section(title, null, null);
		ArrayList<ItemInSection> items = new ArrayList<ItemInSection>();
		switch (position)
		{
			case 0:
			{
				Calendar cur = new GregorianCalendar();
				Calendar c = new GregorianCalendar();
				c.setTimeInMillis(FilterUtils.getFilterDate(getActivity()));
				long mils = cur.after(c) ? cur.getTimeInMillis() : c.getTimeInMillis();
				String itemName = FilterUtils.getFormatedDate(mils).toString();
				items.add(new TitleItem(itemName));
				break;
			}
			case 1:
			{
				RangeBarItem item = new RangeBarItem();
				item.setTime(FilterUtils.getFilterTimeStart(getActivity()), FilterUtils.getFilterTimeEnd(getActivity()));

				items.add(item);
				break;
			}
			case 2:
			{
				ServiciesTypes services = ((BeApp) getActivity().getApplication()).getServiciesTypes();
				int[] selectedIds = FilterUtils.getServices(getActivity());
				if (selectedIds.length > 0)
				{
					for (ServiceType type : services.items)
					{
						if (Arrays.binarySearch(selectedIds, type.id) >= 0)
						{
							CheckBoxItem item = new CheckBoxItem(type.name, true);
							item.id = type.id;
							items.add(item);
						}
						else if (!selectedItemsOnly)
						{
							CheckBoxItem item = new CheckBoxItem(type.name, false);
							item.id = type.id;
							items.add(item);
						}
					}
				}
				else
				{
					if (!selectedItemsOnly)
					{
						for (ServiceType type : services.items)
						{
							CheckBoxItem item = new CheckBoxItem(type.name, false);
							item.id = type.id;
							items.add(item);
						}
					}
				}

				break;
			}
			case 3:
			{
				ServiciesTypes services = ((BeApp) getActivity().getApplication()).getServiciesTypes();
				int[] selectedIds = FilterUtils.getOtherServices(getActivity());

				if (selectedIds.length > 0)
				{
					for (ServiceType type : services.others)
					{
						if (Arrays.binarySearch(selectedIds, type.id) >= 0)
						{
							CheckBoxItem item = new CheckBoxItem(type.name, true);
							item.id = type.id;
							items.add(item);
						}
						else if (!selectedItemsOnly)
						{
							CheckBoxItem item = new CheckBoxItem(type.name, false);
							item.id = type.id;
							items.add(item);
						}
					}
				}
				else
				{
					if (!selectedItemsOnly)
					{
						for (ServiceType type : services.others)
						{
							CheckBoxItem item = new CheckBoxItem(type.name, false);
							item.id = type.id;
							items.add(item);
						}
					}
				}

				break;
			}
			case 4:
			{
				int id = UtilsMethods.getUserCarCategory(getActivity());
				CarsTypes types = ((BeApp) getActivity().getApplication()).getCarTypes();
				UserCar car = DBUtils.getMyCar(getActivity(), id);
				ArrayList<UserCar> cars = new ArrayList<UserCar>();
				if(selectedItemsOnly)
				{
					cars.add(car);
				}
				else
				{
					cars.addAll(DBUtils.getMyCars(getActivity()));
				}
				for (UserCar userCar : cars)
				{
					ItemInSection item = null;
					String sectiontitle = null;
					if (TextUtils.isEmpty(userCar.name))
					{
						if (TextUtils.isEmpty(userCar.car_number))
						{
							for (CarType type : types.types)
							{
								if (type.id == userCar.car_class_id)
								{
									sectiontitle= type.name;
									break;
								}
							}
						}
						else
						{
							sectiontitle = userCar.car_number;
						}
					}
					else
					{
						sectiontitle = userCar.name;
						
					}
					if(selectedItemsOnly)
					{
						item = new TitleItem(sectiontitle);
					}
					else
					{
						item = new CheckBoxItem(sectiontitle, car.id==userCar.id);
						((CheckBoxItem)item).id = userCar.id;
					}
					items.add(item);
				}
				

				break;
			}
			case 5:
			{
				int[] selectedIds = FilterUtils.getServices(getActivity());
				if (selectedIds == null || selectedIds.length < 1)
					return null;

				ServiciesTypes services = ((BeApp) getActivity().getApplication()).getServiciesTypes();
				int maxPrice = 0;
				int minPrice = 0;
				int currentPrice = FilterUtils.getPrice(getActivity());

				for (ServiceType type : services.items)
				{
					if (Arrays.binarySearch(selectedIds, type.id) >= 0)
					{
						maxPrice += type.max_price;
						minPrice += type.min_price;
					}
				}
				if(currentPrice>maxPrice||currentPrice<minPrice)
					currentPrice = maxPrice;
				SeekBarItem item = new SeekBarItem(currentPrice, maxPrice, minPrice);
				item.setPostfix(" " + getString(R.string.wash_coins));
				items.add(item);
			}
			default:
				break;
		}
		s.items = items;
		return s;
	}

	private void mainItemClick(final int position)
	{
		final Calendar current = Calendar.getInstance();
		switch (position)
		{
			case 0:
			{
				long millsSaved = FilterUtils.getFilterDate(getActivity());
				long millsCurrent = System.currentTimeMillis();
				current.setTimeInMillis(millsSaved > millsCurrent ? millsSaved : millsCurrent);
				DatePickerDialog dialog = new DatePickerDialog(getActivity(), new OnDateSetListener()
				{
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
					{
						Calendar c = new GregorianCalendar(year, monthOfYear, dayOfMonth);
						FilterUtils.setFilterDate(getActivity(), c.getTimeInMillis());
						notifyAdapter();
					}
				}, current.get(Calendar.YEAR), current.get(Calendar.MONTH), current.get(Calendar.DAY_OF_MONTH));
				dialog.getDatePicker().setMinDate(millsCurrent);
				dialog.getDatePicker().setMaxDate(millsCurrent + 7 * 24 * 60 * 60 * 1000);
				dialog.show();
				break;
			}
			case 1:
			{
				current.setTimeInMillis(FilterUtils.getFilterTimeStart(getActivity()));
				TimePickerDialog dialog = new TimePickerDialog(getActivity(), new OnTimeSetListener()
				{
					boolean isShown = false;

					@Override
					public void onTimeSet(TimePicker view, final int hourOfDay, final int minute)
					{
						if (!isShown)
						{
							isShown = true;
							TimePickerDialog dialog = new TimePickerDialog(getActivity(), new OnTimeSetListener()
							{
								@Override
								public void onTimeSet(TimePicker view, int hourOfDayEnd, int minuteEnd)
								{
									Calendar calStart = new GregorianCalendar();
									Calendar calEnd = new GregorianCalendar();
									calStart.setTimeInMillis(FilterUtils.getFilterDate(getActivity()));
									calEnd.setTimeInMillis(FilterUtils.getFilterDate(getActivity()));

									calStart.set(Calendar.HOUR_OF_DAY, hourOfDay);
									calStart.set(Calendar.MINUTE, minute);

									calEnd.set(Calendar.HOUR_OF_DAY, hourOfDayEnd);
									calEnd.set(Calendar.MINUTE, minuteEnd);
									FilterUtils.setFilterTImes(getActivity(), calStart.getTimeInMillis(),
											calEnd.getTimeInMillis());

									notifyAdapter();
								}
							}, current.get(Calendar.HOUR_OF_DAY), current.get(Calendar.MINUTE), true);
							// dialog.setTitle(R.string.filter_last_time);
							dialog.show();
						}
						else
						{
							isShown = false;
						}
					}
				}, current.get(Calendar.HOUR_OF_DAY), current.get(Calendar.MINUTE), true);
				// dialog.setTitle(R.string.filter_first_time);
				dialog.show();
				break;
			}
			case 2:
			case 3:
			case 4:
			{
				isEdit = true;
				sectionId = position;
				Section section = getSection(filters.get(position).title, position, false);
				aq.id(R.id.dataListView).adapter(new FilterCheckBoxAdapter(getActivity(), section));
				break;
			}
//			case 4:
//			{
//				startActivity(new Intent(getActivity(), FilterSelectCarActivity.class));
//				// FragmentTransaction ft =
//				// getActivity().getSupportFragmentManager().beginTransaction();
//				// ft.replace(R.id.container, new FilterSelectCarFragment());
//				// ft.commit();
//				break;
//			}
			default:
				break;
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.cancelButton:
			{
				isEdit = false;
				FilterUtils.clearFilters(getActivity());
				notifyAdapter();
				break;
			}
		}
	}

	private void notifyAdapter()
	{
		initFilters();
		mAdapter = new FilterAdapter(getActivity(), filters);
		aq.id(R.id.dataListView).adapter(mAdapter);
	}
}
