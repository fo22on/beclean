package ru.beclean.free.ui.filter;

import ru.beclean.free.R;
import ru.beclean.free.ui.filter.model.CheckBoxItem;
import ru.beclean.free.ui.filter.model.ItemInSection;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FilterCheckBoxAdapter extends BaseAdapter
{
	private static final int VIEW_TYPE_SECTION = 0;
	private static final int VIEW_TYPE_TITLE = 1;
	private static final int VIEW_TYPES_COUNT = VIEW_TYPE_TITLE + 1;
	private Section mSection;
	private LayoutInflater inflater;

	public FilterCheckBoxAdapter(Context context, Section section)
	{
		mSection = section;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount()
	{
		return mSection.getSize();
	}

	@Override
	public Object getItem(int position)
	{
		return mSection.getItem(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public int getViewTypeCount()
	{
		return VIEW_TYPES_COUNT;
	}

	@Override
	public int getItemViewType(int position)
	{
		return position == 0 ? VIEW_TYPE_TITLE : VIEW_TYPE_SECTION;
	}

	@Override
	public boolean isEnabled(int position)
	{
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		switch (getItemViewType(position))
		{
			case VIEW_TYPE_TITLE:
			{
				String item = (String) getItem(position);
				TextView title;
				if (convertView == null)
				{
					convertView = inflater.inflate(R.layout.item_filter_title, parent, false);
					title = (TextView) convertView.findViewById(R.id.titleTextView);
					convertView.setTag(title);
				}
				else
				{
					title = (TextView) convertView.getTag();
				}

				title.setText(item);

				break;
			}
			case VIEW_TYPE_SECTION:
			{
				ViewHolder holder;
				ItemInSection item = (ItemInSection) getItem(position);
				if (convertView == null)
				{
					convertView = inflater.inflate(R.layout.item_filter_item, parent, false);
					holder = new ViewHolder();
					holder.checkBox = (ImageView) convertView.findViewById(R.id.selectImageView);
					holder.title = (TextView) convertView.findViewById(R.id.titleTextView);
					convertView.setTag(holder);
				}
				else
				{
					holder = (ViewHolder) convertView.getTag();
				}
				holder.checkBox.setVisibility(item.isSelected() ? View.VISIBLE : View.INVISIBLE);
				holder.title.setText(item.getTitle());
			}
			default:
				break;
		}

		return convertView;
	}

	private static class ViewHolder
	{
		ImageView checkBox;
		TextView title;
	}

	public void setSelection(int position)
	{
		Object object = getItem(position);
		if (CheckBoxItem.class.isInstance(object))
		{
			CheckBoxItem item = (CheckBoxItem) object;
			item.setSelection(!item.isSelected());
			notifyDataSetChanged();
		}

	}
	
	public void setChecked(int position)
	{
		int length = getCount();
		for (int i = 0; i < length; i++)
		{
			Object object = getItem(i);
			
			if (CheckBoxItem.class.isInstance(object))
			{
				CheckBoxItem item = (CheckBoxItem) object;
				item.setSelection(i==position);
				notifyDataSetChanged();
			}
		}
		

	}

	public Section getSection()
	{
		return mSection;
	}

	public int[] getSelectedIds()
	{
		int[] out = null;
		int count = 0;
		for (ItemInSection item : mSection.items)
		{
			if (((CheckBoxItem) item).isSelected())
			{
				count++;
			}
		}
		out = new int[count];
		for (int i = 0, j = 0; i < mSection.items.size(); i++)
		{
			CheckBoxItem item = (CheckBoxItem) mSection.items.get(i);
			if (item.isSelected())
			{
				out[j] = item.id;
				j++;
			}
		}
		return out;
	}
}
