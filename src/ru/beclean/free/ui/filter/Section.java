package ru.beclean.free.ui.filter;

import java.util.ArrayList;

import ru.beclean.free.ui.filter.model.ItemInSection;

public class Section
{
	public String title;
	public String sublitle;
	public ArrayList<ItemInSection> items;
	
	public Section(String title, String subtitle, ArrayList<ItemInSection> items)
	{
		this.title = title;
		this.sublitle = subtitle;
		this.items = items;
	}
	
	public int getSize()
	{
		if(items==null)
			return 1;
		return items.size() + 1;
	}
	public Object getItem(int position)
	{
		try
		{
			if(position==0)
				return title;
			else
			{
				return items.get(position-1);
			}
		} catch (NullPointerException e)
		{
			return null;
		}
		
	}
	
}
