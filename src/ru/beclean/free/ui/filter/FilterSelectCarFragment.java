package ru.beclean.free.ui.filter;

import com.androidquery.AQuery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import ru.beclean.free.R;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.register.CarsCategoriesAdapter;

public class FilterSelectCarFragment extends ParentFragment implements OnItemClickListener{
	private AQuery aq;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_after_register, container, false);
		aq = new AQuery(rootView);
		
		ListView dataListView = (ListView) rootView.findViewById(R.id.dataListView);
		dataListView.setAdapter(new CarsCategoriesAdapter(getActivity(), true));
		dataListView.setOnItemClickListener(this);
		
		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		//TODO сохранение типа автомобиля
		getActivity().finish();
	}
}
