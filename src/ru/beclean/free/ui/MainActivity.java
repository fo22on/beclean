package ru.beclean.free.ui;

import java.util.ArrayList;

import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.ui.filter.FilterFragment;
import ru.beclean.free.ui.map.MapFragment;
import ru.beclean.free.ui.menu.MenuUtils;
import ru.beclean.free.ui.mycars.MyCarsFragment;
import ru.beclean.free.ui.mywash.MyWashFragment;
import ru.beclean.free.ui.parent.ParentActivity;
import ru.beclean.free.ui.register.PreRegisterFragment;
import ru.beclean.free.ui.register.RegisterFragment;
import ru.beclean.free.ui.reserv.WashReservFragment;
import ru.beclean.free.ui.settings.SettingsActivity;
import ru.beclean.free.ui.splash.OnSplashCompleteListener;
import ru.beclean.free.ui.splash.SplashFragment;
import ru.beclean.free.ui.washlist.WashListFragment;
import ru.beclean.free.utils.OnFilterStateChanged;
import ru.beclean.free.utils.UtilsMethods;
import ru.beclean.free.utils.rpc.OnAuthErrorListener;
import ru.beclean.free.utils.rpc.RPCAnswer;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Toast;

import com.androidquery.AQuery;

public class MainActivity extends ParentActivity implements OnClickListener, OnTouchListener
{
	private int[] ids = { R.id.mapContainer, R.id.searchContainer, R.id.myWashesContainer, R.id.myCarContainer };
	private int containerId = ids[0];
	private int lastSelected = -1;
	private AQuery aq;

	private boolean isFiltersShown = false;
	private boolean isMenuShown = false;

	private View buttons;

	private PointF startPoint;
	private boolean isPerformClickNeeded = false;
	private int pixelsInTouch = 5;
	private Fragment currentFragment;
	
	private OnFilterStateChanged mOnFilterStateChanged;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_whis_menu);
		
		aq = new AQuery(this);
		aq.id(getContainerID()).getView().setAlpha(1);
		buttons = aq.id(R.id.menuFrameLayout).getView();

		aq.id(R.id.menuImageButton).clicked(this);
		aq.id(R.id.settingsTextView).clicked(this);
		aq.id(R.id.mapClickView).clicked(this);

		if (savedInstanceState == null)
		{
			getSupportFragmentManager().beginTransaction()
					.add(containerId, SplashFragment.newInstance(new OnSplashCompleteListener()
					{
						@Override
						public void onSplashComplete(final Fragment f)
						{
							FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
							ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
							ft.remove(f);
							if (UtilsMethods.getToken(getApplicationContext()).equals(""))
							{
								ft.replace(containerId, new PreRegisterFragment());
							}
							else
							{
								if(UtilsMethods.getReserv(getApplicationContext())==null)
								{
									ft.replace(containerId, new MapFragment());
								}
								else
								{
									ft.replace(containerId, new WashReservFragment());
								}
//								WashDescriptionFragment wf = new WashDescriptionFragment();
//								Bundle bundle = new Bundle();
//								bundle.putInt(WashDescriptionFragment.WASH_ID, 113);
//								wf.setArguments(bundle);
//								ft.replace(containerId, wf);
							}
							ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
							ft.commit();
							findViewById(R.id.background).setBackgroundResource(R.color.bg_main_gray_color);
						}
					})).commit();
		}

		pixelsInTouch = (int) (getResources().getDisplayMetrics().density * 5 + 0.5F);
	}

	public void toggleFilter()
	{
		try
		{
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			View v = aq.id(getContainerID()).getView();
			Animator animator;
			if (!isFiltersShown)
			{
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				ft.replace(R.id.filterContainer, new FilterFragment(), FilterFragment.class.getName());
				animator = AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.filter_map_out_animation);
				aq.id(R.id.filterClickLinearLayout).visibility(View.VISIBLE);
			}
			else
			{
				aq.id(R.id.filterClickLinearLayout).visibility(View.GONE);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
				ft.remove(getSupportFragmentManager().findFragmentByTag(FilterFragment.class.getName()));
				animator = AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.filter_map_in_animation);
			}
			ft.commit();
			animator.setTarget(v);
			animator.start();
			
			isFiltersShown = !isFiltersShown;
			
			if(mOnFilterStateChanged!=null)
			{
				mOnFilterStateChanged.onNewState(isFiltersShown);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void toggleMenu(Fragment fragment)
	{
		if (fragment != null)
		{
			currentFragment = fragment;
		}
		ArrayList<AnimatorSet> list = new ArrayList<AnimatorSet>();

		for (int i = 0; i < ids.length; i++)
		{
			list.add(MenuUtils.getViewAnim(findViewById(ids[i]), isMenuShown, ids[i], getContainerID()));
			View v = findViewById(ids[i]);
			v.findViewById(R.id.headerView).setVisibility(View.VISIBLE);
			v.findViewById(R.id.footerView).setVisibility(View.VISIBLE);
		}

		AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.play(list.get(0)).with(list.get(1)).with(list.get(2)).with(list.get(3))
				.with(MenuUtils.getButtonsAnim(buttons, isMenuShown));

		if (!isMenuShown)
		{
			lastSelected = containerId;
			buttons.setVisibility(View.VISIBLE);
			buttons.setOnTouchListener(this);
			aq.id(R.id.menuImageButton).enabled(true);
		}
		else
		{
			animatorSet.addListener(new AnimatorListener()
			{
				@Override
				public void onAnimationStart(Animator animation)
				{
					aq.id(R.id.menuImageButton).enabled(false);
					if (lastSelected != getContainerID())
					{
						aq.id(getContainerID()).getView().findViewById(R.id.headerView).setVisibility(View.GONE);
						aq.id(getContainerID()).getView().findViewById(R.id.footerView).setVisibility(View.GONE);
					}
				}

				@Override
				public void onAnimationRepeat(Animator animation)
				{
				}

				@Override
				public void onAnimationEnd(Animator animation)
				{
					buttons.setVisibility(View.GONE);
					buttons.setOnTouchListener(null);
					if (lastSelected != getContainerID())
					{
						menuItemClicked(getContainerID());
					}
					lastSelected = -1;
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{
					buttons.setVisibility(View.GONE);
					buttons.setOnTouchListener(null);
					if (lastSelected != getContainerID())
					{
						menuItemClicked(getContainerID());
					}
					lastSelected = -1;
				}
			});
		}
		animatorSet.start();
		isMenuShown = !isMenuShown;
	}

	@Override
	public void onBackPressed()
	{
		if (isFiltersShown)
		{
			toggleFilter();
			return;
		}
		if (isMenuShown)
		{
			toggleMenu(null);
			return;
		}
		if(getSupportFragmentManager().getBackStackEntryCount()>0)
		{
			getSupportFragmentManager().popBackStack();
			return;
		}
		super.onBackPressed();
	}

	@Override
	public int getContainerID()
	{
		return containerId;
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.menuImageButton:
				toggleMenu(null);
				break;
			case R.id.settingsTextView:
				startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
				break;
			case R.id.mapClickView:
				toggleFilter();
				break;
			default:
				break;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		switch (event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
				startPoint = new PointF(event.getX(), event.getY());
				isPerformClickNeeded = true;
				break;

			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_OUTSIDE:
			{
				if (isPerformClickNeeded)
				{
					float width = getResources().getDisplayMetrics().widthPixels;
					float spaceStart = width * MenuUtils.getXStayCoeff(ids[ids.length - 1]);
					if (startPoint.x < spaceStart)
						return false;
					for (int i = 0; i < ids.length; i++)
					{
						if (startPoint.x >= width * MenuUtils.getXStayCoeff(ids[i]))
						{
							containerId = ids[i];
							toggleMenu(null);
							break;
						}
					}
				}
				break;
			}
			case MotionEvent.ACTION_MOVE:
			{
				if (isPerformClickNeeded
						&& (Math.abs(startPoint.x - event.getX()) > pixelsInTouch || Math.abs(startPoint.y
								- event.getY()) > pixelsInTouch))
				{
					isPerformClickNeeded = false;
				}
				break;
			}
			default:
				break;
		}
		return true;
	}

	private void menuItemClicked(int id)
	{
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		if (currentFragment != null)
		{
			ft.remove(currentFragment);
			currentFragment = null;
		}
		switch (id)
		{
			case R.id.mapContainer:
				ft.replace(getContainerID(), new MapFragment());
				break;
			case R.id.searchContainer:
				ft.replace(getContainerID(), new WashListFragment());
				break;
			case R.id.myWashesContainer:
				ft.replace(getContainerID(), new MyWashFragment());
				break;
			case R.id.myCarContainer:
				ft.replace(getContainerID(), new MyCarsFragment());
				break;
		}
		ft.commit();
		dropMenuAnim();
	}
	
	private void dropMenuAnim()
	{
		for (int i = 0; i < ids.length; i++)
		{
			View v = findViewById(ids[i]);
			v.setScaleX(1);
			v.setScaleY(1);
		}
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		((BeApp)getApplication()).getTransformer().setOnAuthErrorListener(mOnaAuthErrorListener);
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		((BeApp)getApplication()).getTransformer().setOnAuthErrorListener(null);
	}
	
	private OnAuthErrorListener mOnaAuthErrorListener = new OnAuthErrorListener()
	{
		@Override
		public void onAuthError(final RPCAnswer a)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					Toast.makeText(getApplicationContext(), a.getError(), Toast.LENGTH_LONG).show();
					FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
					transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					//transaction.remove(currentFragment);
					containerId = ids[0];
					transaction.setCustomAnimations(0, R.anim.fade_out_anim);
					transaction.replace(getContainerID(), RegisterFragment.newInstance(getResources().getDisplayMetrics().heightPixels));
					transaction.commit();
				}
			});
		}
	};
	
	public void removeOnFilterStateChanged(OnFilterStateChanged onFilterStateChanged)
	{
		mOnFilterStateChanged=null;
	}

	public void setOnFilterStateChanged(OnFilterStateChanged onFilterStateChanged)
	{
		this.mOnFilterStateChanged = onFilterStateChanged;
	}
}
