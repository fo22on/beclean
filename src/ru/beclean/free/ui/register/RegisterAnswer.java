package ru.beclean.free.ui.register;

import org.json.JSONObject;

public class RegisterAnswer
{
	//{"id":null,"error":null,"result":{"is_exist":false,"auth_data":{"id":59,"phone":"+79889999999"}}}
	//{"id":null,"error":null,"result":{"id":5,"cars":[],"name":null}}
	//{"id":null,"error":null,"result":{"id":3,"is_exist":true}}
	public int id;
	public String name;
	
	public RegisterAnswer (JSONObject object)
	{
		if(object.has("error"))
		{
			JSONObject error = object.optJSONObject("error");
			if(error!=null)
			{
				throw new NullPointerException(error.toString());
			}
		}
	}
}
