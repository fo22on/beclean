package ru.beclean.free.ui.register;

import com.androidquery.AQuery;

import ru.beclean.free.R;
import android.content.res.Resources;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PreRegisterInfoAdapter extends PagerAdapter
{
	private String[] titles;
	private String[] descriptions;
	private int[] icons = {R.drawable.ic_launcher};
	public PreRegisterInfoAdapter (Resources res)
	{
		titles = res.getStringArray(R.array.pre_registration_titles);
		descriptions = res.getStringArray(R.array.pre_registration_descriptions);
	}
	@Override
	public int getCount()
	{
		return titles.length;
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position)
	{
		View convertView = LayoutInflater.from(container.getContext()).inflate(R.layout.item_fragment_pre_register, container, false);
		AQuery aq = new AQuery(convertView);
		aq.id(R.id.titleTextView).text(titles[position]);
		aq.id(R.id.descriptionTextView).text(descriptions[position]);
		aq.id(R.id.pictureImageView).image(icons[position%icons.length]);
		
		container.addView(convertView, 0);
		return convertView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object)
	{
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object)
	{
		return view.equals(object);
	}
	
	@Override
	public int getItemPosition(Object object)
	{
		return POSITION_NONE;
	}

}
