package ru.beclean.free.ui.register;

import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.db.DBUtils;
import ru.beclean.free.ui.MainActivity;
import ru.beclean.free.ui.map.MapFragment;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.servertypes.UserCar;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.UtilsMethods;
import ru.beclean.free.utils.rpc.RPCAnswer;

public class AfterRegisterFragment extends ParentFragment implements OnItemClickListener
{
	private AQuery aq;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_after_register, container, false);
		aq = new AQuery(rootView);

		ListView dataListView = (ListView) rootView.findViewById(R.id.dataListView);
		dataListView.setAdapter(new CarsCategoriesAdapter(getActivity(), false));
		dataListView.setOnItemClickListener(this);
		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		saveCategory((int) id);
		
	}

	private void saveCategory(int id)
	{
		saveCarTypeToServer(id);
	}

	private void saveCarTypeToServer(int id)
	{
		JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_CLIENT_CAR_APPEND, "car_class_id", "" + id);
		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer object, AjaxStatus status)
			{
				if (object != null && !object.isError())
				{
					UserCar car = new UserCar(object.getResult());
					DBUtils.saveCar(getActivity(), car);

					UtilsMethods.saveUserCarCategory(getActivity(), car.id);
					
					openMap();
				}
				else
				{
					Toast.makeText(getActivity(), R.string.my_car_error_servet, Toast.LENGTH_LONG).show();
				}
			}
		};
		ApiUtils.initAuth(getActivity(), cb);
		aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, params,
				RPCAnswer.class, cb);
	}
	
	private void openMap()
	{
		MainActivity activity = (MainActivity) getActivity();
		FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		ft.remove(this);
		ft.replace(activity.getContainerID(), new MapFragment());
		ft.commit();
	}
}
