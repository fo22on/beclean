package ru.beclean.free.ui.register;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.internal.fo;

import ru.beclean.free.ui.servertypes.Reserv;
import ru.beclean.free.ui.servertypes.UserCar;

public class AuthAnsver
{
	//{"id":null,"error":{"type":"JSONRPCError","message":"Procedure not found"},"result":null}
	//{"id":null,"error":null,"result":"9e7f97c2e22e4bdab750167013fc5e66"}
	//{"id":null,"error":null,"result":{"session":"a4b7302e2b844c6fb3e78a527d20a6b4","cars":[],"profile":{"id":3,"name":null}}}
	public String token;
	public boolean carsExist = false;
	public int carCategory;
	public ArrayList<UserCar> cars = new ArrayList<UserCar>();
	public ArrayList<Reserv> reservs = new ArrayList<Reserv>();
	
	public AuthAnsver (JSONObject object)
	{
		JSONObject error = object.optJSONObject("error");
		if(error!=null)
		{
			throw new NullPointerException(error.toString());
		}
		
		JSONObject itemObject = object.optJSONObject("result");
		if(itemObject!=null)
		{
			token = itemObject.optString("session");
			JSONArray cars = itemObject.optJSONArray("cars");
			carsExist = cars!=null&&cars.length()>0;
			if(carsExist)
			{
				for (int i = 0; i < cars.length(); i++)
				{
					this.cars.add(new UserCar(cars.optJSONObject(i)));
				}
				carCategory = this.cars.get(0).car_class_id;
			}
			JSONArray reservs =itemObject.optJSONArray("reservations");
			for (int i = 0; i < reservs.length(); i++)
			{
				this.reservs.add(new Reserv(reservs.optJSONObject(i)));
			}
		}
	}
}
