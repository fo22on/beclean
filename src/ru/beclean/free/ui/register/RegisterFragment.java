package ru.beclean.free.ui.register;

import org.json.JSONObject;

import ru.beclean.free.R;
import ru.beclean.free.db.DBUtils;
import ru.beclean.free.ui.MainActivity;
import ru.beclean.free.ui.map.MapFragment;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.reserv.WashReservFragment;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.UtilsMethods;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class RegisterFragment extends ParentFragment implements OnClickListener
{
	private AQuery aq;
	private float fromY = 0;
	private String phoneNumber;
	private EditText phoneNumberEditText;
	private boolean isTextChangeProgrammatical = false;
	private boolean isRequestSend = false;
	private AuthAnsver currentAnsver = null;
	private final static String PHONE_PREFIX = "+7";

	private int[] codeIds = { R.id.editText1, R.id.editText2, R.id.editText3, R.id.editText4 };

	public static RegisterFragment newInstance(float fromY)
	{
		RegisterFragment f = new RegisterFragment();
		f.fromY = fromY;
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_registration, container, false);
		aq = new AQuery(rootView);

		runStartAnimation();
		phoneNumberEditText = aq.id(R.id.phoneNumberEditText).getEditText();
		new MyTextWatcher(aq.id(R.id.editText1).getEditText());
		new MyTextWatcher(aq.id(R.id.editText2).getEditText());
		new MyTextWatcher(aq.id(R.id.editText3).getEditText());
		new MyTextWatcher(aq.id(R.id.editText4).getEditText());
		aq.id(R.id.confirmButton).clicked(this);

		phoneNumberEditText.addTextChangedListener(phoneNumberTextWatcher);
		return rootView;
	}

	private void runStartAnimation()
	{
		Animation fadeOutAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out_anim);
		fadeOutAnim.setAnimationListener(new AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{
			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				aq.id(R.id.animatedButton).visibility(View.INVISIBLE);
			}
		});

		Animation fadeIn = new TranslateAnimation(0, 0, fromY, 0);
		fadeIn.setDuration(getResources().getInteger(R.integer.animation_duration));
		fadeIn.setInterpolator(getActivity(), android.R.interpolator.decelerate_quad);

		aq.animate(fadeIn);
		aq.id(R.id.animatedButton).animate(fadeOutAnim);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (phoneNumberEditText.isEnabled() && phoneNumberEditText.hasFocus())
		{
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(phoneNumberEditText, InputMethodManager.SHOW_IMPLICIT);
		}
	}

	@Override
	public void onPause()
	{
		super.onPause();
		closeKeyboard();
	}

	private void closeKeyboard()
	{
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		if (phoneNumberEditText.hasFocus())
		{
			imm.hideSoftInputFromWindow(phoneNumberEditText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
		imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, InputMethodManager.HIDE_IMPLICIT_ONLY);
	}

	private TextWatcher phoneNumberTextWatcher = new TextWatcher()
	{
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count)
		{
			Log.d("onTextChanged", "" + s + "; " + start + "; " + before + "; " + count);
			if (!isTextChangeProgrammatical)
			{

				StringBuilder sb = new StringBuilder();
				String phone = UtilsMethods.getPhoneFromFormattedString(s);
				int padding = 0;
				if(phone.length()>0)
				{
					sb.append('(');
					padding++;
				}

				sb.append(phone);
				if(phone.length()>3)
				{
					sb.insert(3 + padding, ") ");
					padding+=2;
				}
				
				if(phone.length()>6)
				{
					sb.insert(6 + padding, " ");
					padding++;
				}
				
				if(phone.length()>8)
				{
					sb.insert(8 + padding, " ");
					padding++;
				}
				boolean isValid = false;
				if (phone.length() == 10 && (phone.charAt(0) == '8' || phone.charAt(0) == '9'))
				{
					phoneNumber = null;
					phoneNumber = phone;
					isValid = true;
				}
				else
				{
					isValid = false;
				}

				initPhoneValidation(isValid);
				isTextChangeProgrammatical = !isTextChangeProgrammatical;
				phoneNumberEditText.setText(sb.toString());
				phoneNumberEditText.setSelection(sb.length());
			}
			else
			{
				isTextChangeProgrammatical = false;
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after)
		{
		}

		@Override
		public void afterTextChanged(Editable s)
		{
		}
	};

	private void initPhoneValidation(boolean isValid)
	{
		aq.id(R.id.confirmButton).enabled(isValid);
		aq.id(R.id.approveCodeLinearLayout).visibility(View.GONE);
		if (isValid)
		{
			if(aq.id(R.id.aproveImageView).getImageView().getAlpha()==0)
			{
				aq.id(R.id.aproveImageView).getImageView().animate().alpha(1);
			}
			phoneNumberEditText.clearFocus();
			if (isRequestSend && currentAnsver != null)
			{
				showPinEdit();
			}
			else
			{
				isRequestSend = false;
			}
		}
		else
		{
			if(aq.id(R.id.aproveImageView).getImageView().getAlpha()==1)
			{
				aq.id(R.id.aproveImageView).getImageView().animate().alpha(0);
			}
		}
	}

	private void initRequestCodeValidation()
	{
		int codeCount = 0;
		for (int i = 0; i < codeIds.length; i++)
		{
			if (!aq.id(codeIds[i]).getText().toString().equals(""))
			{
				codeCount++;
			}
		}
		if (codeCount == 4)
		{
			aq.id(R.id.confirmButton).enabled(true);
		}
		else
		{
			aq.id(R.id.confirmButton).enabled(false);
		}
	}

	private class MyTextWatcher implements TextWatcher
	{
		private EditText current;

		public MyTextWatcher(EditText editText)
		{
			current = editText;
			current.addTextChangedListener(this);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after)
		{
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count)
		{
		}

		@Override
		public void afterTextChanged(Editable s)
		{
			if (s.length() > 1)
			{
				s = s.delete(0, 1);
			}
			if (s.length() > 0)
			{
				switch (current.getId())
				{
					case R.id.editText1:
						aq.id(R.id.editText2).getEditText().requestFocus();
						break;
					case R.id.editText2:
						aq.id(R.id.editText3).getEditText().requestFocus();
						break;
					case R.id.editText3:
						aq.id(R.id.editText4).getEditText().requestFocus();
						break;
					case R.id.editText4:
						break;
					default:
						break;
				}
				initRequestCodeValidation();
			}
			else
			{
				switch (current.getId())
				{
					case R.id.editText1:
						break;
					case R.id.editText2:
						aq.id(R.id.editText1).getEditText().requestFocus();
						break;
					case R.id.editText3:
						aq.id(R.id.editText2).getEditText().requestFocus();
						break;
					case R.id.editText4:
						aq.id(R.id.editText3).getEditText().requestFocus();
						break;
					default:
						break;
				}
				initRequestCodeValidation();
			}
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.confirmButton:
				if (!isRequestSend)
				{
					sendPhoneToServer(phoneNumber);
				}
				else
				{
					sendApproveToServer(getApproveCode());
				}
				// runErrorConfirmAnim();
				break;
			default:
				break;
		}
	}

	private void runErrorConfirmAnim()
	{
		final View animatedView = aq.id(R.id.nonErrorApproveCodeBgView).getView();
		Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out_anim);
		final Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in_anim);
		Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_anim);
		fadeOut.setAnimationListener(new AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{
			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				animatedView.startAnimation(fadeIn);
			}
		});
		animatedView.startAnimation(fadeOut);
		aq.id(R.id.approveCodeLinearLayout).animate(shake);
	}

	private void sendPhoneToServer(String phone)
	{
		JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_CLIENT_NEW, "phone", PHONE_PREFIX + phone);

		isRequestSend = true;
		aq.post(ApiUtils.API_URL, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status)
			{
				super.callback(url, object, status);
				RegisterAnswer answer = null;
				try
				{
					answer = new RegisterAnswer(object);
				} catch (NullPointerException e)
				{
					e.printStackTrace();
				}

				if (answer != null)
				{
					phoneNumberEditText.clearFocus();

					showPinEdit();
					initRequestCodeValidation();
				}
				else
				{
					isRequestSend = false;
					phoneNumberEditText.requestFocus();
				}
			}
		});
	}

	private void showPinEdit()
	{
		aq.id(R.id.approveCodeLinearLayout).visibility(View.VISIBLE);

		aq.id(R.id.editText1).getEditText().requestFocus();
	}

	private void sendApproveToServer(String requestCode)
	{
		JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_CLIENT_AUTH, "phone", "+7" + phoneNumber, "code",
				requestCode);
		aq.post(ApiUtils.API_URL, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status)
			{
				super.callback(url, object, status);

				AuthAnsver ansver = null;
				try
				{
					ansver = new AuthAnsver(object);

					currentAnsver = null;
					currentAnsver = ansver;
				} catch (NullPointerException e)
				{
					runErrorConfirmAnim();
					e.printStackTrace();
				}
				if (ansver != null)
				{
					UtilsMethods.saveToken(getActivity(), ansver.token);
					if (ansver.carsExist)
					{
						DBUtils.saveMyCars(getActivity(), ansver.cars);
						UtilsMethods.saveUserCarCategory(getActivity(), ansver.cars.get(0).id);
					}
					else
					{
						DBUtils.deletAllCars(getActivity());
					}
					closeFragment();

				}
			}
		});
	}

	private String getApproveCode()
	{
		StringBuilder code = new StringBuilder();
		for (int i = 0; i < codeIds.length; i++)
		{
			String key = aq.id(codeIds[i]).getText().toString();
			if (!key.equals(""))
			{
				code.append(key);
			}
		}
		return code.toString();
	}

	private void closeFragment()
	{
		FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		transaction.remove(RegisterFragment.this);
		if(currentAnsver.reservs.size()>0)
		{
			UtilsMethods.saveReserv(getActivity(), currentAnsver.reservs.get(0));
			transaction.replace(((MainActivity) getActivity()).getContainerID(), new WashReservFragment());
		}
		else if (currentAnsver.carsExist)
		{
			transaction.replace(((MainActivity) getActivity()).getContainerID(), new MapFragment());
		}
		else
		{
			transaction.replace(((MainActivity) getActivity()).getContainerID(), new AfterRegisterFragment());
		}
		transaction.commit();
	}
}
