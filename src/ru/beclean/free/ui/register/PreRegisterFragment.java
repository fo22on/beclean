package ru.beclean.free.ui.register;

import com.androidquery.AQuery;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import ru.beclean.free.R;
import ru.beclean.free.ui.MainActivity;
import ru.beclean.free.ui.parent.ParentFragment;

public class PreRegisterFragment extends ParentFragment implements OnPageChangeListener, OnClickListener
{
	private AQuery aq;
	private ViewPager mViewPager;
	private View[] dots = null;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_pre_register, container, false);
		aq = new AQuery(rootView);
		
		mViewPager = (ViewPager)aq.id(R.id.viewPager).getView();
		
		mViewPager.setOnPageChangeListener(this);
		PreRegisterInfoAdapter adapter = new PreRegisterInfoAdapter(getResources());
		initDots(adapter);
		mViewPager.setAdapter(adapter);
		
		aq.id(R.id.registerButton).clicked(this);
		
		return rootView;
	}
	@Override
	public void onPageScrollStateChanged(int arg0)
	{}
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2)
	{}
	@Override
	public void onPageSelected(int position)
	{
		for (int i = 0; i < dots.length; i++)
		{
			dots[i].setBackgroundResource(R.drawable.pager_normal);
		}
		dots[position].setBackgroundResource(R.drawable.pager_current);
	}
	private void initDots(PagerAdapter adapter)
	{
		LinearLayout dotsLayout = (LinearLayout)aq.id(R.id.indicatorsLinearLayout).getView();
		if(dots!=null)
		{
			dotsLayout.removeAllViews();
			dots = null;
		}
		
		dots = new View[adapter.getCount()];
		int dotSize = getResources().getDimensionPixelSize(R.dimen.activity_half_margin);
		for (int i = 0; i < dots.length; i++)
		{
			View dot = new View(getActivity());
			dot.setPadding(dotSize/2, 0, dotSize/2, 0);
			if(i==0)
			{
				dot.setBackgroundResource(R.drawable.pager_current);
			}
			else
			{
				dot.setBackgroundResource(R.drawable.pager_normal);
			}
			LayoutParams params = new LayoutParams(dotSize, dotSize);
			params.leftMargin = dotSize/2;
			params.rightMargin = dotSize/2;
			dotsLayout.addView(dot, params);
			dots[i] = dot;
		}
	}
	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.registerButton:	
			{
				LinearLayout layout = (LinearLayout) v.getParent();
				layout.getY();
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				transaction.remove(this);
				transaction.setCustomAnimations(0, R.anim.fade_out_anim);
				transaction.replace(((MainActivity)getActivity()).getContainerID(), RegisterFragment.newInstance(layout.getY()));
				transaction.commit();
				
				break;
			}
			default:
				break;
		}
	}
}
