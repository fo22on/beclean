package ru.beclean.free.ui.register;

import ru.beclean.free.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CarsCategoriesAdapter extends BaseAdapter
{
	private int[] pictures = {R.drawable.car_class_1, R.drawable.car_class_2, R.drawable.car_class_3, R.drawable.car_class_4};
	private int[] picturesBlack = {R.drawable.type_1_dark, R.drawable.type_2_dark, R.drawable.type_3_dark, R.drawable.type_4_dark};
	private String[] categories;
	private LayoutInflater mInflater;
	boolean whiteTheme = false;
	
	private static final int VIEW_ITEM_TYPE = 0;
	private static final int VIEW_HEADER_TYPE = 1;
	private static final int VIEW_TYPE_COUNT = VIEW_HEADER_TYPE + 1;
	
	public CarsCategoriesAdapter (Context context, boolean whiteTheme)
	{
		categories = context.getResources().getStringArray(R.array.cars_categories);
		mInflater = LayoutInflater.from(context);
		this.whiteTheme = whiteTheme;
	}
	
	@Override
	public int getCount()
	{
		return pictures.length + 1;
	}

	@Override
	public Object getItem(int position)
	{
		return null;
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}
	
	@Override
	public int getItemViewType(int position)
	{
		return (position == 0) ? VIEW_HEADER_TYPE : VIEW_ITEM_TYPE;
	}

	@Override
	public int getViewTypeCount()
	{
		return VIEW_TYPE_COUNT;
	}
	
	@Override
	public boolean isEnabled(int position)
	{
		return getItemViewType(position)==VIEW_ITEM_TYPE;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if(!isEnabled(position))
		{
			if(convertView==null)
			{
				convertView = mInflater.inflate(R.layout.header_transparent, parent, false);
			}
		}
		else
		{
			ViewHolder holder;
			position --;
			if(convertView==null)
			{
				if(whiteTheme)
				{
					convertView = mInflater.inflate(R.layout.item_dialog_select_car_category, parent, false);
				}
				else
				{
					convertView = mInflater.inflate(R.layout.item_fragment_after_register, parent, false);
				}
				holder = new ViewHolder();
				holder.title = (TextView) convertView.findViewById(R.id.titleTextView);
				holder.picture = (ImageView) convertView.findViewById(R.id.pictureImageView);
				convertView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) convertView.getTag();
			}
			holder.title.setText(categories[position]);
			holder.picture.setImageResource(whiteTheme?picturesBlack[position]:pictures[position]);
		}
		return convertView;
	}

	private static class ViewHolder 
	{
		TextView title;
		ImageView picture;
	}
}
