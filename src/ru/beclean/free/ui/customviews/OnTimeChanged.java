package ru.beclean.free.ui.customviews;

public interface OnTimeChanged 
{
	void onTimeChanged(long mils);
}
