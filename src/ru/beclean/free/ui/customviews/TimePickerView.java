package ru.beclean.free.ui.customviews;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.beclean.free.R;
import ru.beclean.free.ui.map.model.Reservation;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TimePickerView extends HorizontalScrollView
{
	private final int MINS_PADDING = 90;
	private int pixelsInHour;
	private float pixelsInMinute;
	private Calendar timePickerDate = null;
	private LinearLayout dataLayout;
	private int padding;

	private NinePatchDrawable bg;
	private Rect drawingRect;
	private Rect reservRect;
	private Paint reservBg;
	private ArrayList<Reservation> reservations;

	private NinePatchDrawable slider;
	private int selectedMins = 0;

	private OnTimeChanged mOnTimeChanged;

	public TimePickerView(Context context)
	{
		super(context);
		init();
	}

	public TimePickerView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public TimePickerView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init();
	}

	private void init()
	{
		LinearLayout dataLayout = new LinearLayout(getContext());
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		dataLayout.setOrientation(LinearLayout.HORIZONTAL);
		dataLayout.setLayoutParams(params);
		addView(dataLayout);
		this.dataLayout = dataLayout;
		setHorizontalScrollBarEnabled(false);
		setVerticalScrollBarEnabled(false);
		padding = (int) (getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin) * 1.2F);

		Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.bg_wash_time_picker);
		byte[] chunk = bm.getNinePatchChunk();

		bg = new NinePatchDrawable(getResources(), bm, chunk, new Rect(), null);
		drawingRect = new Rect();

		bm = BitmapFactory.decodeResource(getResources(), R.drawable.slider_crop);
		chunk = bm.getNinePatchChunk();
		slider = new NinePatchDrawable(getResources(), bm, chunk, new Rect(), null);

		reservRect = new Rect();
		reservBg = new Paint();
		reservBg.setColor(getResources().getColor(R.color.text_gray_color));
	}

	private void setCurrentDate(long date)
	{
		pixelsInHour = getWidth() / 4;
		pixelsInMinute = (float) pixelsInHour / 60F;
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(date);
		if (timePickerDate != null)
		{
			if (calendar.get(Calendar.DAY_OF_YEAR) == timePickerDate.get(Calendar.DAY_OF_YEAR))
			{
				return;
			}
		}

		calendar.add(Calendar.MINUTE, - MINS_PADDING);

		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);

		timePickerDate = calendar;

		dataLayout.removeAllViews();

		calendar = new GregorianCalendar();
		calendar.setTimeInMillis(timePickerDate.getTimeInMillis());

		for (int i = 0; i < 24 * 7; i++)
		{
			TextView textView = new TextView(getContext());
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(pixelsInHour,
					LinearLayout.LayoutParams.MATCH_PARENT);
			textView.setLayoutParams(params);
			textView.setGravity(Gravity.BOTTOM | Gravity.LEFT);
			textView.setTextColor(getResources().getColor(R.color.text_gray_color));
			textView.setText("" + calendar.get(Calendar.HOUR_OF_DAY));

			dataLayout.addView(textView);
			calendar.add(Calendar.HOUR_OF_DAY, 1);
		}
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		int scrollX = getScrollX();
		int scrollY = getScrollY();
		drawingRect.set(scrollX, scrollY + (int) (padding / 3f), scrollX + canvas.getWidth(),
				scrollY + canvas.getHeight() - (int) (padding * 1.5F));
		bg.setBounds(drawingRect);
		bg.draw(canvas);
		if (reservations != null && timePickerDate != null)
		{
			long mils = timePickerDate.getTimeInMillis();
			for (Reservation r : reservations)
			{
				long startScroll = (long) (mils + minsToMils((long)(scrollX / pixelsInMinute)));
				long endScroll = (long) (mils + minsToMils((long)((scrollX + getWidth()) / pixelsInMinute)));
				if (r.end < startScroll || r.start > endScroll)
				{
					continue;
				}
				else
				{
					int left = (int) ((r.start - mils) / 1000 / 60 * pixelsInMinute);
					int top = drawingRect.top;
					int right = (int) ((r.end - mils) / 1000 / 60 * pixelsInMinute);
					int bottom = drawingRect.bottom;
					reservRect.set(left, top, right, bottom);
					canvas.drawRect(reservRect, reservBg);
				}

			}
			// TODO дописать отрисовку резервов
		}
		super.onDraw(canvas);

		int center = canvas.getWidth() / 2;
		if (selectedMins > 0)
		{
			int drawingMins = selectedMins>24?selectedMins:25;
			int width = (int) ((float)drawingMins / 2F * pixelsInMinute);
			slider.setBounds(scrollX + center - width, scrollY, scrollX + center + width, scrollY + canvas.getHeight()
					- padding);
			slider.draw(canvas);
		}
	}

	public void setReservations(ArrayList<Reservation> reservations)
	{
		this.reservations = reservations;
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt)
	{
		super.onScrollChanged(l, t, oldl, oldt);
		if (mOnTimeChanged != null)
		{
			mOnTimeChanged.onTimeChanged(timePickerDate.getTimeInMillis() + minsToMils((long) ((l + getWidth()/2F) / pixelsInMinute)) - minsToMils(selectedMins/2));
		}
	}

	public void setOnTimeChangedListener(OnTimeChanged onTimeChanged)
	{
		mOnTimeChanged = onTimeChanged;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
		postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				setCurrentDate(System.currentTimeMillis());
			}
		}, 200);
	}

	public void setLimitMins(int mins)
	{
		selectedMins = mins;
		postInvalidate();
		mOnTimeChanged.onTimeChanged(timePickerDate.getTimeInMillis() + minsToMils((long) ((getScrollX() + getWidth()/2F) / pixelsInMinute)) - minsToMils(selectedMins/2));
	}
	
	private long minsToMils(long mins)
	{
		return mins*1000*60;
	}
}
