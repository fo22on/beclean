package ru.beclean.free.ui.wash;

import java.util.ArrayList;

import com.androidquery.AQuery;

import ru.beclean.free.R;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class WashGalleryAdapter extends PagerAdapter
{
	private ArrayList<String> mItems;
	private LayoutInflater inflater;
	public WashGalleryAdapter (ArrayList<String> items)
	{
		mItems = items;
	}
	@Override
	public Object instantiateItem(ViewGroup container, int position)
	{
		if(inflater==null)
		{
			inflater = LayoutInflater.from(container.getContext());
		}
		View v = inflater.inflate(R.layout.item_wash_gallery, container, false);
		AQuery aq = new AQuery(v);
		aq.id(R.id.pictureImageView).image(mItems.get(position), true, true);
		container.addView(v, 0);
		return v;
	}
	
	@Override
	public int getCount()
	{
		return mItems.size();
	}
	
	@Override
	public int getItemPosition(Object object)
	{
		return POSITION_NONE;
	}

	@Override
	public boolean isViewFromObject(View view, Object object)
	{
		return view.equals(object);
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object)
	{
		container.removeView((View) object);
	}
}
