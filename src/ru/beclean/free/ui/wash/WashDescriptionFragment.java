package ru.beclean.free.ui.wash;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.db.DBUtils;
import ru.beclean.free.ui.MainActivity;
import ru.beclean.free.ui.customviews.OnTimeChanged;
import ru.beclean.free.ui.customviews.TimePickerView;
import ru.beclean.free.ui.map.model.Reservation;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.reserv.ReservErrors;
import ru.beclean.free.ui.reserv.WashReservFragment;
import ru.beclean.free.ui.servertypes.Reserv;
import ru.beclean.free.ui.servertypes.Services;
import ru.beclean.free.ui.servertypes.ServiciesTypes;
import ru.beclean.free.ui.servertypes.UserCar;
import ru.beclean.free.ui.servertypes.WashFull;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.JsonParamsCreator;
import ru.beclean.free.utils.UtilsMethods;
import ru.beclean.free.utils.rpc.RPCAnswer;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.SupportMapFragment;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class WashDescriptionFragment extends ParentFragment implements OnClickListener
{
	public static final String WASH_ID = "wash_id";

	private AQuery aq;
	private boolean isFavorite = false;
	private int id = -1;
	private WashFull cw;

	private GoogleMap map;
	private TextView fullTimeTextView;
	private int price = 0;
	private int timeServices = 0;
	private Calendar startCalendar = Calendar.getInstance();
	private Calendar endCalendar = Calendar.getInstance();
	private TimePickerView timePicker;

	private SupportMapFragment fragment;
	private UserCar car;
	private ArrayList<Reservation> reserves;
	private ReservErrors mReservErrors = new ReservErrors();
	private float rating = 0f;

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		FragmentManager fm = getChildFragmentManager();
		fragment = (SupportMapFragment) fm.findFragmentById(R.id.mapContainer);
		if (fragment == null)
		{
			fragment = SupportMapFragment.newInstance();
			fm.beginTransaction().replace(R.id.mapContainer, fragment).commit();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_wash, container, false);
		aq = new AQuery(rootView);

		LinearLayout servicesTitleLayout = (LinearLayout) aq.id(R.id.servicesTitleView).getView();
		fullTimeTextView = (TextView) servicesTitleLayout.findViewById(R.id.dataTextView);
		TextView servicesTitle = (TextView) servicesTitleLayout.findViewById(R.id.titleTextView);
		servicesTitle.setText(R.string.wash_services);

		id = getArguments().getInt(WASH_ID, -1); //37

		fullTimeTextView.setText("" + 0);

		aq.id(R.id.confirmButton).clicked(this);

		startCalendar.add(Calendar.MINUTE, 30);
		endCalendar.add(Calendar.MINUTE, 30);
		isFavorite = DBUtils.isWashInFavorites(getActivity(), id);
		initActionBar();
		getFullWashInfo();

		LinearLayout layout = (LinearLayout) aq.id(R.id.scrollContainer).getView();
		timePicker = new TimePickerView(getActivity());
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		timePicker.setLayoutParams(params);
		timePicker.setOnTimeChangedListener(new OnTimeChanged()
		{
			@Override
			public void onTimeChanged(long mils)
			{
				setTime(mils, mils + timeServices * 60 * 1000);
			}
		});
		if (reserves != null)
		{
			timePicker.setReservations(reserves);
		}

		layout.addView(timePicker);
		timePicker.setEnabled(false);

		return rootView;
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (map == null)
		{
			map = fragment.getMap();
			map.setOnMyLocationChangeListener(new OnMyLocationChangeListener()
			{
				@Override
				public void onMyLocationChange(Location location)
				{
					if(getActivity()!=null)
					{
						((BeApp)getActivity().getApplication()).setCurrentLocation(location);
					}
				}
			});
			map.setMyLocationEnabled(true);
			map.getUiSettings().setZoomControlsEnabled(false);
			map.getUiSettings().setMyLocationButtonEnabled(false);
			map.getUiSettings().setAllGesturesEnabled(false);
			map.animateCamera(UtilsMethods.getDefaultMapPosition());
		}
	}

	private void getFullWashInfo()
	{
		JsonParamsCreator paramsCreator = new JsonParamsCreator();
		paramsCreator.addParam("id", id);
		getWashInfo(paramsCreator);
	}

	private void getWashInfo(JsonParamsCreator paramsCreator)
	{
		JSONObject params = paramsCreator.getParams();
		JSONObject washParams = ApiUtils.getParams(ApiUtils.API_METHOD_CARWASHES_GET, params);
		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer answer, AjaxStatus status)
			{
				super.callback(url, answer, status);
				if (answer.isError())
				{
					// TODO ��������� ������ �� �������
					Toast.makeText(getActivity(), answer.getResult().toString(), Toast.LENGTH_LONG).show();
				}
				else
				{
					cw = new WashFull(answer.getResult());
					ServiciesTypes types = ((BeApp)getActivity().getApplication()).getServiciesTypes();
					Services.initServices(cw, types);
					View container = aq.id(R.id.viewPager).getView();
					String dencity = "" + container.getWidth() + "x" + container.getHeight();
					for (int i = 0; i < cw.photos.size(); i++)
					{
						String photo = cw.photos.get(i);
						photo = String.format(((BeApp)getActivity().getApplication()).getFileUrl() + "/photos/cw/%d/%s/%s", cw.id, photo, dencity);
						cw.photos.remove(i);
						cw.photos.add(i,photo);
					}
					initViewDescription();
				}
			}
		};

		ApiUtils.initAuth(getActivity(), cb);
		aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, washParams,
				RPCAnswer.class, cb);
	}

	private void initViewDescription()
	{
		boolean hasOtherServices = UtilsMethods.initSerices(cw.services,
				((BeApp) getActivity().getApplication()).getServiciesTypes());
		LatLng point = new LatLng(cw.lat, cw.lng);
		setTitle(cw.name);
		aq.id(R.id.addressTextView).text(cw.address);
		float[] results = new float[1];
		Location currentLocation = ((BeApp) getActivity().getApplication()).getCurrentPosition();
		Location.distanceBetween(cw.lat, cw.lng, currentLocation.getLatitude(), currentLocation.getLongitude(), results);
		aq.id(R.id.distanceTextView).text(results[0] + " �.").visibility(View.VISIBLE);
		aq.id(R.id.timeTextView).text(cw.workTimes.get(0).toString());
		aq.id(R.id.horizontalScrollView1).visibility(hasOtherServices ? View.VISIBLE : View.GONE);

		if (cw.photos!=null&&cw.photos.size() > 0)
		{
			ViewPager viewPager = (ViewPager) aq.id(R.id.viewPager).visibility(View.VISIBLE).getView();
			viewPager.setPageMargin(-getResources().getDimensionPixelSize(R.dimen.activity_page_margin));
			viewPager.setAdapter(new WashGalleryAdapter(cw.photos));
		}
		else
		{
			aq.id(R.id.viewPager).visibility(View.GONE);
		}
		
		aq.id(R.id.ratingBar).rating(cw.rating);
		aq.id(R.id.ratingView).clicked(this);

		LinearLayout servicesLinearLayout = (LinearLayout) aq.id(R.id.servicesItemsLinearLayout).getView();
		LayoutInflater inflater = LayoutInflater.from(getActivity());
		LinearLayout otherServices = (LinearLayout) aq.id(R.id.servicesOtherLinearLayout).getView();
		int size = otherServices.getHeight();
		int i = 0;

		int id = UtilsMethods.getUserCarCategory(getActivity());
		car = DBUtils.getMyCar(getActivity(), id);
		for (Services service : cw.services)
		{
			if (!service.isOtherServices && service.classAvto == car.car_class_id)
			{
				View v = inflater.inflate(R.layout.item_wash_service, servicesLinearLayout, false);
				ImageView iv = (ImageView) v.findViewById(R.id.selectImageView);
				TextView name = (TextView) v.findViewById(R.id.titleTextView);
				TextView price = (TextView) v.findViewById(R.id.dataTextView);

				iv.setVisibility(View.GONE);
				iv.setTag(false);
				name.setText(service.name);
				name.setTextColor(getResources().getColor(R.color.text_hint_color));
				price.setText("" + service.price);
				price.setTextColor(getResources().getColor(R.color.text_hint_color));

				LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(
						R.dimen.button_default_height));
				v.setLayoutParams(params);
				v.setOnClickListener(servicesClickListener);
				v.setBackgroundResource(i % 2 == 0 ? R.color.bg_main_color : R.color.bg_list_item_second);

				servicesLinearLayout.addView(v);
				v.setTag(service);
				i++;
			}
			else if(service.isOtherServices)
			{
				ImageView iv = new ImageView(getActivity());
				int padding = getResources().getDimensionPixelSize(R.dimen.activity_half_margin);
				iv.setPadding(padding, padding, padding, padding);
				AQuery ivAq = new AQuery(iv); 
				String url = String.format(Locale.ENGLISH, ApiUtils.API_URL_SKIN + "/static/skins/android-phone/default/cw-detail/additional-services/%d.png", service.idService);
				ivAq.image(url, true, true);
				LayoutParams params = new LayoutParams(size, size);
				iv.setLayoutParams(params);
				otherServices.addView(iv);
			}
		}
		
		if(otherServices.getChildCount()<1)
		{
			otherServices.setVisibility(View.GONE);
		}

		setTime(startCalendar.getTimeInMillis(), endCalendar.getTimeInMillis());

		map.addMarker(new MarkerOptions().position(point).title(cw.name)
				.icon(UtilsMethods.getIcon(R.drawable.blue_pin)));
		map.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 16));
	}

	private void setTime(long start, long end)
	{
		startCalendar.setTimeInMillis(start);
		endCalendar.setTimeInMillis(end);
		((TextView) aq.id(R.id.dateTitleView).getView().findViewById(R.id.titleTextView)).setText(R.string.wash_date);
		((TextView) aq.id(R.id.timeTitleView).getView().findViewById(R.id.titleTextView)).setText(R.string.wash_time);

		((TextView) aq.id(R.id.dateTitleView).getView().findViewById(R.id.dataTextView)).setText(DateFormat.format(
				"dd, MMMM", startCalendar));
		String startString = DateFormat.format("HH:mm - ", startCalendar).toString();

		((TextView) aq.id(R.id.timeTitleView).getView().findViewById(R.id.dataTextView)).setText(startString
				+ DateFormat.format("HH:mm", endCalendar));

	}

	private OnClickListener servicesClickListener = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			ImageView iv = (ImageView) v.findViewById(R.id.selectImageView);
			TextView name = (TextView) v.findViewById(R.id.titleTextView);
			TextView priceTextView = (TextView) v.findViewById(R.id.dataTextView);

			Services service = (Services) v.getTag();
			int servicePrice = Integer.parseInt(priceTextView.getText().toString());

			Boolean isSelected = (Boolean) iv.getTag();
			iv.setTag(!isSelected);

			if (isSelected)
			{
				iv.setVisibility(View.GONE);
				name.setTextColor(getResources().getColor(R.color.text_hint_color));
				priceTextView.setTextColor(getResources().getColor(R.color.text_hint_color));
				price -= servicePrice;
				timeServices -= service.runtime;
			}
			else
			{
				iv.setVisibility(View.VISIBLE);
				name.setTextColor(getResources().getColor(R.color.text_black_color));
				priceTextView.setTextColor(getResources().getColor(R.color.text_blue_color));
				price += servicePrice;
				timeServices += service.runtime;
			}
			String priceString = "" + price;
			if (timeServices > 0)
			{
				fullTimeTextView.setText("~ " + timeServices + " "
						+ getResources().getString(R.string.wash_time_postfix));
			}
			else
			{
				fullTimeTextView.setText(null);
			}
			aq.id(R.id.priceTextView).text(priceString + " " + getString(R.string.wash_coins));
			aq.id(R.id.confirmButton).enabled(price > 0);

			timePicker.setLimitMins(timeServices);
		}
	};

	private void initActionBar()
	{
		aq.id(R.id.actionBarBackImageButton).clicked(this);
		// TODO �������� �������� �����
		aq.id(R.id.actionBarTitleTextView).visibility(View.INVISIBLE);
		aq.id(R.id.actionBarSecondActionImageButton).visibility(View.VISIBLE).clicked(this)
				.image(isFavorite ? R.drawable.icon_favorites_on_selector : R.drawable.icon_favorites_off_selector)
				.checked(false);
	}

	private void setTitle(String title)
	{
		aq.id(R.id.actionBarTitleTextView).visibility(View.VISIBLE).text(title);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.actionBarBackImageButton:
				backPress();
				break;
			case R.id.actionBarSecondActionImageButton:
				toggleFavorite(v);
				break;
			case R.id.confirmButton:
			{
				JsonParamsCreator creator = new JsonParamsCreator();
				creator.addParam("cw_id", id);
				creator.addParam("time", UtilsMethods.getStringFromDate(startCalendar.getTimeInMillis()));
				creator.addParam("services_list", getSelectedServices());
				creator.addParam("car_id", car.id);

				JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_RESERVATIONS_SET, creator.getParams());
				AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
				{
					@Override
					public void callback(String url, RPCAnswer object, AjaxStatus status)
					{
						if (!object.isError())
						{
							Reserv reserv = new Reserv(object.getResult());
							UtilsMethods.saveReserv(getActivity(), reserv);
							FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
							ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
							ft.replace(((MainActivity) getActivity()).getContainerID(), new WashReservFragment());
							ft.commit();
						}
						else
						{
							if(object.getErrorType().equalsIgnoreCase("ReservationError"))
							{
								String error = mReservErrors.getError(object.getError());
								Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
							}
						}
					}
				};

				ApiUtils.initAuth(getActivity(), cb);
				aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL,
						params, RPCAnswer.class, cb);
				break;
			}
			case R.id.ratingView:
			{
				Builder builder = new Builder(getActivity());
				View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_set_rating, null, false);
				
				RatingBar bar = (RatingBar)dialogView.findViewById(R.id.ratingBar);
				bar.setRating(rating);
				bar.setOnRatingBarChangeListener(new OnRatingBarChangeListener()
				{
					@Override
					public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser)
					{
						WashDescriptionFragment.this.rating = rating;
					}
				});
				
				builder.setView(dialogView);
				builder.setPositiveButton(R.string.wash_rating_confirm_button, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						if(cw==null||aq==null||getActivity()==null) return;
						JsonParamsCreator creator = new JsonParamsCreator();
						creator.addParam("cw_id", cw.id).addParam("rating", rating);
						JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_CARWASHES_RATING, creator.getParams());
						
						AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
						{
							@Override
							public void callback(String url, RPCAnswer object, AjaxStatus status)
							{
								float rating = (float) object.getResult().optDouble("rating");
								aq.id(R.id.ratingBar).rating(rating);
							}
						};
						
						ApiUtils.initAuth(getActivity(), cb);
						aq.transformer(((BeApp)getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, params, RPCAnswer.class, cb);
						
						dialog.dismiss();
					}
				});
				builder.setNegativeButton(R.string.wash_rating_cancel_button, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
				
				Dialog dialog = builder.create();
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.show();
				
				break;
			}
			default:
				break;
		}
	}

	private JSONArray getSelectedServices()
	{
		JSONArray array = new JSONArray();
		LinearLayout servicesLinearLayout = (LinearLayout) aq.id(R.id.servicesItemsLinearLayout).getView();
		int count = servicesLinearLayout.getChildCount();
		for (int i = 0; i < count; i++)
		{
			View child = servicesLinearLayout.getChildAt(i);
			ImageView ivSelected = (ImageView) child.findViewById(R.id.selectImageView);
			boolean isSelected = ((Boolean) ivSelected.getTag()).booleanValue();
			if (isSelected)
			{
				Services service = (Services) child.getTag();
				array.put(service.idService);
			}
		}
		return array;
	}

	private void toggleFavorite(View v)
	{
		boolean isUpdate = false;

		JsonParamsCreator creator = new JsonParamsCreator();
		creator.addParam("cw_id", "" + id);
		JSONObject favParams = null;
		if (!isFavorite)
		{
			if (getActivity() != null && cw != null)
			{
				DBUtils.addWashToFavorites(getActivity(), cw);
				isUpdate = true;
				favParams = ApiUtils.getParams(ApiUtils.API_METHOD_CARWASH_ADD_TO_FAVORITE, creator.getParams());
			}
		}
		else
		{
			if (getActivity() != null && cw != null)
			{
				DBUtils.removeWashFromFavorites(getActivity(), id);
				isUpdate = true;
				favParams = ApiUtils.getParams(ApiUtils.API_METHOD_CARWASH_REMOVE_FROM_FAVORITE, creator.getParams());
			}
		}
		if (isUpdate)
		{
			isFavorite = !isFavorite;
			((ImageButton) v).setImageResource(isFavorite ? R.drawable.icon_favorites_on_selector
					: R.drawable.icon_favorites_off_selector);
			AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>();
			ApiUtils.initAuth(getActivity(), cb);
			aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, favParams,
					RPCAnswer.class, cb);
		}
	}

	public void setReserves(ArrayList<Reservation> reservations)
	{
		reserves = reservations;
	}
}
