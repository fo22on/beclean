package ru.beclean.free.ui.menu;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import ru.beclean.free.R;

public class MenuUtils
{
	public static AnimatorSet getViewAnim(View v, boolean isMenuShown, int pos, int curPos)
	{
		if (curPos == pos)
			return getCurrentViewAnim(v, isMenuShown, curPos);

		Context context = v.getContext();
		AnimatorSet animatorSet = new AnimatorSet();
		Interpolator interpolator = AnimationUtils.loadInterpolator(context, android.R.anim.accelerate_decelerate_interpolator);
		
		float xCoeff = v.getScaleX()<1?getXCoeff(pos):getXStayCoeff(pos);
		int width = context.getResources().getDisplayMetrics().widthPixels;
		float start = isMenuShown ? width * xCoeff : width;
		float end = isMenuShown ? width : width * xCoeff;

		float alphaCoeff = 0.7F;
		float fromAlpha = isMenuShown ? alphaCoeff : 0;
		float toAlpha = isMenuShown ? 0 : alphaCoeff;
		
		int duration = context.getResources().getInteger(R.integer.animation_duration);
		
		ObjectAnimator translate = ObjectAnimator.ofFloat(v, "x", start, end);
		translate.setDuration(duration);
		translate.setInterpolator(interpolator);

		ObjectAnimator alpha = ObjectAnimator.ofFloat(v, "alpha", fromAlpha, toAlpha);
		alpha.setDuration(duration);
		alpha.setInterpolator(interpolator);
		
		animatorSet.play(translate).with(alpha);

		return animatorSet;
	}

	private static AnimatorSet getCurrentViewAnim(View current, boolean isMenuShown, int pos)
	{
		Context context = current.getContext();
		AnimatorSet animatorSet = new AnimatorSet();
		Interpolator interpolator = AnimationUtils.loadInterpolator(context, android.R.anim.accelerate_decelerate_interpolator);

		float xCoeff = getXCoeff(pos);
		float start = isMenuShown ? context.getResources().getDisplayMetrics().widthPixels * xCoeff : 0;
		float end = isMenuShown ? 0 : context.getResources().getDisplayMetrics().widthPixels * xCoeff;

		float scaleCoeff = getScaleCoeff(pos);
		float fromScale = isMenuShown ? scaleCoeff : 1;
		float toScale = isMenuShown ? 1 : scaleCoeff;

		float alphaCoeff = 0.7F;
		float fromAlpha = isMenuShown ? alphaCoeff : 1;
		float toAlpha = isMenuShown ? 1 : alphaCoeff;

		int duration = context.getResources().getInteger(R.integer.animation_duration);

		ObjectAnimator translate = ObjectAnimator.ofFloat(current, "x", start, end);
		translate.setDuration(duration);
		translate.setInterpolator(interpolator);

		ObjectAnimator scaleX = ObjectAnimator.ofFloat(current, "scaleX", fromScale, toScale);
		scaleX.setDuration(duration);
		scaleX.setInterpolator(interpolator);
		
		ObjectAnimator scaleY = ObjectAnimator.ofFloat(current, "scaleY", fromScale, toScale);
		scaleY.setDuration(duration);
		scaleY.setInterpolator(interpolator);

		ObjectAnimator alpha = ObjectAnimator.ofFloat(current, "alpha", fromAlpha, toAlpha);
		alpha.setDuration(duration);
		alpha.setInterpolator(interpolator);
		
		animatorSet.play(translate).with(scaleX).with(scaleY).with(alpha);
		return animatorSet;
	}
	public static ObjectAnimator getButtonsAnim(View v, boolean isMenuShown)
	{
		Context context = v.getContext();
		Interpolator interpolator = AnimationUtils.loadInterpolator(context, android.R.anim.accelerate_decelerate_interpolator);
		
		float fromAlpha = isMenuShown ? 1 : 0;
		float toAlpha = isMenuShown ? 0 : 1;
		int duration = context.getResources().getInteger(R.integer.animation_duration);
		
		ObjectAnimator alpha = ObjectAnimator.ofFloat(v, "alpha", fromAlpha, toAlpha);
		alpha.setDuration(duration);
		alpha.setInterpolator(interpolator);
		return alpha;
	}
	
	public static float getScaleCoeff(int pos)
	{
		float out;
		switch (pos)
		{
			case R.id.mapContainer:
				out = 0.9F;
				break;
			case R.id.searchContainer:
				out = 0.8F;
				break;
			case R.id.myWashesContainer:
				out = 0.7F;
				break;
			case R.id.myCarContainer:
				out = 0.6F;
				break;
			default:
				out = 0.9F;
				break;
		}
		return out;
	}

	public static float getXCoeff(int pos)
	{
		float out;
		switch (pos)
		{
			case R.id.mapContainer:
				out = 0.7F;
				break;
			case R.id.searchContainer:
				out = 0.45F;
				break;
			case R.id.myWashesContainer:
				out = 0.2F;
				break;
			case R.id.myCarContainer:
				out = -0.05F;
				break;
			default:
				out = 0.7F;
				break;
		}
		return out;
	}
	
	public static float getXStayCoeff(int pos)
	{
		float out;
		switch (pos)
		{
			case R.id.mapContainer:
				out = 0.75F;
				break;
			case R.id.searchContainer:
				out = 0.55F;
				break;
			case R.id.myWashesContainer:
				out = 0.35F;
				break;
			case R.id.myCarContainer:
				out = 0.15F;
				break;
			default:
				out = 0.8F;
				break;
		}
		return out;
	}
}
