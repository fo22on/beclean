package ru.beclean.free.ui.mycars;

import com.androidquery.AQuery;

import ru.beclean.free.R;
import ru.beclean.free.ui.register.CarsCategoriesAdapter;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class MyCarSelectCategoryDialog extends Dialog implements android.view.View.OnClickListener, OnItemClickListener{
	private AQuery aq;
	public int carClassId = -1;
	
	public MyCarSelectCategoryDialog(Context context) {
		super(context, R.style.myBackgroundStyle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setCanceledOnTouchOutside(false);
		View rootView = getLayoutInflater().inflate(R.layout.dialog_select_car_category, null, false);
		aq = new AQuery(rootView);
		initactionBar();
		aq.id(R.id.dataListView).itemClicked(this).adapter(new CarsCategoriesAdapter(context, true));
		setContentView(rootView);
		WindowManager.LayoutParams wmlp = getWindow().getAttributes();
		wmlp.width = WindowManager.LayoutParams.MATCH_PARENT;
		wmlp.height = WindowManager.LayoutParams.MATCH_PARENT;
	}
	
	private void initactionBar()
	{
		aq.id(R.id.actionBarTitleTextView).text(R.string.title_car_types);
		aq.id(R.id.actionBarBackImageButton).clicked(this);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.actionBarBackImageButton:
			dismiss();
			break;

		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		carClassId = position;
		dismiss();
	}
}
