package ru.beclean.free.ui.mycars;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.db.DBUtils;
import ru.beclean.free.ui.parent.ParentActivity;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.servertypes.UserCar;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.JsonParamsCreator;
import ru.beclean.free.utils.UtilsMethods;
import ru.beclean.free.utils.rpc.RPCAnswer;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class MyCarsFragment extends ParentFragment implements OnClickListener, OnItemClickListener,
		OnItemLongClickListener
{

	private AQuery aq;
	private MyCarsAdapter mAdapter;
	private SwipeRefreshLayout swipeRefreshLayout;
	private boolean isFromMenu = true;

	private int duration;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(getArguments()!=null)
		{
			boolean fromFilter = getArguments().getBoolean("from_filter", false); 
			isFromMenu = !fromFilter;
		}
		
		View rootView = inflater.inflate(R.layout.fragment_my_cars, container, false);
		aq = new AQuery(rootView);
		initActionBar();
		
		UserCar selectedCar = DBUtils.getMyCar(getActivity(), UtilsMethods.getUserCarCategory(getActivity()));
		mAdapter = new MyCarsAdapter(getActivity(), DBUtils.getMyCars(getActivity()), selectedCar.id);
		aq.id(R.id.dataListView).itemClicked(this).itemLongClicked(this).adapter(mAdapter);

		duration = getResources().getInteger(R.integer.animation_buttons_duration);

		swipeRefreshLayout = (SwipeRefreshLayout) aq.id(R.id.swipe_container).getView();
		swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener()
		{
			@Override
			public void onRefresh()
			{
				updateCars();
			}
		});
		swipeRefreshLayout.setColorScheme(R.color.text_blue_color, R.color.text_white_color, R.color.text_blue_color,
				R.color.text_white_color);

		return rootView;
	}

	private void initActionBar()
	{
		aq.id(R.id.actionBarBackImageButton).image(isFromMenu?(R.drawable.icon_menu_selector):(R.drawable.icon_back_selector)).clicked(this);
		aq.id(R.id.actionBarTitleTextView).text(R.string.menu_item_my_car);
		aq.id(R.id.actionBarSecondActionImageButton).image(R.drawable.icon_add_selector).visibility(View.VISIBLE)
				.clicked(this).getImageView().setContentDescription(getString(R.string.my_car_add_button_title));
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.actionBarBackImageButton:
				if (mAdapter != null && mAdapter.isEditMode())
				{
					setEditMode(false, -1);
				}
				else
				{
					if(isFromMenu)
					{
						toggleMenu();
					}
					else
					{
						getActivity().onBackPressed();
					}
				}
				break;
			case R.id.actionBarSecondActionImageButton:
				if (mAdapter != null && mAdapter.isEditMode())
				{
					deleteCars();
				}
				else
				{
					showCarFragment(-1);
				}
				break;
			default:
				break;
		}
	}

	private void deleteCars()
	{
		int[] selectedItems = mAdapter.getSelected();
		for (int i = 0; i < selectedItems.length; i++)
		{
			JsonParamsCreator creator = new JsonParamsCreator();
			creator.setParams("" + mAdapter.getItem(selectedItems[i]).id);
			JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_CLIENT_CAR_REMOVE, creator.getArrayParams());
			AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
			{
				@Override
				public void callback(String url, RPCAnswer object, AjaxStatus status)
				{
					//super.callback(url, object, status);
				}
			};
			ApiUtils.initAuth(getActivity(), cb);
			aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, params,
					RPCAnswer.class, cb);
			DBUtils.deleteCar(getActivity(), mAdapter.getItem(selectedItems[i]).id);
		}
		mAdapter.delete();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		if (mAdapter == null)
			return;
		if (mAdapter.isEditMode())
		{
			mAdapter.setSelected(position);
		}
		else
		{
			showCarFragment(mAdapter.getItem(position).id);
		}
	}

	private void showCarFragment(int id)
	{
		FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		MyCarsAddFragment f = new MyCarsAddFragment();
		if (id != -1)
		{
			Bundle bundle = new Bundle();
			bundle.putInt(MyCarsAddFragment.ARG_CAR_ID, id);
			f.setArguments(bundle);
		}
		ft.replace(((ParentActivity) getActivity()).getContainerID(), f, MyCarsAddFragment.class.getName());
		ft.addToBackStack(MyCarsAddFragment.class.getName());
		ft.commit();
	}

	private void updateCars()
	{
		if(mAdapter.isEditMode()) setEditMode(false, -1);
		JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_CLIENT_CARS_GET);
		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer object, AjaxStatus status)
			{
				if(object.isError())
				{
					
				}
				else
				{
					JSONArray array = object.getResultArray();
					ArrayList<UserCar> cars = new ArrayList<UserCar>();
					for (int i = 0; i < array.length(); i++)
					{
						cars.add(new UserCar(array.optJSONObject(i)));
					}
				}
				swipeRefreshLayout.setRefreshing(false);
			}
		};
		ApiUtils.initAuth(getActivity(), cb);
		aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, params,
				RPCAnswer.class, cb);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
	{
		if (mAdapter.getCount() > 1)
		{
			setEditMode(true, position);
			return true;
		}
		return false;
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		if(mAdapter!=null)
		{
			UtilsMethods.saveUserCarCategory(getActivity(), mAdapter.getSelection());
		}
	}

	private void setEditMode(boolean isEdit, int position)
	{
		mAdapter.setEditMode(isEdit);
		if (isEdit)
		{
			mAdapter.setSelected(position);
			aq.id(R.id.actionBarBackImageButton).getView().animate().alpha(0).setDuration(duration)
					.setListener(new AnimatorListenerAdapter()
					{
						@Override
						public void onAnimationEnd(Animator animation)
						{
							aq.id(R.id.actionBarBackImageButton).image(R.drawable.icon_check_selector).getView().animate().alpha(1)
									.setDuration(duration);
						}
					});
			aq.id(R.id.actionBarSecondActionImageButton).getView().animate().alpha(0).setDuration(duration)
					.setListener(new AnimatorListenerAdapter()
					{
						@Override
						public void onAnimationEnd(Animator animation)
						{
							aq.id(R.id.actionBarSecondActionImageButton).image(R.drawable.icon_trash_selector)
									.getView().animate().alpha(1).setDuration(duration);
						}
					});
		}
		else
		{
			aq.id(R.id.actionBarSecondActionImageButton).getView().animate().alpha(0).setDuration(duration)
					.setListener(new AnimatorListenerAdapter()
					{
						@Override
						public void onAnimationEnd(Animator animation)
						{

							aq.id(R.id.actionBarSecondActionImageButton).image(R.drawable.icon_add_selector).getView()
									.animate().alpha(1).setDuration(duration);
						}
					});
			;
			aq.id(R.id.actionBarBackImageButton).getView().animate().alpha(0).setDuration(duration)
					.setListener(new AnimatorListenerAdapter()
					{
						@Override
						public void onAnimationEnd(Animator animation)
						{
							aq.id(R.id.actionBarBackImageButton).image(R.drawable.icon_menu_selector).getView()
									.animate().alpha(1).setDuration(duration);
						}
					});
		}
		swipeRefreshLayout.setEnabled(!isEdit);
	}
}
