package ru.beclean.free.ui.mycars;

import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.db.DBUtils;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.servertypes.CarType;
import ru.beclean.free.ui.servertypes.CarsTypes;
import ru.beclean.free.ui.servertypes.UserCar;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.JsonParamsCreator;
import ru.beclean.free.utils.UtilsMethods;
import ru.beclean.free.utils.rpc.RPCAnswer;

public class MyCarsAddFragment extends ParentFragment implements OnClickListener
{
	public static final String ARG_CAR_ID = "arg_car_id";
	private AQuery aq;
	private int carId = -1;
	private EditText name;
	private EditText number;

	private int selectedClassId = 1;
	private CarsTypes carTypes;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_car_add, container, false);
		aq = new AQuery(rootView);

		if (getArguments() != null)
		{
			carId = getArguments().getInt(ARG_CAR_ID, -1);
		}
		aq.id(R.id.addButton).enabled(false).clicked(this);
		aq.id(R.id.typeLinearLayout).clicked(this).enabled(carId == -1);
		name = aq.id(R.id.nameEditText).getEditText();
		number = aq.id(R.id.numberEditText).getEditText();
		
		number.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(!TextUtils.isEmpty(s)&&UtilsMethods.isNumber(s))
				{
					aq.id(R.id.addButton).enabled(true);
					number.setError(null);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});

		number.setOnEditorActionListener(new OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				CharSequence str = number.getText();
				if (actionId == EditorInfo.IME_ACTION_SEARCH
						|| actionId == EditorInfo.IME_ACTION_DONE
						|| (actionId == EditorInfo.IME_ACTION_UNSPECIFIED && event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
				{

					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
							Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(number.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					
					if(!TextUtils.isEmpty(str)&&UtilsMethods.isNumber(str))
					{
						aq.id(R.id.addButton).enabled(true);
					}
					else
					{
						number.setError(getString(R.string.my_car_error_number_format));
						aq.id(R.id.addButton).enabled(false);
					}
					return true;
				}
				
				
				
				return false;
			}
		});
		initActionBar();

		carTypes = ((BeApp) getActivity().getApplication()).getCarTypes();

		initFields();
		initType();
		
		return rootView;
	}

	private void initFields() {
		if(carId!=-1)
		{
			UserCar uc = DBUtils.getMyCar(getActivity(), carId);
			name.setText(uc.name);
			number.setText(uc.car_number);
			selectedClassId = uc.car_class_id;
		}
	}

	private void initActionBar()
	{
		aq.id(R.id.actionBarBackImageButton).clicked(this);
		aq.id(R.id.actionBarTitleTextView).text(R.string.my_car_add_title);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.actionBarBackImageButton:
				getActivity().onBackPressed();
				break;
			case R.id.addButton:
				saveCar();
				break;
			case R.id.typeLinearLayout:
				Dialog dialog = new MyCarSelectCategoryDialog(getActivity());
				dialog.setOnDismissListener(new OnDismissListener()
				{

					@Override
					public void onDismiss(DialogInterface dialog)
					{
						int tmpId = ((MyCarSelectCategoryDialog) dialog).carClassId;
						if (tmpId > -1)
						{
							selectedClassId = tmpId;
							initType();
						}
					}
				});
				dialog.show();
				break;
			default:
				break;
		}
	}

	private void saveCar()
	{
		blockInterface(true);
		JsonParamsCreator creator = new JsonParamsCreator();
		creator.addParam("car_class_id", selectedClassId);
		creator.addParam("name", name.getText().toString());
		creator.addParam("car_number", number.getText().toString());
		AjaxCallback<RPCAnswer> cb;
		JSONObject params;
		if (carId != -1)
		{
			JsonParamsCreator updateCreator = new JsonParamsCreator();
			updateCreator.addParam("id", "" + carId);
			updateCreator.addParam("params", creator.getParams());
			params = ApiUtils.getParams(ApiUtils.API_METHOD_CLIENT_CAR_UPDATE, updateCreator.getParams());
		}
		else
		{
			params = ApiUtils.getParams(ApiUtils.API_METHOD_CLIENT_CAR_APPEND, creator.getParams());

		}
		cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer object, AjaxStatus status)
			{
				if (object != null && !object.isError())
				{
					UserCar car = new UserCar(object.getResult());
					DBUtils.saveCar(getActivity(), car);
					getActivity().onBackPressed();
				}
				else
				{
					Toast.makeText(getActivity(), R.string.my_car_error_servet, Toast.LENGTH_LONG).show();
				}

				blockInterface(false);
			}
		};
		ApiUtils.initAuth(getActivity(), cb);
		aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, params,
				RPCAnswer.class, cb);
	}

	private void initType()
	{
		for (CarType type : carTypes.types)
		{
			if (type.id == selectedClassId)
			{
				aq.id(R.id.typeTextView).text(type.name);
				break;
			}
		}
	}

	private void blockInterface(boolean isBlock)
	{
		try
		{
			aq.id(R.id.addButton).enabled(!isBlock);
			if(carId>-1)
			{
				aq.id(R.id.typeLinearLayout).enabled(false);
			}
			else
			{
				aq.id(R.id.typeLinearLayout).enabled(!isBlock);
			}
			number.setEnabled(!isBlock);
			name.setEnabled(!isBlock);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
