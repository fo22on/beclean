package ru.beclean.free.ui.mycars;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import ru.beclean.free.R;
import ru.beclean.free.ui.servertypes.UserCar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyCarsAdapter extends BaseAdapter
{
	private LayoutInflater inflater;
	private ArrayList<UserCar> mItems;
	private int selectedPosition = 0;

	private boolean isEditMode = false;
	private ArrayList<Integer> selected = new ArrayList<Integer>();
	
	private int bgNormal = R.color.bg_main_color;
	private int bgSelected = R.color.bg_list_item_selected;

	private int[] carTypesImages = { R.drawable.icon_car_class_1, R.drawable.icon_car_class_1_unselected,
			R.drawable.icon_car_class_2, R.drawable.icon_car_class_2_unselected, R.drawable.icon_car_class_3,
			R.drawable.icon_car_class_3_unselected, R.drawable.icon_car_class_4, R.drawable.icon_car_class_4_unselected };

	public MyCarsAdapter(Context context, ArrayList<UserCar> items, int selectedId)
	{
		inflater = LayoutInflater.from(context);
		mItems = items;
		for (int i = 0; i < mItems.size(); i++)
		{
			if(mItems.get(i).id==selectedId)
			{
				selectedPosition = i;
				break;
			}
		}
	}

	@Override
	public int getCount()
	{
		return mItems.size();
	}

	@Override
	public UserCar getItem(int position)
	{
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	public void setSelected(int position)
	{
		int pos = selected.indexOf(Integer.valueOf(position));
		if (pos >= 0)
		{
			selected.remove(pos);
		}
		else
		{
			if(selected.size()!=mItems.size()-1)
			{
				selected.add(Integer.valueOf(position));
			}
		}
		notifyDataSetChanged();
	}

	public void clearSelected()
	{
		selected.clear();
	}

	public int[] getSelected()
	{
		if (isEditMode)
		{
			int[] ids = new int[selected.size()];
			for (int i = 0; i < ids.length; i++)
			{
				ids[i] = selected.get(i).intValue();
			}
			return ids;
		}
		return null;
	}

	public void setEditMode(boolean isEditMode)
	{
		if (!isEditMode)
		{
			selected.clear();
		}
		this.isEditMode = isEditMode;
		notifyDataSetChanged();
	}

	public boolean isEditMode()
	{
		return isEditMode;
	}
	
	public void delete()
	{
		if(isEditMode)
		{
			Collections.sort(selected, new Comparator<Integer>()
			{

				@Override
				public int compare(Integer lhs, Integer rhs)
				{
					return rhs.compareTo(lhs);
				}
			});
			for (Integer position : selected)
			{
				mItems.remove(position.intValue());
			}
		}
		notifyDataSetChanged();
	}
	
	public void setItems (ArrayList<UserCar> items)
	{
		mItems = items;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		UserCar item = getItem(position);
		ViewHolder holder;
		if (convertView == null)
		{
			convertView = inflater.inflate(R.layout.item_my_car, parent, false);
			holder = new ViewHolder();
			holder.carIcon = (ImageView) convertView.findViewById(R.id.carTypeImageView);
			holder.carIcon.setOnClickListener(pictureClickListener);
			holder.carName = (TextView) convertView.findViewById(R.id.carNameTextView);
			holder.carNumber = (TextView) convertView.findViewById(R.id.carNumberTextView);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (selected.contains(Integer.valueOf(position)))
		{
			convertView.setBackgroundResource(bgSelected);
		}
		else
		{
			convertView.setBackgroundResource(bgNormal);
		}

		holder.carIcon.setTag(position);
		holder.carIcon.setImageResource(getItemBm(item.car_class_id, selectedPosition == position));
		holder.carName.setText(item.name);
		holder.carNumber.setText(item.car_number);

		return convertView;
	}

	public void setSelection(int position)
	{
		selectedPosition = position;
		notifyDataSetChanged();
	}
	
	public int getSelection()
	{
		return mItems.get(selectedPosition).id;
	}

	private int getItemBm(int category, boolean isEnabled)
	{
		int position = 0;

		switch (category)
		{
			case 1:
				position = 0;
				break;
			case 2:
				position = 2;
				break;
			case 3:
				position = 4;
				break;
			case 4:
				position = 6;
				break;
		}

		if (!isEnabled)
		{
			position++;
		}

		return carTypesImages[position];
	}
	
	private OnClickListener pictureClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			int position = ((Integer)v.getTag()).intValue();
			setSelection(position);
		}
	};

	private static class ViewHolder
	{
		TextView carName;
		TextView carNumber;
		ImageView carIcon;
	}
	
}
