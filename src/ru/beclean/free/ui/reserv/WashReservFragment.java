package ru.beclean.free.ui.reserv;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import com.androidmapsextensions.SupportMapFragment;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.AlarmManager;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.ui.parent.ParentActivity;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.servertypes.Reserv;
import ru.beclean.free.ui.servertypes.Services;
import ru.beclean.free.ui.servertypes.ServiciesTypes;
import ru.beclean.free.ui.servertypes.ServiciesTypes.ServiceType;
import ru.beclean.free.ui.servertypes.WashFull;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.JsonParamsCreator;
import ru.beclean.free.utils.OnBackPressedListener;
import ru.beclean.free.utils.UtilsMethods;
import ru.beclean.free.utils.rpc.RPCAnswer;

public class WashReservFragment extends ParentFragment implements OnClickListener
{
	private final static String ACTION_TICK = "ru.beclean.free.ui.reserv.WashReservFragment.ACTION_TICK";
	private final static int updateTime = 60000;
	private GoogleMap map;
	private AQuery aq;

	private SupportMapFragment fragment;

	private Reserv mReserv = null;
	private WashFull cw;

	private BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			getActivity().runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					aq.id(R.id.remainingTimeTextView).text(
							getString(R.string.reserv_time_to_run_title) + " " + getRemainingTime());
				}
			});
		}
	};

	private OnBackPressedListener onBackPressedListener = new OnBackPressedListener()
	{
		@Override
		public boolean onBackPressed()
		{
			if (getChildFragmentManager().getBackStackEntryCount() > 0)
			{
				getChildFragmentManager().popBackStack();
			}
			else
			{
				getActivity().finish();
			}
			return true;
		}
	};

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		FragmentManager fm = getChildFragmentManager();
		fragment = (SupportMapFragment) fm.findFragmentById(R.id.mapContainer);
		if (fragment == null)
		{
			fragment = SupportMapFragment.newInstance();
			fm.beginTransaction().replace(R.id.mapContainer, fragment).commit();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_wash_reserv, container, false);
		aq = new AQuery(rootView);
		initActionBar();

		aq.id(R.id.roadButton).visibility(View.GONE).clicked(this);
		aq.id(R.id.cancelButton).clicked(this);
		aq.id(R.id.servicesTextView).clicked(this);

		mReserv = UtilsMethods.getReserv(getActivity());

		aq.id(R.id.priceTextView).text(mReserv.price + " " + getString(R.string.wash_coins));
		aq.id(R.id.startWashTextView).text(getString(R.string.reserv_start_time_title) + " " + getStartTime());
		aq.id(R.id.remainingTimeTextView).text(getString(R.string.reserv_time_to_run_title) + " " + getRemainingTime());

		getWashInfo();

		return rootView;
	}

	private void initActionBar()
	{
		aq.id(R.id.actionBarBackImageButton).visibility(View.GONE);
		TextView title = aq.id(R.id.actionBarTitleTextView).getTextView();
		title.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		title.setPadding(getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin), 0, getResources()
				.getDimensionPixelSize(R.dimen.activity_horizontal_margin), 0);
		aq.id(R.id.actionBarSecondActionImageButton).image(R.drawable.ic_device_access_capp_selector).clicked(this)
				.visibility(View.VISIBLE);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (map == null)
		{
			map = fragment.getMap();
			map.setOnMyLocationChangeListener(new OnMyLocationChangeListener()
			{
				@Override
				public void onMyLocationChange(Location location)
				{
					((BeApp) getActivity().getApplication()).setCurrentLocation(location);
					initDistance();
				}
			});
			map.setMyLocationEnabled(true);
			map.getUiSettings().setZoomControlsEnabled(false);
			map.getUiSettings().setMyLocationButtonEnabled(false);
			map.getUiSettings().setAllGesturesEnabled(false);
			map.animateCamera(UtilsMethods.getDefaultMapPosition());
		}
		((ParentActivity) getActivity()).addBackPressListener(onBackPressedListener);
		getActivity().registerReceiver(updateReceiver, new IntentFilter(ACTION_TICK));
		setAlarm();

	}

	@Override
	public void onPause()
	{
		super.onPause();
		((ParentActivity) getActivity()).removeBackPressListener(onBackPressedListener);
		cancelAlarm();
		getActivity().unregisterReceiver(updateReceiver);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.actionBarSecondActionImageButton:
			{
				if (mReserv == null)
					return;
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:" + mReserv.phone));
				startActivity(intent);
				break;
			}
			case R.id.roadButton:
			{
				if (cw == null)
					return;
				Location current = ((BeApp) getActivity().getApplication()).getCurrentPosition();
				String uri = String.format(Locale.ENGLISH,
						"http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", current.getLatitude(),
						current.getLongitude(), "Home Sweet Home", cw.lat, cw.lng, "Where the party is at");
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
				intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
				startActivity(intent);
				break;
			}
			case R.id.cancelButton:
			{
				Builder builder = new Builder(getActivity());
				builder.setTitle(R.string.settings_warning_logout_title);
				builder.setMessage(R.string.reserv_warning_cancel);
				builder.setPositiveButton(R.string.reserv_warning_button_confirm_title,
						new android.content.DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								cancelReserv();
								dialog.dismiss();
							}
						});
				builder.setNegativeButton(R.string.reserv_warning_button_cancel_title,
						new android.content.DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								dialog.dismiss();
							}
						});
				builder.create().show();
				break;
			}
			case R.id.servicesTextView:
			{
				break;
			}
			default:
				break;
		}
	}

	private void getWashInfo()
	{
		JsonParamsCreator paramsCreator = new JsonParamsCreator();
		paramsCreator.addParam("id", mReserv.cw_id);
		JSONObject washParams = ApiUtils.getParams(ApiUtils.API_METHOD_CARWASHES_GET, paramsCreator.getParams());
		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer answer, AjaxStatus status)
			{
				super.callback(url, answer, status);
				if (answer.isError())
				{
					Toast.makeText(getActivity(), answer.getResult().toString(), Toast.LENGTH_LONG).show();
				}
				else
				{
					cw = new WashFull(answer.getResult());
					ServiciesTypes types = ((BeApp) getActivity().getApplication()).getServiciesTypes();
					Services.initServices(cw, types);
					initViewDescription();
				}
			}
		};

		ApiUtils.initAuth(getActivity(), cb);
		aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, washParams,
				RPCAnswer.class, cb);
	}

	private void initViewDescription()
	{
		if (cw == null)
			return;
		LatLng point = new LatLng(cw.lat, cw.lng);
		map.addMarker(new MarkerOptions().position(point).title(cw.name)
				.icon(UtilsMethods.getIcon(R.drawable.blue_pin)));
		map.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 16));

		aq.id(R.id.actionBarTitleTextView).text(cw.name);
		aq.id(R.id.addressTextView).text(cw.address);
		initDistance();

		LinearLayout otherServices = (LinearLayout) aq.id(R.id.otherServicesLinearLayout).getView();
		int size = otherServices.getHeight();
		for (Services service : cw.services)
		{
			if (service.isOtherServices)
			{
				ImageView iv = new ImageView(getActivity());
				int padding = getResources().getDimensionPixelSize(R.dimen.activity_half_margin);
				iv.setPadding(padding, padding, padding, padding);
				AQuery ivAq = new AQuery(iv);
				// http://mapi.dev-beclean.me/static/skins/android-phone/default/cw-detail/additional-services/10.png
				String url = String
						.format(Locale.ENGLISH, ApiUtils.API_URL_SKIN
								+ "/static/skins/android-phone/default/cw-detail/additional-services/%d.png",
								service.idService);
				ivAq.image(url, true, true);
				LayoutParams params = new LayoutParams(size, size);
				iv.setLayoutParams(params);
				otherServices.addView(iv);
			}
		}
		if (otherServices.getChildCount() < 1)
		{
			otherServices.setVisibility(View.GONE);
		}
	}

	private void initDistance()
	{
		if (cw == null)
			return;
		Location cwLocation = new Location(cw.name);
		cwLocation.setLatitude(cw.lat);
		cwLocation.setLongitude(cw.lng);

		aq.id(R.id.distanceTextView).text(
				UtilsMethods.getDistance(cwLocation, ((BeApp) getActivity().getApplication()).getCurrentPosition()));
	}

	private String getStartTime()
	{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault());
		return format.format(new Date(mReserv.start));
	}

	private String getRemainingTime()
	{
		long remaing = mReserv.start - System.currentTimeMillis();

		return remaing / 60000 + " " + getString(R.string.wash_time_postfix);
	}

	public void setAlarm()
	{
		AlarmManager am = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent();
		i.setAction(ACTION_TICK);
		PendingIntent pi = PendingIntent.getBroadcast(getActivity(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
		am.setRepeating(AlarmManager.RTC_WAKEUP, 0, updateTime, pi);
	}

	public void cancelAlarm()
	{
		Intent intent = new Intent();
		intent.setAction(ACTION_TICK);
		PendingIntent sender = PendingIntent.getBroadcast(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}

	private void cancelReserv()
	{
		JsonParamsCreator creator = new JsonParamsCreator();
		creator.addParam("id", "" + mReserv.id);
		JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_RESERVATIONS_REMOVE, creator.getParams());
		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer object, AjaxStatus status)
			{
				super.callback(url, object, status);
				UtilsMethods.removeReserv(getActivity());
			}
		};
		
		ApiUtils.initAuth(getActivity(), cb);
		aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, params,
				RPCAnswer.class, cb);
	}
}
