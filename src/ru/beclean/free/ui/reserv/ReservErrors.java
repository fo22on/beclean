package ru.beclean.free.ui.reserv;

import android.annotation.SuppressLint;
import java.util.HashMap;

@SuppressLint("DefaultLocale")
public class ReservErrors
{
	private HashMap<String, String> errors;

	@SuppressLint("DefaultLocale")
	public ReservErrors()
	{
		errors = new HashMap<String, String>();
		errors.put("carwash services count != services_list".toLowerCase(), "������ ����� �� ������������ ���������� ���� ����!");
		errors.put("reservation is only 30 minutes from the current time".toLowerCase(),
				"�������� ������ �����. ��� ������ ��������� ���������� �������� ������ ����� 30 �����!");
		errors.put("Incorrect Reserv Time".toLowerCase(), "��������� ����� ������ ��������!");
		errors.put("Reservation Exists For User During The Time Interval".toLowerCase(),
				"� ��� ��� ���� ����� ��� ���������� �������!");
		errors.put("Carwash Is Not Approved".toLowerCase(), "�������� � ������ ������ ����� ����������!");
		errors.put("Carwash Unscrewed".toLowerCase(), "�������� � ������ ������ ����� ����������!");
	}

	@SuppressLint("DefaultLocale")
	public String getError(String key)
	{
		String out = errors.get(key.toLowerCase());
		return out == null ? "����� ����� �� ���." : out;
	}
}
