package ru.beclean.free.ui.washlist;

import java.util.ArrayList;
import java.util.Collections;

import ru.beclean.free.R;
import ru.beclean.free.ui.map.model.CarWash;
import ru.beclean.free.ui.map.model.CarWash.CarWashDistanceComparator;
import ru.beclean.free.ui.map.model.CarWash.CarWashPriceComparator;
import ru.beclean.free.utils.UtilsMethods;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

public class WashListAdapter extends BaseAdapter
{
	private ArrayList<CarWash> mItems;
	private LayoutInflater inflater;
	private Location currentLocation;
	private CarWashDistanceComparator distanceComparator;
	private CarWashPriceComparator priceComparator;
	
	public WashListAdapter (Context context, ArrayList<CarWash> items, Location current)
	{
		mItems = items;
		inflater = LayoutInflater.from(context);
		currentLocation = current;
		distanceComparator = new CarWashDistanceComparator(currentLocation);
		priceComparator = new CarWashPriceComparator();
	}
	
	@Override
	public int getCount()
	{
		return mItems.size();
	}

	@Override
	public CarWash getItem(int position)
	{
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		WashItemViewHolder holder;
		CarWash item = getItem(position);
		
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.item_wash_all, parent, false);
			holder = new WashItemViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.titleTextView);
			holder.description = (TextView) convertView.findViewById(R.id.descriptionTextView);
			holder.price = (TextView) convertView.findViewById(R.id.priceTextView);
			holder.distance = (TextView) convertView.findViewById(R.id.distanceTextView);
			holder.rating = (RatingBar) convertView.findViewById(R.id.ratingBar);
			convertView.setTag(holder);
		}
		else
		{
			holder = (WashItemViewHolder) convertView.getTag();
		}
		
		holder.title.setText(item.name);
		holder.description.setText(item.address);
		holder.price.setText("" + item.sumPrice);
		holder.distance.setText(UtilsMethods.getDistance(item.location, currentLocation));
		holder.rating.setRating(item.rating);
		
		return convertView;
	}
	
	public void setSort(int id)
	{
		switch (id)
		{
			case R.id.distanceRadio:
				Collections.sort(mItems, distanceComparator);
				break;
			case R.id.priceRadio:
				Collections.sort(mItems, priceComparator);
				break;
			case R.id.timeRadio:
				
				break;
		}
		
		notifyDataSetChanged();
	}
}
