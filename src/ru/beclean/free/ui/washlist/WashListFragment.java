package ru.beclean.free.ui.washlist;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.db.DBUtils;
import ru.beclean.free.ui.MainActivity;
import ru.beclean.free.ui.map.MapFragment;
import ru.beclean.free.ui.map.model.CarWash;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.servertypes.UserCar;
import ru.beclean.free.ui.wash.WashDescriptionFragment;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.FilterUtils;
import ru.beclean.free.utils.JsonParamsCreator;
import ru.beclean.free.utils.OnFilterStateChanged;
import ru.beclean.free.utils.UtilsMethods;
import ru.beclean.free.utils.rpc.RPCAnswer;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.model.LatLng;

public class WashListFragment extends ParentFragment implements OnClickListener, OnItemClickListener,
		OnCheckedChangeListener
{
	private AQuery aq;
	private WashListAdapter mAdapter;
	private SwipeRefreshLayout swipeRefreshLayout; 
	private RadioGroup filter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_wash_list, container, false);
		aq = new AQuery(rootView);

		aq.id(R.id.dataListView).itemClicked(this);
		
		swipeRefreshLayout = (SwipeRefreshLayout) aq.id(R.id.swipe_container).getView();
		swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener()
		{
			@Override
			public void onRefresh()
			{
				downloadWashes();
			}
		});
		swipeRefreshLayout.setColorScheme(R.color.text_blue_color,R.color.text_white_color, R.color.text_blue_color,R.color.text_white_color);/*android.R.color.holo_blue_bright, 
                android.R.color.holo_green_light, 
                android.R.color.holo_orange_light, 
                android.R.color.holo_red_light);*/
		
		filter = (RadioGroup) aq.id(R.id.sortRadioGroup).getView();
		filter.setOnCheckedChangeListener(this);
		initActionBar();
		downloadWashes();
		
		return rootView;
	}

	private void initActionBar()
	{
		aq.id(R.id.actionBarBackImageButton).image(R.drawable.icon_menu_selector).clicked(this);
		aq.id(R.id.actionBarTitleTextView).text(R.string.menu_item_search);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.actionBarBackImageButton:
				toggleMenu();
				break;

			default:
				break;
		}
	}

	private void downloadWashes()
	{
		JsonParamsCreator creator = new JsonParamsCreator();
		RectF currentRect = UtilsMethods.getRectForMapList(getActivity(),
				((BeApp) getActivity().getApplication()).getCurrentPosition());
		LatLng leftTop = new LatLng(currentRect.left, currentRect.top);
		LatLng rightBottom = new LatLng(currentRect.right, currentRect.bottom);
		JSONArray leftTopPoint = new JSONArray();
		JSONArray rightBottomPoint = new JSONArray();
		try
		{
			rightBottomPoint.put(rightBottom.longitude);
			rightBottomPoint.put(rightBottom.latitude);
			leftTopPoint.put(leftTop.longitude);
			leftTopPoint.put(leftTop.latitude);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		
		UserCar car = DBUtils.getMyCar(getActivity(), UtilsMethods.getUserCarCategory(getActivity()));
		creator.addParam("geo_square", leftTopPoint, rightBottomPoint).addParam("car_class_id", car.car_class_id);
		if (FilterUtils.getFilterTimeEnd(getActivity()) != 0)
		{
			creator.addParam("free_time", FilterUtils.getFilterFormattedTime(getActivity(), 0),
					FilterUtils.getFilterFormattedTime(getActivity(), 1));
		}
		int[] services = FilterUtils.getServices(getActivity()); 
		if(services!=null&&services.length>0)
		{
			JSONArray array = new JSONArray();
			for (int service : services)
			{
				array.put(service);
			}
			creator.addParam("services_list", array);
			
			creator.addParam("prices_list", FilterUtils.getPriceList(getActivity()));
		}
		
		JSONObject params = creator.getParams();

		JSONObject rotatorParams = ApiUtils.getParams(ApiUtils.API_METHOD_CARWASHES_ROTATOR, params);
		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer answer, AjaxStatus status)
			{
				super.callback(url, answer, status);
				if (!answer.isError())
				{
					ArrayList<CarWash> washes = new ArrayList<CarWash>();
					JSONArray array = answer.getResultArray();
					for (int i = 0; i < array.length(); i++)
					{
						CarWash carWash = new CarWash(array.optJSONObject(i));
						washes.add(carWash);
					}
					mAdapter = new WashListAdapter(getActivity(), washes,
							((BeApp) getActivity().getApplication()).getCurrentPosition());
					mAdapter.setSort(filter.getCheckedRadioButtonId());
					aq.id(R.id.dataListView).adapter(mAdapter);
				}

				swipeRefreshLayout.setRefreshing(false);
			}
		};

		ApiUtils.initAuth(getActivity(), cb);
		aq.progress(R.id.downloadProgressBar).transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, rotatorParams,
				RPCAnswer.class, cb);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
		WashDescriptionFragment wf = new WashDescriptionFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(WashDescriptionFragment.WASH_ID, mAdapter.getItem(position).id);
		wf.setArguments(bundle);
		wf.setReserves(mAdapter.getItem(position).reservations);

		ft.addToBackStack(MapFragment.class.getName());
		ft.replace(((MainActivity) getActivity()).getContainerID(), wf);
		ft.commit();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId)
	{
		if (mAdapter != null)
			mAdapter.setSort(checkedId);
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		((MainActivity) getActivity()).removeOnFilterStateChanged(mOnFilterStateChanged);
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		((MainActivity) getActivity()).setOnFilterStateChanged(mOnFilterStateChanged);
	}
	
	private OnFilterStateChanged mOnFilterStateChanged = new OnFilterStateChanged()
	{
		@Override
		public void onNewState(boolean isOpen)
		{
			if(!isOpen)
			{
				downloadWashes();
			}
		}
	};
}
