package ru.beclean.free.ui.washlist;

import android.widget.RatingBar;
import android.widget.TextView;

public class WashItemViewHolder
{
	public TextView title;
	public TextView description;
	public TextView price;
	public TextView distance;
	public RatingBar rating;
}