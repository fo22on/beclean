package ru.beclean.free.ui.mywash;

import java.util.ArrayList;

import ru.beclean.free.R;
import ru.beclean.free.ui.washlist.WashItemViewHolder;
import ru.beclean.free.utils.UtilsMethods;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

public class MyWashesAdapter extends BaseAdapter
{
	private ArrayList<MyCarWash> mItems;
	private LayoutInflater inflater;
	private Location currentLocation;
	private boolean isEditMode = false;
	private ArrayList<Integer> selected = new ArrayList<Integer>();
	private int bgNormal = R.color.bg_main_color;
	private int bgSelected = R.color.bg_list_item_selected;

	public MyWashesAdapter(Context context, ArrayList<MyCarWash> items, Location current)
	{
		mItems = items;
		inflater = LayoutInflater.from(context);
		currentLocation = current;
	}

	@Override
	public int getCount()
	{
		return mItems.size();
	}

	@Override
	public MyCarWash getItem(int position)
	{
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	public ArrayList<MyCarWash> getItems()
	{
		return mItems;
	}

	public void setSelected(int position)
	{
		int pos = selected.indexOf(Integer.valueOf(position));
		if (pos >= 0)
		{
			selected.remove(pos);
		}
		else
		{
			selected.add(Integer.valueOf(position));
		}
		notifyDataSetChanged();
	}
	
	public void clearSelected()
	{
		selected.clear();
	}
	
	public int[] getSelected()
	{
		if(isEditMode)
		{
			int[] ids = new int[selected.size()];
			for (int i = 0; i < ids.length; i++)
			{
				ids[i] = selected.get(i).intValue();
			}
			return ids;
		}
		return null;
	}

	public void setEditMode(boolean isEditMode)
	{
		if (!isEditMode)
		{
			selected.clear();
		}
		this.isEditMode = isEditMode;
		notifyDataSetChanged();
	}

	public boolean isEditMode()
	{
		return isEditMode;
	}

	@Override
	public boolean isEnabled(int position)
	{
		return isEditMode || getItem(position).isAvalable;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		WashItemViewHolder holder;
		MyCarWash item = getItem(position);

		if (convertView == null)
		{
			convertView = inflater.inflate(R.layout.item_wash_all, parent, false);
			holder = new WashItemViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.titleTextView);
			holder.description = (TextView) convertView.findViewById(R.id.descriptionTextView);
			holder.price = (TextView) convertView.findViewById(R.id.priceTextView);
			holder.distance = (TextView) convertView.findViewById(R.id.distanceTextView);
			holder.rating = (RatingBar) convertView.findViewById(R.id.ratingBar);
			holder.rating.setVisibility(View.GONE);
			convertView.setTag(holder);
		}
		else
		{
			holder = (WashItemViewHolder) convertView.getTag();
		}

		if (selected.contains(Integer.valueOf(position)))
		{
			convertView.setBackgroundResource(bgSelected);
		}
		else
		{
			convertView.setBackgroundResource(bgNormal);
		}

		holder.title.setText(item.name);
		holder.description.setText(item.address);
		if (item.isAvalable)
		{
			holder.price.setTextColor(Color.GREEN);
			holder.price.setText(R.string.wash_enable);
		}
		else
		{
			holder.price.setTextColor(Color.RED);
			holder.price.setText(R.string.wash_disable);
		}
		holder.distance.setText(UtilsMethods.getDistance(item.location, currentLocation));

		return convertView;
	}
}
