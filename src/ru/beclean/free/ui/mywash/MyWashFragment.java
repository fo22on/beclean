package ru.beclean.free.ui.mywash;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.beclean.free.R;
import ru.beclean.free.application.BeApp;
import ru.beclean.free.db.DBUtils;
import ru.beclean.free.ui.MainActivity;
import ru.beclean.free.ui.map.MapFragment;
import ru.beclean.free.ui.map.model.CarWash;
import ru.beclean.free.ui.parent.ParentFragment;
import ru.beclean.free.ui.wash.WashDescriptionFragment;
import ru.beclean.free.utils.ApiUtils;
import ru.beclean.free.utils.JsonParamsCreator;
import ru.beclean.free.utils.rpc.RPCAnswer;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class MyWashFragment extends ParentFragment implements OnClickListener, OnItemClickListener,
		OnItemLongClickListener
{
	private AQuery aq;
	private MyWashesAdapter mAdapter;
	private ListView mListView;
	private SwipeRefreshLayout swipeRefreshLayout;
	private int duration;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_my_wash, container, false);
		aq = new AQuery(rootView);
		initActionBar();

		mListView = (ListView) aq.id(R.id.dataListView).getView();

		duration = getResources().getInteger(R.integer.animation_buttons_duration);
		swipeRefreshLayout = (SwipeRefreshLayout) aq.id(R.id.swipe_container).getView();
		swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener()
		{
			@Override
			public void onRefresh()
			{
				updateWashes();
				swipeRefreshLayout.setRefreshing(false);
			}
		});
		swipeRefreshLayout.setColorScheme(R.color.text_blue_color, R.color.text_white_color, R.color.text_blue_color,
				R.color.text_white_color);
		// swipeRefreshLayout.setEnabled(false);

		updateWashes();
		mListView.setOnItemClickListener(this);
		mListView.setOnItemLongClickListener(this);

		return rootView;
	}

	private void updateWashes()
	{
		String[] ids = DBUtils.getFavoritesWashesIds(getActivity());
		JsonParamsCreator creator = new JsonParamsCreator();
		creator.setParams(ids);
		JSONObject params = ApiUtils.getParams(ApiUtils.API_METHOD_CARWASHES_UPDATE_ROTATOR, creator.getArrayParams());

		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>()
		{
			@Override
			public void callback(String url, RPCAnswer answer, AjaxStatus status)
			{
				super.callback(url, answer, status);
				if (!answer.isError())
				{
					ArrayList<MyCarWash> items = DBUtils.getFavoritesWashes(getActivity());
					ArrayList<MyCarWash> updated = new ArrayList<MyCarWash>();
					JSONArray array = answer.getResultArray();
					for (int i = 0; i < array.length(); i++)
					{
						updated.add(new MyCarWash(array.optJSONObject(i)));
					}

					for (MyCarWash wash : updated)
					{
						int index = items.indexOf(wash);
						if (index >= 0)
						{
							items.remove(index);
							items.add(index, wash);
						}
					}
					Collections.sort(items, new MyCarWash.CarWashAvalableComparator());

					mAdapter = new MyWashesAdapter(getActivity(), items,
							((BeApp) getActivity().getApplication()).getCurrentPosition());
					mListView.setAdapter(mAdapter);
				}
				swipeRefreshLayout.setRefreshing(false);
			}
		};

		ApiUtils.initAuth(getActivity(), cb);
		aq.progress(R.id.downloadProgressBar).transformer(((BeApp) getActivity().getApplication()).getTransformer())
				.post(ApiUtils.API_URL, params, RPCAnswer.class, cb);
	}

	private void initActionBar()
	{
		aq.id(R.id.actionBarBackImageButton).image(R.drawable.icon_menu_selector).clicked(this);
		aq.id(R.id.actionBarTitleTextView).text(R.string.menu_item_my_washes);
		aq.id(R.id.actionBarSecondActionImageButton).image(R.drawable.icon_trash_selector).clicked(this)
				.visibility(View.VISIBLE).getView().setAlpha(0);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.actionBarBackImageButton:
				if (mAdapter != null && mAdapter.isEditMode())
				{
					setEditMode(false, -1);
				}
				else
				{
					toggleMenu();
				}
				break;
			case R.id.actionBarSecondActionImageButton:
				if (mAdapter.isEditMode())
				{
					int[] positions = mAdapter.getSelected();
					ArrayList<MyCarWash> cws = mAdapter.getItems();
					ArrayList<MyCarWash> toDelete = new ArrayList<MyCarWash>();
					for (int i = 0; i < positions.length; i++)
					{
						MyCarWash cw = cws.get(positions[i]);
						DBUtils.removeWashFromFavorites(getActivity(), cw.id);
						removeFromServer(cw.id);
						toDelete.add(cw);
					}
					for (MyCarWash myCarWash : toDelete)
					{
						cws.remove(myCarWash);
					}
					mAdapter.clearSelected();
					mAdapter.notifyDataSetChanged();
				}
				break;
			default:
				break;
		}
	}

	private void removeFromServer(int cwId)
	{
		JsonParamsCreator creator = new JsonParamsCreator();
		creator.addParam("cw_id", "" + cwId);
		JSONObject favParams = ApiUtils
				.getParams(ApiUtils.API_METHOD_CARWASH_REMOVE_FROM_FAVORITE, creator.getParams());
		AjaxCallback<RPCAnswer> cb = new AjaxCallback<RPCAnswer>();
		ApiUtils.initAuth(getActivity(), cb);
		aq.transformer(((BeApp) getActivity().getApplication()).getTransformer()).post(ApiUtils.API_URL, favParams,
				RPCAnswer.class, cb);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
	{
		if (!mAdapter.isEditMode())
		{
			setEditMode(true, position);
			return true;
		}
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		if (mAdapter.isEditMode())
		{
			mAdapter.setSelected(position);
		}
		else
		{
			FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
			WashDescriptionFragment wf = new WashDescriptionFragment();
			Bundle bundle = new Bundle();
			bundle.putInt(WashDescriptionFragment.WASH_ID, mAdapter.getItem(position).id);
			wf.setArguments(bundle);
			wf.setReserves(mAdapter.getItem(position).reservations);

			ft.addToBackStack(MapFragment.class.getName());
			ft.replace(((MainActivity) getActivity()).getContainerID(), wf);
			ft.commit();
		}
	}

	private void setEditMode(boolean isEdit, int position)
	{
		mAdapter.setEditMode(isEdit);
		if (isEdit)
		{
			mAdapter.setSelected(position);
			aq.id(R.id.actionBarBackImageButton).getView().animate().alpha(0).setDuration(duration)
					.setListener(new AnimatorListenerAdapter()
					{
						@Override
						public void onAnimationEnd(Animator animation)
						{
							aq.id(R.id.actionBarBackImageButton).image(R.drawable.icon_check_selector).getView().animate().alpha(1)
									.setDuration(duration);
							aq.id(R.id.actionBarSecondActionImageButton).getView().animate().alpha(1)
									.setDuration(duration);
						}
					});
		}
		else
		{
			aq.id(R.id.actionBarSecondActionImageButton).getView().animate().alpha(0).setDuration(duration);
			aq.id(R.id.actionBarBackImageButton).getView().animate().alpha(0).setDuration(duration)
					.setListener(new AnimatorListenerAdapter()
					{
						@Override
						public void onAnimationEnd(Animator animation)
						{
							aq.id(R.id.actionBarBackImageButton).image(R.drawable.icon_menu_selector).getView()
									.animate().alpha(1).setDuration(duration);
							aq.id(R.id.actionBarSecondActionImageButton).getView().animate().alpha(0)
									.setDuration(duration);
						}
					});
		}
		swipeRefreshLayout.setEnabled(!isEdit);
	}
}
