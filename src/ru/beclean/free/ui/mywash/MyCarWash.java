package ru.beclean.free.ui.mywash;

import java.util.Comparator;

import org.json.JSONObject;

import com.google.android.gms.internal.ih;

import android.database.Cursor;

import ru.beclean.free.ui.map.model.CarWash;

public class MyCarWash extends CarWash
{
	public boolean isAvalable = false;
	public MyCarWash(JSONObject object)
	{
		super(object);
		isAvalable = true;
	}
	
	public MyCarWash(Cursor cursor)
	{
		super(cursor);
		isAvalable = false;
	}
	
	public static class CarWashAvalableComparator implements Comparator<MyCarWash>
	{
		@Override
		public int compare(MyCarWash lhs, MyCarWash rhs)
		{
			if(lhs.isAvalable==rhs.isAvalable)
			{
				return 0;
			}
			else if(lhs.isAvalable&&!rhs.isAvalable)
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
	}
}
