package ru.beclean.free.application;

import ru.beclean.free.ui.servertypes.CarsTypes;
import ru.beclean.free.ui.servertypes.ServiciesTypes;
import ru.beclean.free.ui.servertypes.ServiciesTypes.ServiceType;
import ru.beclean.free.utils.UtilsMethods;
import ru.beclean.free.utils.rpc.RPCTransformer;
import android.app.Application;
import android.location.Location;

public class BeApp extends Application
{
	private CarsTypes mCarTypes;
	private ServiciesTypes mServiciesTypes;
	private RPCTransformer mTransformer;
	private String phoneSupport;
	private Location currentLocation = null;
	private String fileServerUrl;
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		mTransformer = new RPCTransformer();
	}
	
	public CarsTypes getCarTypes()
	{
		return mCarTypes;
	}

	public void setCarTypes(CarsTypes mCarTypes)
	{
		this.mCarTypes = mCarTypes;
	}

	public ServiciesTypes getServiciesTypes()
	{
		return mServiciesTypes;
	}

	public String[] getServiciesTypesIds()
	{
		if (mServiciesTypes == null || mServiciesTypes.items == null || mServiciesTypes.items.size() < 1)
			return new String[] { "1", "2" };
		String[] out = new String[mServiciesTypes.items.size()];
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < out.length; i++)
		{
			ServiceType item = mServiciesTypes.items.get(i);
			sb.append(item.id);
			out[i] = sb.toString();
			sb.setLength(0);
		}
		return out;
	}

	public void setServiciesTypes(ServiciesTypes mServiciesTypes)
	{
		this.mServiciesTypes = mServiciesTypes;
	}
	public RPCTransformer getTransformer()
	{
		return mTransformer;
	}
	
	public Location getCurrentPosition()
	{
		if(currentLocation!=null)
		{
			return currentLocation;
		}
		return UtilsMethods.getDefaultPosition();
	}
	
	public void setCurrentLocation(Location location)
	{
		currentLocation = location;
	}

	public void setPhoneSupport(String phone)
	{
		phoneSupport = phone;
	}
	
	public String getPhoneSupport()
	{
		return phoneSupport;
	}
	
	public void setFileUrl(String url)
	{
		fileServerUrl = url;
	}
	
	public String getFileUrl()
	{
		return fileServerUrl;
	}
}
